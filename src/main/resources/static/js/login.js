apiv1.form({
	selector: 'form[name=login]',
	alertSuccess: 'Login successful!',
	alertFailure: 'Login failed!',
	alertSuccessMessage: 'Redirecting you in 3 seconds...',
	handle: function(request) {
		request.done(function(data) {
			var location;
			if (data.isVolunteer) {
				location = '/volunteer';
			} else {
				location = '/charity';
			}
			setTimeout(function() {
				window.location.assign(location);
			}, 3000);
		});
	},
});
