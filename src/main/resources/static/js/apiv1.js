var apiv1 = {};

apiv1.request = function(config) {
	_.defaults(config, {
		url: '/api/v1',
		type: 'GET',
		data: '',
		alertFailure: 'Failure!',
	});

	var data = config.data;
	if (config.type != 'GET') {
		data = JSON.stringify(data);
	}
	var request = $.ajax({
		url: config.url,
		type: config.type,
		data: data,
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
	});

	return request
		.always(function() {
			$('#alerts').html('');
		})
		.fail(function(xhr) {
			let data = xhr.responseJSON;
			var message;
			switch (data.errorType) {
				case "formValidation":
					message = data.field + ': ' + data.reason;
					break;
				case "volunteerExists":
				case "userExists":
					message = "A user with this email already exists.";
					break;
				case "charityExists":
					message = "A charity with this email already exists.";
					break;
				default:
					message = data.message;
					break;
			}
			$('#alerts').append($('<div class="alert alert-danger"><strong>'+config.alertFailure+'</strong> '+message+'</div>'));
		});
};

apiv1.form = function(config) {
	_.defaults(config, {
		selector: 'form',
		type: 'POST',
		alertSuccess: 'Success!',
		alertFailure: 'Failure!',
		alertSuccessMessage: 'The operation was successful',
		handle: function(request) { },
	});

	$(config.selector).submit(function(event) {
		event.preventDefault();
		var form = $(this);

		var alerts = $('#alerts');
		alerts.html('').append($(
			'<div class="alert alert-info">' +
				'<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>' +
				'<span class="sr-only">Loading...</span>' +
			'</div>'
		));

		var request = apiv1.request({
			url: form.attr('action'),
			type: config.type,
			data: form.serializeJSON(),
			alertFailure: config.alertFailure,
		});

		request.done(function(data) {
			var alert = $('<div class="alert alert-success"><strong>'+config.alertSuccess+'</strong> '+config.alertSuccessMessage+'</div>');
			alerts.append(alert);
		})
		config.handle(request);
	});
};
