function doSearch(form) {
	var results = form.find('#results');

	if (form.find('input[name=search]').val().length > 2) {
		var search = apiv1.request({
			url: form.attr('action'),
			type: 'GET',
			data: form.serializeJSON(),
		});

		search.done(function(data) {
			results.html('');

			for (let job of data) {
				results.append(microteerjob.render(job));
			}
		});
	}
};

$('form[name=search] input').on('keyup', _.debounce(function(event) {
	doSearch($(this).parent('form'));
}, 300));

$('form[name=search]').submit(function(event) {
	event.preventDefault();
	doSearch($(this));
});
