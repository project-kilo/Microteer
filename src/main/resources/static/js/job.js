var microteerjob = {};

microteerjob.render = function(data, onAccept, onReject) {
	var row = $('<div class="job"></div>').data('uuid', data.uuid);

	var summary = $('<h2></h2>').text(data.summary);
	var description = $('<p class="job-description"></p>').text(data.description);
	row.append($('<div class="main"></div>').append(summary).append(description));

	var org = $('<p class="organisation"></p>').text(data.owner);
	var expiration = $('<p></p>').text('Expiration: '+data.expiration);
	let location = data.location;
	var address = $('<p></p>').text(location.address1+"\n"+location.town+"\n"+location.postcode);
	row.append($('<div class="meta"></div>').append(org).append(expiration).append(address));

	var buttons = $('<div class="buttons"></div>');
	if (onAccept) {
		var acceptButton = $('<button class="accept"><i class="fa fa-check fa-2x" aria-hidden="true"></i></button>');
		acceptButton.click(function (event) {
			var job = $(this).closest('div.job');
			onAccept(job);
		});
		buttons.append(acceptButton);
	}
	if (onReject) {
		var rejectButton = $('<button class="reject"><i class="fa fa-close fa-2x" aria-hidden="true"></i></button>');
		rejectButton.click(function (event) {
			var job = $(this).closest('div.job');
			onReject(job);
		});
		buttons.append(rejectButton);
	}
	row.append(buttons);

	return row;
};
