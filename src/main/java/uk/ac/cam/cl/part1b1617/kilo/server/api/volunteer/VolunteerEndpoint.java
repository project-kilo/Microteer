package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import ratpack.func.Action;
import ratpack.handling.Chain;

public class VolunteerEndpoint implements Action<Chain> {

	@Override
	public void execute(Chain chain) throws Exception {
		chain
			.path(new SignUpHandler())
			.path(":email", new VolunteerHandler())
			.path(":email/allocated", new AllocatedRootHandler())
			.path(":email/allocated/:id", new AllocatedHandler())
			.path(":email/accepted", new AcceptedRootHandler())
			.path(":email/accepted/:id", new AcceptedHandler())
			.path(":email/notification", new NotificationRootHandler())
			.path(":email/notification/:id", new NotificationHandler())
		;
	}

}
