package uk.ac.cam.cl.part1b1617.kilo.server;

import static ratpack.handlebars.Template.handlebarsTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.github.jknack.handlebars.context.FieldValueResolver;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.context.MapValueResolver;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import ratpack.error.ClientErrorHandler;
import ratpack.error.ServerErrorHandler;
import ratpack.guice.Guice;
import ratpack.handlebars.HandlebarsModule;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.handling.Handlers;
import ratpack.handling.RequestLogger;
import ratpack.registry.Registry;
import ratpack.registry.RegistryBuilder;
import ratpack.server.BaseDir;
import ratpack.server.RatpackServer;
import ratpack.session.Session;
import ratpack.session.SessionModule;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.DatabaseImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Notification;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.api.ErrorResponseHandler;
import uk.ac.cam.cl.part1b1617.kilo.server.api.SessionHandler;
import uk.ac.cam.cl.part1b1617.kilo.server.api.charity.CharityEndpoint;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobEndpoint;
import uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer.VolunteerEndpoint;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.UnauthorizedResponse;

public class Main {
    public static final DatabaseImpl database = new DatabaseImpl();

    private static final Set<String> PUBLIC_PAGES = ImmutableSet.of(
        "login",
        "about",
        "contact",
        "volunteer/signup",
        "charity/signup"
    );
    private static final Set<String> VOLUNTEER_PAGES = ImmutableSet.of(
        "volunteer/profile",
        "volunteer/available-jobs",
        "volunteer/jobs",
        "volunteer/search"
    );
    private static final Set<String> ORG_PAGES = ImmutableSet.of(
        "charity/add-job",
        "charity/add-user",
        "charity/profile",
        "charity/jobs"
    );

    private static class NotificationContainer {
        public List<Notification> notifications;

        public NotificationContainer(List<Notification> notifications) {
            this.notifications = notifications;
        }
    }

    public static void main(String... args) throws Exception {

        RatpackServer.start(server -> server
                .serverConfig(c -> c.baseDir(BaseDir.find()))
                .registry(Guice.registry(b -> b
                    .module(SessionModule.class)
                    .module(HandlebarsModule.class)
                ))
                .handlers(chain -> chain
                    .all(RequestLogger.ncsa())
                    .files(f -> f.dir("vendor").path("vendor"))
                    .files(f -> f.dir("static"))
                    .prefix("api", api -> api
                        .register(r -> r
                            .add(ClientErrorHandler.class, new ErrorResponseHandler())
                            .add(ServerErrorHandler.class, new ErrorResponseHandler())
                        )
                        .prefix("v1", apiv1 -> apiv1
                            .all(Handlers.accepts("application/json"))
                            .all(Handlers.contentTypes("application/json"))
                            .prefix("session", new SessionHandler())
                            .prefix("volunteer", new VolunteerEndpoint())
                            .prefix("charity", new CharityEndpoint())
                            .prefix("job", new JobEndpoint())
                        )
                        .notFound()
                    )
                    .register(r -> r
                        .add(ClientErrorHandler.class, new PageErrorHandler())
                        .add(ServerErrorHandler.class, new PageErrorHandler())
                    )
                    .all(ctx -> {
                        ctx.get(Session.class).getData().then(sessionData -> {
                            Control control = ControlFactory.makeControl();

                            RegistryBuilder builder = Registry.builder();
                            try {
                                User user = LoginUtils.getLoggedInUser(control, ctx, sessionData);
                                builder.add(User.class, user);
                                if (user.volunteer != null) {
                                    builder.add(Volunteer.class, user.volunteer);
                                } else if (user.adminOf != null) {
                                    builder.add(Organisation.class, user.adminOf);
                                }

                                Iterable<Notification> notifications =
                                    control.getReceivedNotifications(user);
                                builder.add(
                                    NotificationContainer.class,
                                    new NotificationContainer(ImmutableList.copyOf(notifications))
                                );
                            } catch (UnauthorizedResponse e) {
                            }

                            ctx.next(builder.build());
                        });
                    })
                    .get(ctx -> renderPage(ctx, "home")) // index handler (GET /)
                    .get("volunteer", ctx -> ctx.redirect(302, "/volunteer/available-jobs"))
                    .get("charity", ctx -> ctx.redirect(302, "/charity/jobs"))
                    .all(Handlers.chain(
                        Handlers.get(),
                        publicPageHandler(),
                        volunteerPageHandler(),
                        orgPageHandler(),
                        Handlers.notFound()
                    ))
                )
        );
    }

    private static Handler publicPageHandler() {
        return Handlers.onlyIf(
            ctx -> PUBLIC_PAGES.contains(ctx.getRequest().getPath()),
            ctx -> renderPage(ctx, ctx.getRequest().getPath())
        );
    }

    private static Handler volunteerPageHandler() {
        return Handlers.onlyIf(
            ctx -> VOLUNTEER_PAGES.contains(ctx.getRequest().getPath()),
            ctx -> {
                if (!ctx.maybeGet(User.class).isPresent()) {
                    throw new UnauthorizedResponse();
                }
                if (!ctx.maybeGet(Volunteer.class).isPresent()) {
                    throw new ForbiddenResponse();
                }
                renderPage(ctx, ctx.getRequest().getPath());
            }
        );
    }

    private static Handler orgPageHandler() {
        return Handlers.onlyIf(
            ctx -> ORG_PAGES.contains(ctx.getRequest().getPath()),
            ctx -> {
                if (!ctx.maybeGet(User.class).isPresent()) {
                    throw new UnauthorizedResponse();
                }
                if (!ctx.maybeGet(Organisation.class).isPresent()) {
                    throw new ForbiddenResponse();
                }
                renderPage(ctx, ctx.getRequest().getPath());
            }
        );
    }

    private static void renderPage(Context context, String path) {
        Map<String, Object> map = new HashMap<>();
        if (context.maybeGet(User.class).isPresent()) {
            map.put("user", context.get(User.class));
            if (context.maybeGet(Volunteer.class).isPresent()) {
                map.put("volunteer", context.get(Volunteer.class));
            }
            if (context.maybeGet(Organisation.class).isPresent()) {
                map.put("org", context.get(Organisation.class));
            }
            if (context.maybeGet(NotificationContainer.class).isPresent()) {
                map.put("notifications", context.get(NotificationContainer.class).notifications);
            }
        }

        context.render(handlebarsTemplate(
            String.format("%s.html", path),
            com.github.jknack.handlebars.Context
                .newBuilder(map)
                .resolver(
                    MapValueResolver.INSTANCE,
                    JavaBeanValueResolver.INSTANCE,
                    FieldValueResolver.INSTANCE
                )
                .build()
        ));
    }
}
