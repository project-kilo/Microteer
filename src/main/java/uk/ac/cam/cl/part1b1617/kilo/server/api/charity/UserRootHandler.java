package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class UserRootHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(UserRootHandler.class);

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");

		Control control = ControlFactory.makeControl();
		Organisation org;
		try {
			org = control.getOrganisation(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.list(context, org, control))
				.post(() -> this.create(context, org, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void list(Context context, Organisation org, Control control) {
		Iterable<User> users = control.getAdmins(org);
		List<UserResult> results = StreamSupport.stream(users.spliterator(), false)
			.map(j -> new UserResult(j)).collect(Collectors.toList());

		context.render(Jackson.json(results));
		context.getResponse().status(200);
	}

	private void create(Context context, Organisation org, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			User user = LoginUtils.getLoggedInOrgUser(control, context, sessionData);

			if (user.adminOf != org) {
				logger.warn("Forbidden attempt to create user under {} by {}", org.email, user.email);
				throw new ForbiddenResponse("You are not authorized to add users to this charity");
			}

			Promise<UserRequest> request = context.parse(
				Jackson.fromJson(UserRequest.class)
			);
			request.then(r -> {
				r.validate();

				ArrayList<String> passwordWithHash = HashPassword.generatePasswordHash(r.password);
				User newUser = new User(
					passwordWithHash.get(0),
					passwordWithHash.get(1),
					Instant.now(),
					r.fullname,
					r.email,
					org
				);

				control.addOrganisationUser(newUser, org);
				context.getResponse().status(201);
				context.header("Location", String.format(
					"/api/v1/charity/%s/user/%s",
					org.email, newUser.email
				));
				context.render(Jackson.json(new UserResult(newUser)));
			});
		});

	}
}
