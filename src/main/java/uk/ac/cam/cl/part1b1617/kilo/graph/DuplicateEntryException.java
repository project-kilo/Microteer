package uk.ac.cam.cl.part1b1617.kilo.graph;
public class DuplicateEntryException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String identifier;
	public DuplicateEntryException(String identifier)
	{
		this.identifier = identifier;
	} 
}
