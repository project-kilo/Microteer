package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.neo4j.ogm.session.Session;

import com.google.common.collect.ImmutableMap;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;

public class UserManager implements UserManagerService {

	protected Session session;

	public UserManager(Session session) {
		this.session = session;
	}

	@Override
	public User getUserForEmail(String email) throws EntryNotFoundException {
		User user = session.load(User.class, email);
		if (user!=null) {

			if (user.volunteer != null) {
				user.volunteer = session.load(Volunteer.class, user.volunteer.getId(), 1);
				user.volunteer.jobs = session.load(VolunteerJobs.class, user.volunteer.jobs.getId(), 1);
			} else {
				user.adminOf = session.load(Organisation.class, user.adminOf.email, 1);
			}

			return user;
		}
		throw new EntryNotFoundException(email);
	}

	@Override
	public void addVolunteer(Volunteer volunteer) throws DuplicateEntryException {
		User duplicate=null;
		try {
			duplicate = session.load(User.class, volunteer.user.email);
		}
		catch (NullPointerException e) {

		}
		if (duplicate!=null) {
			throw new DuplicateEntryException(volunteer.user.email);
		}
		createVolunteerJobs(volunteer);
		session.save(volunteer);
		session.query(
			"MATCH (v:Volunteer) WHERE id(v) = {id} " +
			"CALL spatial.addNode({layer}, v) " +
			"YIELD node RETURN node",
			ImmutableMap.of(
				"id", volunteer.getId(),
				"layer", Database.SPATIAL_LAYER
			)
		);
	}

	private void createVolunteerJobs(Volunteer volunteer) {
		VolunteerJobs jobs = new VolunteerJobs();
		jobs.volunteer = volunteer;
		volunteer.jobs = jobs;
	}

	@Override
	public Organisation getOrganisation(String email) throws EntryNotFoundException {
		Organisation org = this.session.load(Organisation.class, email);
		if (org == null) {
			throw new EntryNotFoundException(email);
		}
		return org;
	}

	@Override
	public Iterable<User> getAdmins(Organisation organisation) {
		return this.session.query(
			User.class,
			"MATCH (u:User)-[:ADMIN_OF]->(:Organisation {email: {email}}) " +
			"RETURN u",
			ImmutableMap.of(
				"email", organisation.email
			)
		);
	}

	@Override
	public void addOrganisation(Organisation organisation) throws DuplicateEntryException {
		Organisation duplicate = null;
		try {
			duplicate = session.load(Organisation.class, organisation.email);
		}
		catch (NullPointerException e) {

		}
		if (duplicate!=null) {
			throw new DuplicateEntryException(organisation.email);
		}
		session.save(organisation);
		session.query(
			"MATCH (o:Organisation) WHERE id(o) = {id} " +
			"CALL spatial.addNode({layer}, o) " +
			"YIELD node RETURN node",
			ImmutableMap.of(
				"id", organisation.getId(),
				"layer", Database.SPATIAL_LAYER
			)
		);
	}

	@Override
	public void updateVolunteer(Volunteer volunteer) {
		session.save(volunteer);
		session.query(
			"MATCH ()-[r:RTREE_REFERENCE]-(v:Volunteer) WHERE id(v) = {id} " +
			"DELETE r",
			ImmutableMap.of(
				"id", volunteer.getId()
			)
		);
		session.query(
			"MATCH (v:Volunteer) WHERE id(v) = {id} " +
			"CALL spatial.addNode({layer}, v) " +
			"YIELD node RETURN node",
			ImmutableMap.of(
				"id", volunteer.getId(),
				"layer", Database.SPATIAL_LAYER
			)
		);
	}

	@Override
	public void updateOrganisation(Organisation organisation) {
		session.save(organisation);
		session.query(
			"MATCH ()-[r:RTREE_REFERENCE]-(o:Organisation) WHERE id(o) = {id} " +
			"DELETE r",
			ImmutableMap.of(
				"id", organisation.getId()
			)
		);
		session.query(
			"MATCH (o:Organisation) WHERE id(o) = {id} " +
			"CALL spatial.addNode({layer}, o) " +
			"YIELD node RETURN node",
			ImmutableMap.of(
				"id", organisation.getId(),
				"layer", Database.SPATIAL_LAYER
			)
		);
	}

	@Override
	public void removeVolunteer(Volunteer volunteer) {
		session.delete(volunteer.jobs);
		session.delete(volunteer.user);
		session.delete(volunteer);
	}

	@Override
	public void removeOrganisation(Organisation organisation) {
		organisation = session.load(Organisation.class, organisation.email, 1);
		for (Job job : organisation.jobs) {
			session.delete(job);
		}
		session.delete(organisation);
	}

	@Override
	public void addOrganisationUser(User user) throws DuplicateEntryException {
		User duplicate = session.load(User.class, user.email);
		if (duplicate!=null) {
			throw new DuplicateEntryException(user.email);
		}
		session.save(user);
	}

	@Override
	public void removeOrganisationUser(User user) {
		session.delete(user);
	}

	@Override
	public void updateOrganisationUser(User user) {
		session.save(user);
	}

	@Override
	public Volunteer loadVolunteerFrom(VolunteerJobs volunteerJobs) {
		volunteerJobs = session.load(VolunteerJobs.class, volunteerJobs.getId());
		Volunteer result = session.load(Volunteer.class, volunteerJobs.volunteer.getId());
		result.jobs = volunteerJobs;
		return result;
	}

}
