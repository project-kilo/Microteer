package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.time.DayOfWeek;
import java.util.EnumMap;

import org.neo4j.ogm.cypher.BooleanOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Time;

public class TimePeriodServiceImpl implements TimePeriodService {

    protected Session session;

    public TimePeriodServiceImpl(Session session) {
        this.session = session;
    }

    @Override
    public Time getTimeEntity(DayOfWeek day, TimePeriod period) {
        Filter dayFilter = new Filter("day", day);
        Filter periodFilter = new Filter("timePeriod", period);
        periodFilter.setBooleanOperator(BooleanOperator.AND);

        Iterable<Time> results = this.session.loadAll(
            Time.class,
            new Filters(dayFilter, periodFilter)
        );
        return results.iterator().next();
    }

    static void initialize(Session session) {
        EnumMap<DayOfWeek, EnumMap<TimePeriod, Time>> times = new EnumMap<>(DayOfWeek.class);
        for (DayOfWeek day : DayOfWeek.values()) {
            times.put(day, new EnumMap<>(TimePeriod.class));
        }

        Iterable<Time> existing = session.loadAll(Time.class);
        for (Time time : existing) {
            times.get(time.day).put(time.timePeriod, time);
        }

        for (DayOfWeek day : DayOfWeek.values()) {
            for (TimePeriod period : TimePeriod.values()) {
                EnumMap<TimePeriod, Time> innerMap = times.get(day);
                if (!innerMap.containsKey(period)) {
                    Time newTime = new Time(day, period);
                    session.save(newTime);
                    innerMap.put(period, newTime);
                }
            }
        }
    }
}
