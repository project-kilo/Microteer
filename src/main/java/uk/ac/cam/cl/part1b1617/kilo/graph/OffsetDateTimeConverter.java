package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import org.neo4j.ogm.typeconversion.AttributeConverter;

public class OffsetDateTimeConverter implements AttributeConverter<OffsetDateTime, String> {

    @Override
    public String toGraphProperty(OffsetDateTime value) {
        return value.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    @Override
    public OffsetDateTime toEntityAttribute(String value) {
        return OffsetDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }
}
