package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;

public class FormValidationException extends ErrorResponse {
    private static final long serialVersionUID = -3748394610251093056L;
    public final String field;
    public final String reason;

    public FormValidationException(String field, String reason) {
        super(String.format("%s: %s", field, reason));
        this.field = field;
        this.reason = reason;
    }

    @Override
    public String getErrorType() {
        return "formValidation";
    }
}
