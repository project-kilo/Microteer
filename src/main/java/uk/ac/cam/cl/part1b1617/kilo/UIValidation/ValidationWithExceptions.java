package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

import java.io.IOException;

import com.google.common.base.Strings;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;

public class ValidationWithExceptions {

	public static void validateFullAddress(String address,String city,
			String postcode, String country) throws FormValidationException {
		validatePostcode(postcode);
		validateAddressLine(address);
		validateCity(city);
		validateCountry(country);
	}

	public static void validateUserCreation(String fullname, 
			String password, String email) throws FormValidationException {
		validateFullname(fullname);
		validatePassword(password);
		validateEmail(email);
	}

	public static void validateAddress(AddressLocation location) throws FormValidationException {
		try {
			location.getGPSLocation();
		} catch (IOException e) {
			throw new FormValidationException("Address","Is not a valid location");
		}
	}

	public static void validateFullname(String fullname) throws FormValidationException {
		if (Strings.isNullOrEmpty(fullname)) {
			throw new FormValidationException("Full name", "Cannot be empty");
		}
		if (!UserInputValidation.userName(fullname)) {
			throw new FormValidationException("Full name", "Invalid");
		}
	}

	public static void validateOrgname(String orgname) throws FormValidationException {
		if (Strings.isNullOrEmpty(orgname)) {
			throw new FormValidationException("Organisation name", "Cannot be empty");
		}
		if (!UserInputValidation.organisationName(orgname)) {
			throw new FormValidationException("Organisation name", "Invalid");
		}
	}

	public static void validateDescription(String description) throws FormValidationException {
		if (Strings.isNullOrEmpty(description)) {
			throw new FormValidationException("Description", "Cannot be empty");
		}
		if (!UserInputValidation.description(description)) {
			throw new FormValidationException("Description", "Too long");
		}
	}

	public static void validateWebsite(String website) throws FormValidationException {
		if (Strings.isNullOrEmpty(website)) {
			throw new FormValidationException("Website", "Cannot be empty");
		}
		if (!UserInputValidation.organisationWebsite(website)) {
			throw new FormValidationException("Website", "Not a valid URL");
		}
	}

	public static void validateAddressLine(String address) throws FormValidationException {
		if (Strings.isNullOrEmpty(address)) {
			throw new FormValidationException("Address 1", "Cannot be empty");
		}
		if (!UserInputValidation.userAddress(address)) {
			throw new FormValidationException("Address", "Invalid address");
		}
	}

	public static void validateCity(String city) throws FormValidationException {
		if (Strings.isNullOrEmpty(city)) {
			throw new FormValidationException("City", "Cannot be empty");
		}
		if (!UserInputValidation.userAddress(city)) {
			throw new FormValidationException("City", "Invalid city");
		}
	}

	public static void validateEmail(String email) throws FormValidationException {
		if (Strings.isNullOrEmpty(email)) {
			throw new FormValidationException("Email", "Cannot be empty");
		}
		if (!UserInputValidation.email(email)) {
			throw new FormValidationException("Email", "Invalid email");
		}
	}

	public static void validatePassword(String password) throws FormValidationException {
		if (Strings.isNullOrEmpty(password)) {
			throw new FormValidationException("Password", "Cannot be empty");
		}
		if (!UserInputValidation.userPassword(password)) {
			throw new FormValidationException("Password", "Invalid password");
		}
	}

	public static void validatePostcode(String postcode) throws FormValidationException {
		if (Strings.isNullOrEmpty(postcode)) {
			throw new FormValidationException("Postcode", "Cannot be empty");
		}
		if (!UserInputValidation.postCode(postcode)) {
			throw new FormValidationException("Postcode", "Invalid postcode");
		}
	}

	public static void validateCountry(String country) throws FormValidationException {
		if (Strings.isNullOrEmpty(country)) {
			throw new FormValidationException("Country", "Cannot be empty");
		}
		if (!UserInputValidation.userAddress(country)) {
			throw new FormValidationException("Country", "Invalid country");
		}
	}

	public static void validatePhone(String phone) throws FormValidationException {
		if (Strings.isNullOrEmpty(phone)) {
			throw new FormValidationException("Phone", "Cannot be empty");
		}
		if (!UserInputValidation.phoneNumber(phone)) {
			throw new FormValidationException("Phone", "Invalid phone number");
		}
	}

}
