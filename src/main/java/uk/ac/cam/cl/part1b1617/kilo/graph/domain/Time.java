package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.time.DayOfWeek;

import org.neo4j.ogm.annotation.NodeEntity;

import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;

@NodeEntity
public class Time extends Entity {
	public DayOfWeek day;
	public TimePeriod timePeriod;

	public Time(DayOfWeek day, TimePeriod timePeriod) {
		this.day = day;
		this.timePeriod = timePeriod;
	}

	public Time() {
	}
}
