package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;

public interface MatchingService {
    Iterable<Job> getMatchingJobs(Volunteer volunteer);
    Iterable<Job> getMatchingJobs(Volunteer volunteer, Filter filter);
    Iterable<Job> getMatchingJobs(Volunteer volunteer, Filters filters);

    Iterable<Volunteer> getMatchingVolunteers(Job job);
    Iterable<Volunteer> getMatchingVolunteers(Job job, Filter filter);
    Iterable<Volunteer> getMatchingVolunteers(Job job, Filters filters);
}
