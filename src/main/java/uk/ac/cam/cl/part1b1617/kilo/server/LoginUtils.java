package uk.ac.cam.cl.part1b1617.kilo.server;

import java.util.Base64;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.handling.Context;
import ratpack.http.Headers;
import ratpack.session.SessionData;

import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.UnauthorizedResponse;

public class LoginUtils {
	private static final Logger logger = LoggerFactory.getLogger(LoginUtils.class);

	private static User loginSession(Control control, SessionData sessionData) {
		String email;
		try {
			email = (String)sessionData.get("name").get();
		} catch (NoSuchElementException e) {
			throw new UnauthorizedResponse();
		} catch (Exception e) {
			logger.error("Error retrieving data from session");
			throw new RuntimeException(e);
		}

		User user;
		try {
			user = control.getUserForEmail(email);
		} catch (EntryNotFoundException e) {
			logger.error("Cannot find user {}", email);
			throw new UnauthorizedResponse();
		}

		return user;
	}

	private static User loginBasicAuth(Control control, String authorization) {
		Base64.Decoder decoder = Base64.getDecoder();
		String decoded = new String(decoder.decode(authorization));
		String[] parts = decoded.split(":", 2);
		String email = parts[0];
		String password = parts[1];

		User user;
		try {
			user = control.getUserForEmail(email);
		} catch (EntryNotFoundException e) {
			logger.info("Failed login, no such user {}", email);
			throw new UnauthorizedResponse();
		}

		if (!HashPassword.checkPassword(password, user.passwordHash, user.passwordSalt)) {
			logger.info("Failed login by {}", email);
			throw new UnauthorizedResponse();
		}

		return user;
	}

	public static User getLoggedInUser(
		Control control,
		Context context,
		SessionData sessionData
	) {
		Headers headers = context.getRequest().getHeaders();
		if (headers.contains("Authorization")) {
			String[] authorizationParts = headers.get("Authorization").split(" ", 2);
			if (authorizationParts[0].equals("Basic")) {
				return LoginUtils.loginBasicAuth(control, authorizationParts[1]);
			}
		}

		return LoginUtils.loginSession(control, sessionData);
	}

	public static Volunteer getLoggedInVolunteer(
		Control control,
		Context context,
		SessionData sessionData
	) {
		User user = LoginUtils.getLoggedInUser(control, context, sessionData);

		Volunteer volunteer = user.volunteer;
		if (volunteer == null) {
			logger.warn("{} is not a Volunteer", user.email);
			throw new ForbiddenResponse();
		}

		return volunteer;
	}

	public static User getLoggedInOrgUser(
		Control control,
		Context context,
		SessionData sessionData
	) {
		User user = LoginUtils.getLoggedInUser(control, context, sessionData);
		if (user.adminOf == null) {
			logger.warn("{} is not an admin of an organisation", user.email);
			throw new ForbiddenResponse();
		}

		return user;
	}
}
