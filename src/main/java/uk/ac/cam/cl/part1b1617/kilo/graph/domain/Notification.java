package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.time.Instant;
import java.util.UUID;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.Convert;
import org.neo4j.ogm.typeconversion.UuidStringConverter;

import uk.ac.cam.cl.part1b1617.kilo.graph.InstantConverter;

@NodeEntity
public class Notification extends Entity {
    @Index(primary = true, unique = true)
    @Convert(UuidStringConverter.class)
    public UUID uuid;

    @Convert(InstantConverter.class)
    public Instant submitted;
    public String subject;
    public String body;

    @Relationship(type = "TO", direction = Relationship.OUTGOING)
    public User to;
    @Relationship(type = "FROM", direction = Relationship.OUTGOING)
    public User from;
    @Relationship(type = "INVOLVING", direction = Relationship.OUTGOING)
    public Job involving;

    public Notification() {
        super();
        this.uuid = UUID.randomUUID();
        this.submitted = Instant.now();
    }
}
