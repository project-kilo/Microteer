package uk.ac.cam.cl.part1b1617.kilo.server.error;

public class ForbiddenResponse extends ErrorResponse {
	private static final long serialVersionUID = 1L;

	public ForbiddenResponse() {
		super("Forbidden");
	}
	public ForbiddenResponse(String message) {
		super(message);
	}

	public String getErrorType() {
		return "forbidden";
	}

	public int responseCode() {
		return 403;
	}
}
