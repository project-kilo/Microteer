package uk.ac.cam.cl.part1b1617.kilo.server.error;

public class ServerError extends ErrorResponse {
	private static final long serialVersionUID = 1L;

	public ServerError(String message) {
		super(message);
	}

	public String getErrorType() {
		return "serverError";
	}

	public int responseCode() {
		return 500;
	}
}
