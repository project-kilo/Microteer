package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

public interface UserInputValidationInterface {
	
	//Validation for the volunteer sign up/edit profile
	
	//Check that volunteer first and secondname is valid
	//Both methods return true if name is less than 32 characters
	//and contains only letters(a-z); false otherwise
	public boolean volunteerName(String name);
	
	//Check volunteer description is valid - returns true if so.
	//Description is valid if it is less than 500 characters.
	//Returns false otherwise.
	public boolean volunteerDescription(String description);
	
	
	//Validation for the organisation sign up/edit profile
	//Check org. name is valid - returns true if so (false otherwise).
	//Name is valid if is less than 50 characters long.
	public boolean organisationName(String name);
	
	//Check org. description is valid - returns true if so; false otherwise.
	//Description must be less than 500 characters.
	public boolean organisationDescription(String description);
	
	//Check org. location is valid - returns true if so; false otherwise.
	//Location should be a standard postcode.
	//TODO: what do you mean by "standard" postcode?
	//we can't assume this is restricted to the UK unless we are explicit
	public boolean organisationLocation(String postcode);
	
	//Check org. phone number is valid - returns true if so; false otherwise.
	//Number is either a landline or mobile number.
	public boolean organisationPhoneNumber(String phonenumber);
	
	//Check org. website is valid - returns true if so; false otherwise.
	//Ensure website is s a proper address
	public boolean organisationWebsite(String websiteaddress);
	
	//Check org. email is valid - returns true if so.
	//Ensure a proper email address.
	public boolean organisationEmail(String email);
	
	
	//Used to check passwords for either user, returns true if valid; false otherwise.
	//Password must be more than 6 characters long and be a combination of letters and numbers.
	public boolean userPassword(String password);
	
	
}