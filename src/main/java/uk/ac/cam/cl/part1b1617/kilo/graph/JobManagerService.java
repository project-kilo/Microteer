package uk.ac.cam.cl.part1b1617.kilo.graph;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;

import java.util.Set;
import java.util.UUID;

public interface JobManagerService {

    void addJob(Job job, Organisation organisation);

    Job getJob(UUID id) throws EntryNotFoundException;

    //remove a job object from the graph
    void removeJob(Job job);

    //update a job on the graph, assuming the given job has had all it's relationships updated
    void updateJob(Job job);

    //switch the allocated relation to an accepted relation
    void acceptJob(Job job, Volunteer volunteer);

    //remove the allocated relation
    void rejectJob(Job job, Volunteer volunteer);

    //switch the accepted relation to an allocated relation
    void unacceptJob(Job job, Volunteer volunteer, double fitness);

    //this adds a JobAllocationEdge object to the graph, and updates job and volunteer.jobs to link to it
    void suggestJob(Job job, Volunteer volunteer, double fitness);

    void jobCompleted(Volunteer volunteer, Job job, int rating);

    Job loadJob (Job job);

    void rateJob(Job job, int rating);

    void addRequiredSkillToJob(Job job, Skill skill);

    void addWantedSkillToJob(Job job, Skill skill);

    Set<Job> getJobsForOrganisation(Organisation organisation);
}
