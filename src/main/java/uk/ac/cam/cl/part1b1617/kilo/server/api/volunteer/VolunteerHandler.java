package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import java.util.ArrayList;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class VolunteerHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(VolunteerHandler.class);

	protected static class VolunteerResult {
		public String email;
		public String fullName;

		public VolunteerResult(Volunteer volunteer) {
			this.email = volunteer.user.email;
			this.fullName = volunteer.user.fullName;
		}
	}

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");

		Control control = ControlFactory.makeControl();
		User user;
		try {
			user = control.getUserForEmail(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}
		Volunteer volunteer = user.volunteer;
		if (volunteer == null) {
			throw new NotFoundResponse(email);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.get(context, volunteer))
				.put(() -> this.update(context, volunteer, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void get(Context context, Volunteer volunteer) {
		context.getResponse().status(200);
		context.render(Jackson.json(new VolunteerResult(volunteer)));
	}

	private void update(Context context, Volunteer volunteer, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			Volunteer loggedIn = LoginUtils.getLoggedInVolunteer(control, context, sessionData);

			if (!volunteer.user.email.equals(loggedIn.user.email)) {
				logger.warn("Forbidden attempt to edit {} by {}", volunteer.user.email, loggedIn.user.email);
				throw new ForbiddenResponse("You are not authorized to update this volunteer");
			}

			Promise<ProfileRequest> profile = context.parse(
				Jackson.fromJson(ProfileRequest.class)
			);
			profile.then(p -> {
				// Validation
				p.validate();

				Long maxDist;
				try {
					maxDist = Long.parseLong(p.dist);
				} catch (NumberFormatException e) {
					maxDist = 30L;// Default max distance
				}

				ArrayList<String> passwordWithHashNew = HashPassword.generatePasswordHash(p.password);
				volunteer.user.passwordHash = passwordWithHashNew.get(0);
				volunteer.user.passwordSalt = passwordWithHashNew.get(1);
				logger.info("Updated password for {}", volunteer.user.email);

				if (!volunteer.user.email.equals(p.email)) {
					logger.info("Updated volunteer {} to {}", volunteer.user.email, p.email);
					volunteer.user.email = p.email;
				}
				if (!volunteer.user.fullName.equals(p.fullname)) {
					logger.info("Updated volunteer {} name to {}", volunteer.user.email, p.fullname);
					volunteer.user.fullName = p.fullname;
				}
				if (!volunteer.phone.equals(p.phone)) {
					logger.info("Updated volunteer {} phone to {}", volunteer.user.email, p.phone);
					volunteer.phone = p.phone;
				}
				if (volunteer.maxDistancePreferenceKm != maxDist) {
					logger.info("Updated volunteer {} max distance to {}", volunteer.user.email, maxDist);
					volunteer.maxDistancePreferenceKm = maxDist;
				}
				AddressLocation address = new AddressLocation(
					p.address1,
					p.address2,
					p.city,
					p.county,
					p.postcode,
					p.country
				);
				volunteer.location = address.getGPSLocation();
				volunteer.skills = new HashSet<>();
				control.addTimesToVolunteer(volunteer, p.times);
				control.addSkillsToVolunteer(volunteer, p.skills);

				control.updateVolunteer(volunteer);
				context.getResponse().status(200);
				context.render(Jackson.json(new VolunteerResult(volunteer)));
			});
		});
	}
}
