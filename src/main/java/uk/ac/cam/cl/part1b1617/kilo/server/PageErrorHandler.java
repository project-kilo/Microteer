package uk.ac.cam.cl.part1b1617.kilo.server;

import static ratpack.handlebars.Template.handlebarsTemplate;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableSet;

import ratpack.error.ClientErrorHandler;
import ratpack.error.ServerErrorHandler;
import ratpack.handling.Context;

import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;

public class PageErrorHandler implements ClientErrorHandler, ServerErrorHandler {

	private static final Logger logger = LoggerFactory.getLogger(PageErrorHandler.class);

	private static final Set<Integer> RENDERABLE_ERROR_CODES = ImmutableSet.of(
		400, 401, 403, 404
	);

	@Override
	public void error(Context context, int statusCode) throws Exception {
		context.getResponse().status(statusCode);
		if (!RENDERABLE_ERROR_CODES.contains(statusCode)) {
			statusCode = 400;
		}
		context.render(handlebarsTemplate(String.format("errors/%d.html", statusCode)));
	}

	@Override
	public void error(Context context, Throwable throwable) throws Exception {
		if (throwable instanceof ErrorResponse) {
			ErrorResponse error = (ErrorResponse) throwable;
			this.error(context, error.responseCode());
		} else {
			logger.error("Unexpected error: {}", throwable.getMessage(), throwable);
			context.getResponse().status(500);
			context.render(handlebarsTemplate("errors/500.html"));
		}
	}
}
