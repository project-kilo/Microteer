package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;

public class Geocode implements Serializable
{

    private static final long serialVersionUID = 1L;

    /**
     * @param address to geocode. the address can be a full street address, or just a city,
     * country, or postal code.
     * @return Coordinates representing the location of the address in GPS coordinates
     * @throws IOException, JSONException
     */
    public static GPSLocation geocode(final String address) throws IOException, JSONException
    {
    	String url = encode(address);
    	System.out.println(url);
    	JSONObject json = JsonReader.readJsonFromUrl(url);
    	Object longitude = json.get("lon");
    	Object latitude = json.get("lat");
    	String lonString = String.valueOf(longitude);
    	String latString = String.valueOf(latitude);
    	Double lonDouble = Double.valueOf(lonString);
    	Double latDouble = Double.valueOf(latString);
        return new GPSLocation(latDouble,lonDouble);
    }
    
    /**
     * builds the geo-coding url
     *
     * @param address query portion of the url
     * @return a url string that includes the address query as well as the rest of the url specification 
     */
    private static String encode(final String address)
    {
        return "http://nominatim.openstreetmap.org/search/" + urlEncode(address) + 
        		"?format=json&limit=1";
    }

    /**
     * url-encode a string
     *
     * @param value
     * @return url-encoded string
     */
    private static String urlEncode(final String value)
    {
        try
        {
            return URLEncoder.encode(value, "UTF-8").replace("+", "%20");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
