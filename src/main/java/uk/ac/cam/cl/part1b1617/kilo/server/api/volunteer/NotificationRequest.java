package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificationRequest {
	public String subject;
	public String body;

	public NotificationRequest(
		@JsonProperty("subject") String subject,
		@JsonProperty("body") String body
	) {
		this.subject = subject;
		this.body = body;
	}
}
