package uk.ac.cam.cl.part1b1617.kilo.server;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.control.ControlImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.JobManagerService;
import uk.ac.cam.cl.part1b1617.kilo.graph.JobManagerServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.MatchingService;
import uk.ac.cam.cl.part1b1617.kilo.graph.MatchingServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.NotificationService;
import uk.ac.cam.cl.part1b1617.kilo.graph.NotificationServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.SkillService;
import uk.ac.cam.cl.part1b1617.kilo.graph.SkillServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.TimePeriodService;
import uk.ac.cam.cl.part1b1617.kilo.graph.TimePeriodServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.UserManager;
import uk.ac.cam.cl.part1b1617.kilo.graph.UserManagerService;

public class ControlFactory {
	
	//All other handlers should use this method to make a control module for interacting with the database
	  public static Control makeControl(){
		  org.neo4j.ogm.session.Session currentSession = Main.database.openSession();
		  JobManagerService js = new JobManagerServiceImpl(currentSession);
		  MatchingService ms = new MatchingServiceImpl(currentSession);
		  UserManagerService us = new UserManager(currentSession);
		  SkillService ss = new SkillServiceImpl(currentSession);
		  TimePeriodService ts = new TimePeriodServiceImpl(currentSession);
		  NotificationService ns = new NotificationServiceImpl(currentSession);
		  Control control = new ControlImpl(js, ms, us, ss, ts, ns);
		  return control;
	  }
}
