package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = VolunteerSkillEdge.TYPE)
public class VolunteerSkillEdge {
    public static final String TYPE = "HAS";

    Long id;

    @StartNode
    public Volunteer volunteer;
    @EndNode
    public Skill skill;

    public double confidence;

    public VolunteerSkillEdge() {}

    public VolunteerSkillEdge(Volunteer v, Skill s, double c) {
        volunteer = v;
        skill = s;
        confidence = c;
    }
}
