package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

public abstract class Entity {
    Long id;

    public Long getId() {
        return id;
    }
}
