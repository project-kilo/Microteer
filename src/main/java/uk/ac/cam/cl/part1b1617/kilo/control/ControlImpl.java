package uk.ac.cam.cl.part1b1617.kilo.control;

import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.ConvertTime;
import uk.ac.cam.cl.part1b1617.kilo.graph.*;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;
import uk.ac.cam.cl.part1b1617.kilo.recommender.EAction;
import uk.ac.cam.cl.part1b1617.kilo.recommender.Recommender;
import uk.ac.cam.cl.part1b1617.kilo.server.api.TimeMatrix;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;

public class ControlImpl implements Control {

    protected JobManagerService jobManagerService;
    protected UserManagerService userManagerService;
    protected SkillService skillService;
    protected MatchingService matchingService;
    protected TimePeriodService timePeriodService;
    protected NotificationService notificationService;

    public ControlImpl(JobManagerService jobManagerService, MatchingService matchingService,
                       UserManagerService userManagerService, SkillService skillService,
                       TimePeriodService timePeriodService, NotificationService notificationService) {
        this.jobManagerService = jobManagerService;
        this.userManagerService = userManagerService;
        this.skillService = skillService;
        this.matchingService = matchingService;
        this.timePeriodService = timePeriodService;
        this.notificationService = notificationService;
    }


    @Override
    public User getUserForEmail(String email) throws EntryNotFoundException{
        return userManagerService.getUserForEmail(email);
    }

    @Override
    public void addVolunteerWithoutTimes(Volunteer v, List<String> skills) throws DuplicateEntryException{
        
        userManagerService.addVolunteer(v);

        addSkillsToVolunteer(v, skills);

        Iterable<Job> nearbyJobs = matchingService.getMatchingJobs(v);

        for (Job job : nearbyJobs) {
            double score = Recommender.score(v, job);
            if (score > 0) {
                jobManagerService.suggestJob(job, v, score);
            }
        }
    }
    
    @Override
    public void addVolunteer(Volunteer v, List<String> skills, Set<Time> times) throws DuplicateEntryException{
        v.times = times;
        
        userManagerService.addVolunteer(v);

        addSkillsToVolunteer(v, skills);

        Iterable<Job> nearbyJobs = matchingService.getMatchingJobs(v);

        for (Job job : nearbyJobs) {
            double score = Recommender.score(v, job);
            if (score > 0) {
                jobManagerService.suggestJob(job, v, score);
            }
        }
    }

    @Override
    public void addVolunteer(Volunteer volunteer, List<String> skills, TimeMatrix matrix) throws DuplicateEntryException {
        this.addVolunteer(volunteer, skills, ConvertTime.convertTimes(timePeriodService, matrix));
    }

    @Override
    public void addTimesToVolunteer(Volunteer volunteer, TimeMatrix matrix) {
        volunteer.times = ConvertTime.convertTimes(timePeriodService, matrix);
    }

    @Override
    public void updateVolunteer(Volunteer v){
        userManagerService.updateVolunteer(v);
        rematchVolunteer(v);
    }

    @Override
    public void updateVolunteerNoAlloc(Volunteer v) {
        userManagerService.updateVolunteer(v);
    }

    @Override
    public void removeVolunteer(Volunteer v){
        userManagerService.removeVolunteer(v);
    }

    @Override
    public Set<Job> getMatches(Volunteer v){
        //todo return matches based on how good of a match they are
        Set<Job> result = new HashSet<>();

        for (JobAllocationEdge jobAllocationEdge : v.jobs.allocated) {
            Job j = jobManagerService.loadJob(jobAllocationEdge.job);
            result.add(j);
        }
        return result;
    }

    @Override
    public Set<Job> getCompletedJobs(Volunteer volunteer) {
        Set<Job> result = new HashSet<>();
        for (Job job : volunteer.jobs.completed) {
            Job j = jobManagerService.loadJob(job);
            result.add(j);
        }
        return result;
    }

    @Override
    public Set<Job> getAcceptedJobs(Volunteer volunteer) {
        Set<Job> result = new HashSet<>();
        for (Job job : volunteer.jobs.accepted) {
            Job j = jobManagerService.loadJob(job);
            result.add(j);
        }
        return result;
    }

    @Override
    public Iterable<Job> searchJobs(Volunteer volunteer, String search) {
        return this.matchingService.getMatchingJobs(
            volunteer,
            new Filter("description", ComparisonOperator.LIKE, "*"+search+"*")
        );
    }

    @Override
    public Set<Volunteer> getAcceptedVolunteers(Job job) {
        Set<Volunteer> result = new HashSet<>();
        for (VolunteerJobs volunteerJobs : job.acceptedVolunteers) {
            Volunteer volunteer = userManagerService.loadVolunteerFrom(volunteerJobs);
            result.add(volunteer);
        }
        return result;
    }

    @Override
    public Set<Volunteer> getCompletedVolunteers(Job job) {
        Set<Volunteer> result = new HashSet<>();
        for (VolunteerJobs volunteerJobs : job.completedVolunteers) {
            Volunteer volunteer = userManagerService.loadVolunteerFrom(volunteerJobs);
            result.add(volunteer);
        }
        return result;
    }

    @Override
    public void acceptJob(Job job, Volunteer volunteer ) {
        Recommender.updateVolunteerOnAction(volunteer, job, EAction.ACCEPT);
        jobManagerService.acceptJob(job, volunteer);

        Notification notification = new Notification();
        notification.subject = "Job Acceptance";
        notification.body = String.format(
            "%s has accepted your job \"%s\"",
            volunteer.user.fullName,
            job.summary
        );
        notification.from = volunteer.user;
        notification.involving = job;
        this.sendNotificationToMany(notification, this.userManagerService.getAdmins(job.owner));
    }

    @Override
    public void unacceptJob(Job j, Volunteer v){
        double fitness = Recommender.score(v, j);
        jobManagerService.unacceptJob(j, v, fitness);

        Notification notification = new Notification();
        notification.subject = "Job Unacceptance";
        notification.body = String.format(
            "%s has unaccepted your job \"%s\"",
            v.user.fullName,
            j.summary
        );
        notification.from = v.user;
        notification.involving = j;
        this.sendNotificationToMany(notification, this.userManagerService.getAdmins(j.owner));
    }

    @Override
    public void rejectJob(Job job, Volunteer volunteer, MatchQuality matchQuality) {
        if (matchQuality == MatchQuality.BAD_MATCH) {
            Recommender.updateVolunteerOnAction(volunteer, job, EAction.REJECT);
        }
        jobManagerService.rejectJob(job, volunteer);
    }

    @Override
    public Organisation getOrganisation(String email) throws EntryNotFoundException {
        return this.userManagerService.getOrganisation(email);
    }

    @Override
    public void addJob(Job j, Organisation organisation, List<String> skillsRequired, List<String> skillsWanted){
        jobManagerService.addJob(j, organisation);

        j = jobManagerService.loadJob(j);

        addSkillsToJob(j, skillsRequired, skillsWanted);

        Iterable<Volunteer> nearbyVolunteers = matchingService.getMatchingVolunteers(j);

        for (Volunteer volunteer : nearbyVolunteers) {
            double score = Recommender.score(volunteer, j);
            if (score > 0) {
                jobManagerService.suggestJob(j, volunteer, score);
            }
        }
    }

    @Override
    public Job getJob(UUID id) throws EntryNotFoundException {
        return this.jobManagerService.getJob(id);
    }

    @Override
    public Iterable<User> getAdmins(Organisation organisation) {
        return this.userManagerService.getAdmins(organisation);
    }

    @Override
    public void addOrganisation(Organisation organisation) throws DuplicateEntryException{
        userManagerService.addOrganisation(organisation);
    }

    @Override
    public void updateOrganisation(Organisation o){
        userManagerService.updateOrganisation(o);
    }

    @Override
    public void updateJob(Job j){
        this.updateJobNoAlloc(j);
        rematchJob(j);
    }

    @Override
    public void updateJobNoAlloc(Job j) {
        jobManagerService.updateJob(j);

        Notification notification = new Notification();
        notification.subject = "Job Update";
        notification.body = String.format(
            "Job \"%s\" has been updated",
            j.summary
        );
        notification.involving = j;

        Iterator<User> users = j.acceptedVolunteers.stream()
            .map(vj -> this.userManagerService.loadVolunteerFrom(vj).user)
            .iterator();
        this.sendNotificationToMany(notification, () -> users);
    }

    @Override
    public void removeJob(Job j){
        Notification notification = new Notification();
        notification.subject = "Job Deleted";
        notification.body = String.format(
            "Job \"%s\" has been deleted",
            j.summary
        );
        notification.involving = j;

        Iterator<User> users = j.acceptedVolunteers.stream()
            .map(vj -> this.userManagerService.loadVolunteerFrom(vj).user)
            .iterator();
        this.sendNotificationToMany(notification, () -> users);

        jobManagerService.removeJob(j);
    }

    @Override
    public Set<Job> getOwnedJobs(Organisation o){
        return jobManagerService.getJobsForOrganisation(o);
    }

    @Override
    public void addOrganisationUser(User user, Organisation organisation) throws DuplicateEntryException {
        user.adminOf = organisation;
        userManagerService.addOrganisationUser(user);
    }

    @Override
    public void removeOrganisationUser(User user) {
        userManagerService.removeOrganisationUser(user);
    }

    @Override
    public void removeOrganisation(Organisation organisation) {
        userManagerService.removeOrganisation(organisation);
    }

    @Override
    public void updateOrganisationUser(User user) {
        userManagerService.updateOrganisationUser(user);
    }

    @Override
    public void jobCompleted(Volunteer volunteer, Job job, int rating) {
        jobManagerService.jobCompleted(volunteer, job, rating);
    }

    @Override
    public void addSkillsToJob(Job job, List<String> skillsRequired, List<String> skillsWanted) {
        for (String s : skillsRequired) {
            Skill skill = skillService.getSkillForSkillName(s);
            if (skill == null) {
                skillService.addSkill(s);
                skill = skillService.getSkillForSkillName(s);
            }
            jobManagerService.addRequiredSkillToJob(job, skill);
        }

        for (String s : skillsWanted) {
            Skill skill = skillService.getSkillForSkillName(s);
            if (skill == null) {
                skillService.addSkill(s);
                skill = skillService.getSkillForSkillName(s);
            }
            jobManagerService.addWantedSkillToJob(job, skill);
        }
    }

    @Override
    public Time getTimeEntity(OffsetDateTime date) {
        TimePeriod period;
        switch (date.getHour()) {
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                period = TimePeriod.MORNING;
                break;
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                period = TimePeriod.AFTERNOON;
                break;
            default:
                period = TimePeriod.EVENING;
                break;
        }

        return timePeriodService.getTimeEntity(date.getDayOfWeek(), period);
    }

    @Override
    public void rateJob(Volunteer volunteer, Job job, int rating) {
        job = jobManagerService.loadJob(job);
        jobManagerService.rateJob(job, rating);
    }

    @Override
    public void addSkillsToVolunteer(Volunteer volunteer, List<String> skills) {
        for (String s : skills) {
            Skill skill = skillService.getSkillForSkillName(s);
            VolunteerSkillEdge volunteerSkillEdge = new VolunteerSkillEdge();
            volunteerSkillEdge.skill = skill;
            volunteerSkillEdge.volunteer = volunteer;
            volunteerSkillEdge.confidence = 1;

            volunteer.skills.add(volunteerSkillEdge);
            skillService.addVolunteerSkillEdge(volunteerSkillEdge);
        }
    }

    private void rematchJob(Job j) {
        Iterable<Volunteer> nearbyVolunteers = matchingService.getMatchingVolunteers(j);
        j.allocatedVolunteers = new HashSet<>();

        for (Volunteer volunteer : nearbyVolunteers) {
            if (!j.completedVolunteers.contains(volunteer.jobs) && !j.acceptedVolunteers.contains(volunteer.jobs)) {
                double score = Recommender.score(volunteer, j);
                if (score > 0) {
                    Notification notification = new Notification();
                    notification.subject = "Job Match";
                    notification.body = String.format(
                        "You've matched a new job \"%s\"",
                        j.summary
                    );
                    notification.involving = j;
                    notification.to = volunteer.user;
                    notificationService.addNotification(notification);

                    jobManagerService.suggestJob(j, volunteer, score);
                }
            }
        }
    }

    private void rematchVolunteer(Volunteer v) {
        Iterable<Job> nearbyJobs = matchingService.getMatchingJobs(v);
        v.jobs.allocated = new HashSet<>();

        for (Job job : nearbyJobs) {
            if (!v.jobs.completed.contains(job) && !v.jobs.accepted.contains(job)) {
                double score = Recommender.score(v, job);
                if (score > 0) {
                    jobManagerService.suggestJob(job, v, score);
                }
            }
        }
    }

    @Override
    public void addNotification(Notification notification) {
        this.notificationService.addNotification(notification);
    }

    @Override
    public void removeNotification(Notification notification) {
        this.notificationService.removeNotification(notification);
    }

    @Override
    public Iterable<Notification> getReceivedNotifications(User user) {
        return this.notificationService.getReceived(user);
    }

    @Override
    public Notification getNotification(UUID id) throws EntryNotFoundException {
        return this.notificationService.get(id);
    }

    private void sendNotificationToMany(Notification template, Iterable<User> users) {
        for (User user : users) {
            Notification notification = new Notification();
            notification.submitted = template.submitted;
            notification.subject = template.subject;
            notification.body = template.body;
            notification.involving = template.involving;
            notification.from = template.from;
            notification.to = user;

            this.notificationService.addNotification(notification);
        }
    }
}
