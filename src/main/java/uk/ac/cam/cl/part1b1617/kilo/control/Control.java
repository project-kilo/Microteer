package uk.ac.cam.cl.part1b1617.kilo.control;

import uk.ac.cam.cl.part1b1617.kilo.graph.DuplicateEntryException;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;
import uk.ac.cam.cl.part1b1617.kilo.server.api.TimeMatrix;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface Control {

    /**
     * get the user object with the given email
     * @param email email of the user to find
     * @return user with said email
     * @throws EntryNotFoundException
     */
    User getUserForEmail (String email) throws EntryNotFoundException;

    //Volunteer operations
    /////////////////////////////

    /**
     * add volunteer to system
     * create matches between nearby jobs and this volunteer, making use of the keyword score provided by the keyword
     * matching module
     * @param volunteer volunteer to add to the system
     * @param skills the skills possessed by the volunteer
     * @throws DuplicateEntryException
     */
    void addVolunteer(Volunteer volunteer, List<String> skills, Set<Time> times) throws DuplicateEntryException;
    void addVolunteer(Volunteer volunteer, List<String> skills, TimeMatrix matrix) throws DuplicateEntryException;

    void addVolunteerWithoutTimes(Volunteer volunteer, List<String> skills) throws DuplicateEntryException;

    void addTimesToVolunteer(Volunteer volunteer, TimeMatrix matrix);
    
    /**
     * update volunteer information and trigger reallocation on system
     * @param volunteer new volunteer object to replace the old one
     */
    void updateVolunteer(Volunteer volunteer);

    /**
     * update volunteer information that has no effect on allocation
     * @param volunteer new volunteer object to replace the old one
     */
    void updateVolunteerNoAlloc(Volunteer volunteer);

    /**
     * remove volunteer and related matches from system
     * @param volunteer volunteer to remove
     */
    void removeVolunteer(Volunteer volunteer);

    /**
     * return the jobs that the volunteer has been matched to and the jobs are still available
     * @param volunteer volunteer's matches you want
     * @return an empty set if no jobs are found
     */
    Set<Job> getMatches (Volunteer volunteer);

    /**
     * get the completed jobs for the given volunteer
     * @param volunteer the volunteer whose completed jobs you want
     * @return the jobs completed by the volunteer
     */
    Set<Job> getCompletedJobs(Volunteer volunteer);

    /**
     * get the jobs accepted by the given volunteer
     * @param volunteer the volunteer whose accepted jobs you want
     * @return the jobs accepted by the volunteer
     */
    Set<Job> getAcceptedJobs(Volunteer volunteer);

    /**
     * get jobs for a volunteer that match a search term
     * @param volunteer the volunteer to match spatially and with available time
     * @param search the term to search for in the job description, case insensitive
     * @return the matching jobs
     */
    Iterable<Job> searchJobs(Volunteer volunteer, String search);

    /**
     * update the matching to show that the match has been accepted
     * notify organisation of accept
     * @param job job to be accepted
     * @param volunteer volunteer accepting the job
     */
    void acceptJob(Job job, Volunteer volunteer);

    /**
     * update the matching to show that the match is no longer accepted, but not declined
     * (volunteer may be busy with something else / is doing another job)
     * notify organisation of un-accept
     * @param job job being unaccepted
     * @param volunteer volunteer unaccepting a job
     */
    void unacceptJob(Job job, Volunteer volunteer);

    /**
     * remove match between the job and the volunteer and update profile
     * @param job job being unmatched
     * @param volunteer volunteer unmatching the job
     * @param matchQuality whether the volunteer has the proper skills despite rejecting the job
     */
    void rejectJob(Job job, Volunteer volunteer, MatchQuality matchQuality);

    /**
     * add skills to a volunteer
     * @param volunteer volunteer who is having skills added
     * @param skills the skills to add to the volunteer
     */
    void addSkillsToVolunteer(Volunteer volunteer, List<String> skills);

    /**
     * rate a job that the volunteer has completed
     * @param volunteer the volunteer rating the job
     * @param job the job being rated
     * @param rating the rating as an integer from 1 to 5
     */
    void rateJob(Volunteer volunteer, Job job, int rating);



    //Organisation operations
    /////////////////////////////

    /**
     * get an organisation from its email
     * @param email the email of the organisation
     * @return the organisation
     */
    Organisation getOrganisation(String email) throws EntryNotFoundException;

    /**
     * add job to system
     * create matches between nearby volunteers and this job, making use of the keyword score provided by the keyword
     * matching module
     * @param job the job to add
     * @param organisation the organisation the job belongs to
     * @param skillsRequired the skills required for the job
     * @param skillsWanted the skills wanted for the job
     */
    void addJob(Job job, Organisation organisation, List<String> skillsRequired, List<String> skillsWanted);

    /**
     * get job by ID
     * @param id the job's UUID
     */
    Job getJob(UUID id) throws EntryNotFoundException;

    /**
     * get all admins of an organisation
     * @param organisation the organisation
     * @return an iterable over all admin users
     */
    Iterable<User> getAdmins(Organisation organisation);

    /**
     * add an organisation to the system
     * @param organisation organisation to be added
     * @throws DuplicateEntryException
     */
    void addOrganisation(Organisation organisation) throws DuplicateEntryException;

    /**
     * update organisation information
     * @param organisation new organisation object
     */
    void updateOrganisation(Organisation organisation);

    /**
     * remove an organisation
     * @param organisation the organisation to remove
     */
    void removeOrganisation(Organisation organisation);

    /**
     * update the job and trigger reallocation on the system
     * @param job new job object
     */
    void updateJob(Job job);

    /**
     * update the job without reallocating matches
     * @param job new job object
     */
    void updateJobNoAlloc(Job job);

    /**
     * remove job and related matches from graph
     * @param job job to remove
     */
    void removeJob(Job job);

    /**
     * return the jobs that this organisation has created
     * @param organisation organisation's jobs you want
     * @return the jobs belonging to this organisation
     */
    Set<Job> getOwnedJobs(Organisation organisation);

    /**
     * add an organisation user
     * @param user user being added
     * @param organisation organisation the user belongs to
     * @throws DuplicateEntryException
     */
    void addOrganisationUser(User user,Organisation organisation) throws DuplicateEntryException;

    /**
     * remove an organisation user
     * @param user user to remove
     */
    void removeOrganisationUser(User user);

    /**
     * update an organisation user
     * @param user new organisation user
     */
    void updateOrganisationUser(User user);

    /**
     * called by the organisation when the job has been completed
     * remove allocation and accepted edges
     * update ratings
     * @param volunteer volunteer that completed the job
     * @param job job that was completed
     * @param rating integer rating out of 5
     */
    void jobCompleted(Volunteer volunteer, Job job, int rating);

    /**
     * add required and wanted skills to a job
     * @param job job who is having skills added
     * @param skillsRequired the skills required
     * @param skillsWanted the skills desired but not required
     */
    void addSkillsToJob(Job job, List<String> skillsRequired, List<String> skillsWanted);


    /**
     * get the volunteers who have accepted a given job
     * @param job the job whose accepted volunteers you want
     * @return the volunteers who have accepted the job
     */
    Set<Volunteer> getAcceptedVolunteers(Job job);

    /**
     * get the volunteers who have completed a given job
     * @param job the job whose completed volunteers you want
     * @return the volunteers who have completed the job
     */
    Set<Volunteer> getCompletedVolunteers(Job job);

    /**
     * get a time period entity given a date
     * @param date the date to retrieve
     * @return the time entity corresponding to the date
     */
    Time getTimeEntity(OffsetDateTime date);

    void addNotification(Notification notification);
    void removeNotification(Notification notification);
    Iterable<Notification> getReceivedNotifications(User user);
    Notification getNotification(UUID id) throws EntryNotFoundException;
}
