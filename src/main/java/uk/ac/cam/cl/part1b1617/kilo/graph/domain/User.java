package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.time.Instant;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import uk.ac.cam.cl.part1b1617.kilo.graph.InstantConverter;

@NodeEntity
public class User extends Entity {

    @Index(primary=true, unique=true) public String email;

    public String passwordHash;
    public String passwordSalt;
    @Convert(InstantConverter.class)
    public Instant passwordLastChanged;
    @Convert(InstantConverter.class)
    public Instant signUp;
    public String fullName;

    @Relationship(type = "VOLUNTEER", direction = Relationship.OUTGOING)
    public Volunteer volunteer;
    @Relationship(type = "ADMIN_OF", direction = Relationship.OUTGOING)
    public Organisation adminOf;

    public User(String passwordHash, String passwordSalt, Instant signUp, String fullName, String email, Organisation organisation) {
        this(passwordHash, passwordSalt, signUp, fullName, email);
        this.adminOf = organisation;
    }

    public User(String passwordHash, String passwordSalt, Instant signUp, String fullName, String email, Volunteer volunteer) {
        this(passwordHash, passwordSalt, signUp, fullName, email);
        this.volunteer = volunteer;
    }

    public User(String passwordHash, String passwordSalt, Instant signUp, String fullName, String email) {
        this.email = email;
        this.fullName = fullName;
        this.passwordHash = passwordHash;
        this.passwordSalt = passwordSalt;
        this.signUp = signUp;
        this.passwordLastChanged = signUp;
    }

    public User() {
        super();
    }
}
