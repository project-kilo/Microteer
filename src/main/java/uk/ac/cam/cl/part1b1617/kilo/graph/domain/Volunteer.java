package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;
import uk.ac.cam.cl.part1b1617.kilo.graph.GPSLocationConverter;

@NodeEntity
public class Volunteer extends Entity {

	@Convert(GPSLocationConverter.class)
	public GPSLocation location;
	public long maxDistancePreferenceKm;
	public String phone;
	public Double rating = 0.0;
	public int numberOfRatings = 0;

	@Relationship(type = "VOLUNTEER", direction = Relationship.INCOMING)
	public User user;

	@Relationship(type = "JOBS", direction = Relationship.OUTGOING)
	public VolunteerJobs jobs;

	@Relationship(type = "AVAILABLE", direction = Relationship.OUTGOING)
	public Set<Time> times = new HashSet<>();

	@Relationship(type = VolunteerSkillEdge.TYPE, direction = Relationship.OUTGOING)
	public Set<VolunteerSkillEdge> skills = new HashSet<>();

	public Volunteer() {
	}

	//TODO GPSLocation vs AddressLocation
	public Volunteer(String email, String fullName, Instant signUp, String passwordHash, String passwordSalt,
			GPSLocation gpsLocation, long maxDistancePreferenceKm, String phone) {
		this.location = gpsLocation;
		this.maxDistancePreferenceKm = maxDistancePreferenceKm;
		this.phone = phone;
		user = new User(passwordHash, passwordSalt, signUp, fullName, email, this);
	}

	//TODO GPSLocation vs AddressLocation
	public Volunteer(String email, String fullName, Instant signUp, String passwordHash,
			String passwordSalt, GPSLocation gpsLocation, long maxDistancePreferenceKm, 
			String phone, Set<VolunteerSkillEdge> skills) {
		this.location = gpsLocation;
		this.maxDistancePreferenceKm = maxDistancePreferenceKm;
		this.phone = phone;
		this.skills = skills;
		user = new User(passwordHash, passwordSalt, signUp, fullName, email, this);
	}

	public JobAllocationEdge getJobAllocationEdgeFor(Job job) {
		for (JobAllocationEdge jobAllocationEdge : this.jobs.allocated) {
			if (jobAllocationEdge.job == job) {
				return jobAllocationEdge;
			}
		}
		return null;
	}
}


