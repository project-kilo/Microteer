package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobResult;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class AllocatedRootHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(AllocatedRootHandler.class);

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");

		Control control = ControlFactory.makeControl();
		User user;
		try {
			user = control.getUserForEmail(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}
		Volunteer volunteer = user.volunteer;
		if (volunteer == null) {
			throw new NotFoundResponse(email);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.list(context, volunteer, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void list(Context context, Volunteer volunteer, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			Volunteer loggedIn = LoginUtils.getLoggedInVolunteer(control, context, sessionData);

			if (!volunteer.user.email.equals(loggedIn.user.email)) {
				logger.warn("Forbidden attempt to view jobs for {} by {}", volunteer.user.email, loggedIn.user.email);
				throw new ForbiddenResponse("You are not authorized to view this resource");
			}

			Set<Job> jobs = control.getMatches(volunteer);
			List<JobResult> results = jobs.stream()
				.map(j -> new JobResult(j)).collect(Collectors.toList());

			context.render(Jackson.json(results));
			context.getResponse().status(200);
		});
	}
}
