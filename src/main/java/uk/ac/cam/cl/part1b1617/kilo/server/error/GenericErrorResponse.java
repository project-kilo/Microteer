package uk.ac.cam.cl.part1b1617.kilo.server.error;

public class GenericErrorResponse extends ErrorResponse {

	private static final long serialVersionUID = 1L;

	public int responseCode;

	public GenericErrorResponse(String message, int responseCode) {
		super(message);
		this.responseCode = responseCode;
	}

	public int responseCode() {
		return this.responseCode;
	}
}
