package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = JobAllocationEdge.TYPE)
public class JobAllocationEdge {
    public static final String TYPE = "ALLOCATED";

    Long id;

    @StartNode
    public VolunteerJobs volunteerJobs;
    @EndNode
    public Job job;

    public double fitness;

    public JobAllocationEdge(Job job, VolunteerJobs volunteerJobs, double fitness) {
        this.job = job;
        this.volunteerJobs = volunteerJobs;
        this.fitness = fitness;
    }

    public JobAllocationEdge() {
        this.volunteerJobs = null;
        this.job = null;
        this.fitness = 0.0;
    }

    public Long getId() {
        return id;
    }
}
