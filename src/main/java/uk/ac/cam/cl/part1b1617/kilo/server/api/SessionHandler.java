package uk.ac.cam.cl.part1b1617.kilo.server.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

import ratpack.exec.Promise;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ServerError;
import uk.ac.cam.cl.part1b1617.kilo.server.error.UnauthorizedResponse;

public class SessionHandler implements Action<Chain> {
	private static final Logger logger = LoggerFactory.getLogger(SessionHandler.class);

	protected static class LoginRequest {
		public String email;
		public String password;

		public LoginRequest(
			@JsonProperty("email") String email,
			@JsonProperty("password") String password
		) {
			this.email = email;
			this.password = password;
		}
	}

	protected static class LoginResponse {
		public boolean loggedIn = true;
		public boolean isVolunteer;
		public String organisation;
		public String email;

		public LoginResponse(String email) {
			this.email = email;
			this.isVolunteer = true;
			this.organisation = "";
		}

		public LoginResponse(String email, String organisation) {
			this.email = email;
			this.isVolunteer = false;
			this.organisation = organisation;
		}
	}

	protected static class LogoutResponse {
		public boolean loggedIn = false;
	}

	@Override
	public void execute(Chain chain) throws Exception {
		chain
			.post(ctx -> this.login(ctx))
			.delete("session", ctx -> this.logout(ctx))
		;
	}

	private void logout(Context context) {
		context.get(Session.class).getData().then(sessionData -> {
			sessionData.remove("name");
			context.getResponse().status(200);
			context.render(Jackson.json(new LogoutResponse()));
		});
	}

	private void login(Context context) {
		Promise<LoginRequest> request = context.parse(
			Jackson.fromJson(LoginRequest.class)
		);

		request.then(r -> {
			Control control = ControlFactory.makeControl();

			User user;
			try {
				user = control.getUserForEmail(r.email);
				if (!HashPassword.checkPassword(
					r.password,
					user.passwordHash,
					user.passwordSalt
				)) {
					logger.info("Failed login for {}", user.email);
					throw new UnauthorizedResponse();
				}
			} catch (EntryNotFoundException e) {
				logger.info("Failed login, no such user {}", r.email);
				throw new UnauthorizedResponse();
			}

			context.get(Session.class).getData().then(sessionData -> {
				sessionData.set("name", user.email);
				if (user.volunteer != null) {
					context.getResponse().status(200);
					context.render(Jackson.json(new LoginResponse(user.email)));
				} else if (user.adminOf != null) {
					context.getResponse().status(200);
					context.render(Jackson.json(new LoginResponse(user.email, user.adminOf.email)));
				} else {
					// we should never get here
					logger.error("User {} is not a volunteer or admin", user.email);
					throw new ServerError("User is not a volunteer or admin");
				}
			});
		});
	}
}
