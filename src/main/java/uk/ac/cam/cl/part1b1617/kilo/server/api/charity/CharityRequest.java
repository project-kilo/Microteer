package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import static uk.ac.cam.cl.part1b1617.kilo.UIValidation.ValidationWithExceptions.*;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CharityRequest {
	public String orgname;
	public String orgemail;

	public String postcode;
	public String address1;
	public String address2;
	public String city;
	public String county;
	public String country;
	public String website;
	public String phone;
	public String description;

	public String adminName;
	public String adminEmail;
	public String adminPassword;

	public CharityRequest(
		@JsonProperty("orgname") String orgname,
		@JsonProperty("orgemail") String orgemail,
		@JsonProperty("postcode") String postcode,
		@JsonProperty("address1") String address1,
		@JsonProperty("address2") String address2,
		@JsonProperty("city") String city,
		@JsonProperty("county") String county,
		@JsonProperty("country") String country,
		@JsonProperty("phone") String phone,
		@JsonProperty("website") String website,
		@JsonProperty("description") String description,
		@JsonProperty("adminName") String adminName,
		@JsonProperty("adminEmail") String adminEmail,
		@JsonProperty("adminPassword") String adminPassword
	) {
		this.orgname = orgname;
		this.orgemail = orgemail;
		this.postcode = postcode;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.county = county;
		this.country = country;
		this.phone = phone;
		this.website = website;
		this.description = description;

		this.adminName = adminName;
		this.adminEmail = adminEmail;
		this.adminPassword = adminPassword;
	}

	public void validate() {
		validateOrgname(this.orgname);
		validateEmail(this.orgemail);
		validateFullAddress(
			this.address1,
			this.city,
			this.postcode,
			this.country
		);
		validatePhone(this.phone);
		validateDescription(this.description);
		validateWebsite(this.website);

		validateFullname(this.adminName);
		validateEmail(this.adminEmail);
		validatePassword(this.adminPassword);
	}
}
