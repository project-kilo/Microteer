package uk.ac.cam.cl.part1b1617.kilo.server.api.job;

import java.util.List;
import java.util.stream.Collectors;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.server.TimeUtils;

public class JobResult {
	public String summary;
	public String description;
	public AddressLocation location;
	public String expiration;
	public Long requiredVolunteers;

	public String uuid;

	public String owner;
	public List<String> requiredSkills;
	public List<String> wantedSkills;

	public JobResult(Job job) {
		this.summary = job.summary;
		this.description = job.description;
		this.location = job.location;
		this.expiration = TimeUtils.formatDate(job.expiration);
		this.requiredVolunteers = job.requiredVolunteers;

		this.uuid = job.uuid.toString();
		this.owner = job.owner.email;

		this.requiredSkills = job.requiredSkills.stream()
			.map(skill -> skill.skill).collect(Collectors.toList());
		this.wantedSkills = job.wantedSkills.stream()
			.map(skill -> skill.skill).collect(Collectors.toList());
	}
}
