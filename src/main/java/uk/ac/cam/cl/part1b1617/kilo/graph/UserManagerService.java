package uk.ac.cam.cl.part1b1617.kilo.graph;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;

public interface UserManagerService {

	/**
	 * find a user in the database
	 * @param email an alphanumeric string
	 * @return the User object from the database that matches the email address
	 * @throws EntryNotFoundException if a user with the given email address is not found
	 */
	User getUserForEmail(String email) throws EntryNotFoundException;

	/**
	 * adds the volunteer, if the volunteer is not already in the database
	 * @param volunteer the volunteer to be added
	 * @throws DuplicateEntryException if volunteer is already in the database
	 */
	void addVolunteer(Volunteer volunteer) throws DuplicateEntryException;

	/**
	 * get an organisation from the database
	 * @param email the email corresponding to the organisation
	 * @return the Organisation entity
	 */
	Organisation getOrganisation(String email) throws EntryNotFoundException;

	/**
	 * get the admins for an organisation
	 * @param organisation the organisation
	 * @return a set of administrator users
	 */
	Iterable<User> getAdmins(Organisation organisation);

	/**
	 * adds the organisation, if the organisation is not already in the database
	 * @param organisation the organisation to be added
	 * @throws DuplicateEntryException if organisation is already in the database
	 */
	void addOrganisation(Organisation organisation)throws DuplicateEntryException;

	/**
	 * updates the volunteer, if the volunteer's username already in the database
	 * @param volunteer the volunteer to be modify
	 */
	void updateVolunteer(Volunteer volunteer);

	/**
	 * updates the organisation, if the organisation username already in the database
	 * @param organisation the organisation to be modify
	 */
	void updateOrganisation(Organisation organisation);

	/**
	 * removes the volunteer, if the volunteer is found in the database
	 * @param volunteer volunteer to remove
	 */
	void removeVolunteer(Volunteer volunteer);

	/**
	 * removes the organisation, if the organisation is found in the database
	 * @param organisation organisation to remove
	 */
	void removeOrganisation(Organisation organisation);

	/**
	 * adds the organisation, if the organisation is not already in the database
	 * @param user the user to be added
	 * @throws DuplicateEntryException if organisation is already in the database
	 */
	void addOrganisationUser(User user) throws DuplicateEntryException;

	/**
	 * removes the organisation, if the organisation is in the database
	 * @param user the organisation to be added
	 */
	void removeOrganisationUser(User user);

	/**
	 * updates the organisation, if the organisation's username is not already in the database
	 * @param user the organisation to be added
	 */
	void updateOrganisationUser(User user);

	Volunteer loadVolunteerFrom(VolunteerJobs volunteerJobs);

}
