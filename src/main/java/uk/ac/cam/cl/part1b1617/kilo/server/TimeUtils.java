package uk.ac.cam.cl.part1b1617.kilo.server;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class TimeUtils {
	public static OffsetDateTime parseDateString(String string) {
		return OffsetDateTime.parse(string, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
	}

	public static String formatDate(TemporalAccessor date) {
		return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(date);
	}
}
