package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.util.UUID;

import org.neo4j.ogm.session.Session;

import com.google.common.collect.ImmutableMap;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Notification;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;

public class NotificationServiceImpl implements NotificationService{

	protected Session session;

	public NotificationServiceImpl(Session session) {
		this.session = session;
	}

	@Override
	public void addNotification(Notification notification) {
		//session.save(notification);
	}

	@Override
	public void removeNotification(Notification notification) {
		//session.delete(notification);
	}

	@Override
	public Iterable<Notification> getReceived(User user) {
		return this.session.query(
			Notification.class,
			"MATCH (u:User {email: {email}})<-[:TO]-(n:Notification) RETURN n",
			ImmutableMap.of(
				"email", user.email
			)
		);
	}

	@Override
	public Notification get(UUID id) throws EntryNotFoundException {
		Notification notification = this.session.load(Notification.class, id);
		if (notification == null) {
			throw new EntryNotFoundException(id.toString());
		}
		return notification;
	}
}
