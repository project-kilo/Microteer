package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import static uk.ac.cam.cl.part1b1617.kilo.UIValidation.ValidationWithExceptions.*;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import uk.ac.cam.cl.part1b1617.kilo.server.api.TimeMatrix;

public class ProfileRequest {
	public String fullname;
	public String password;
	public String email;
	public String postcode;
	public String address1;
	public String address2;
	public String city;
	public String county;
	public String country;
	public String phone;
	public String dist;
	public List<String> skills;
	public TimeMatrix times;

	public ProfileRequest(
		@JsonProperty("fullname") String fullname,
		@JsonProperty("password") String password,
		@JsonProperty("email") String email,
		@JsonProperty("postcode") String postcode,
		@JsonProperty("address1") String address1,
		@JsonProperty("address2") String address2,
		@JsonProperty("city") String city,
		@JsonProperty("county") String county,
		@JsonProperty("country") String country,
		@JsonProperty("phone") String phone,
		@JsonProperty("dist") String dist,
		@JsonProperty("skills") List<String> skills,
		@JsonProperty("times") TimeMatrix times
	) {
		this.fullname = fullname;
		this.password = password;
		this.email = email;
		this.postcode = postcode;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.county = county;
		this.country = country;
		this.phone = phone;
		this.dist = dist;
		this.skills = skills;
		this.times = times;

		if (this.skills == null) {
			this.skills = new ArrayList<>();
		}
		if (this.times == null) {
			this.times = new TimeMatrix(null, null, null, null, null, null, null);
		}
	}

	public void validate() {
		validateFullname(this.fullname);
		validatePassword(this.password);
		validateEmail(this.email);
		validateFullAddress(
			this.address1,
			this.city,
			this.postcode,
			this.country
		);
		validatePhone(this.phone);
	}
}
