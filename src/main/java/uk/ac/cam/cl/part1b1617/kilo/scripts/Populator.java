package uk.ac.cam.cl.part1b1617.kilo.scripts;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.neo4j.ogm.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;
import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.control.ControlImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.Database;
import uk.ac.cam.cl.part1b1617.kilo.graph.DatabaseImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.DuplicateEntryException;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.JobManagerService;
import uk.ac.cam.cl.part1b1617.kilo.graph.JobManagerServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.MatchingService;
import uk.ac.cam.cl.part1b1617.kilo.graph.MatchingServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.NotificationService;
import uk.ac.cam.cl.part1b1617.kilo.graph.NotificationServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.SkillService;
import uk.ac.cam.cl.part1b1617.kilo.graph.SkillServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.TimePeriodService;
import uk.ac.cam.cl.part1b1617.kilo.graph.TimePeriodServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.UserManager;
import uk.ac.cam.cl.part1b1617.kilo.graph.UserManagerService;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Time;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;

public class Populator {

	private final int NUM_VOLUNTEERS = 20;
	private final int NUM_JOBS = 50;

	protected Database database;

	protected JobManagerService jobManagerService;
	protected UserManagerService userManagerService;
	protected SkillService skillService;
	protected MatchingService matchingService;
	protected TimePeriodService timePeriodService;
	protected NotificationService notificationService;
	protected Control control;

	protected DayOfWeek[] days = DayOfWeek.values();
	protected TimePeriod[] timePeriods = TimePeriod.values();
	protected Random random = new Random();

	protected List<List<String>> skillsList = new ArrayList<>();
	protected List<String> skillList = new ArrayList<>();

	protected BufferedReader br;


	//populate the neo4j database with a test set of entities
	public static void main(String[] args) {
		Populator populator = new Populator();
		try {
			populator.setUp();
		} catch (DuplicateEntryException | EntryNotFoundException e) {
			e.printStackTrace();
		}
	}

	protected void setUp() throws DuplicateEntryException, EntryNotFoundException {
		database = new DatabaseImpl();
		database.purge();

		Session thisSession = database.openSession();

		this.jobManagerService = new JobManagerServiceImpl(thisSession);
		this.userManagerService = new UserManager(thisSession);
		this.skillService = new SkillServiceImpl(thisSession);
		this.matchingService = new MatchingServiceImpl(thisSession);
		this.timePeriodService = new TimePeriodServiceImpl(thisSession);
		this.notificationService = new NotificationServiceImpl(thisSession);

		this.control = new ControlImpl(jobManagerService, matchingService, 
				userManagerService, skillService, timePeriodService, notificationService);

		List<String> skills1 = new ArrayList<>();
		skills1.add("electrician");
		List<String> skills2 = new ArrayList<>();
		skills2.add("plumbing");
		List<String> skills3 = new ArrayList<>();
		skills3.add("cooking");
		List<String> skills4 = new ArrayList<>();
		skills4.add("general volunteering");
		List<String> skills5 = new ArrayList<>();
		skills5.add("electrician");
		skills5.add("plumbing");

		this.skillsList.add(skills1);
		this.skillsList.add(skills2);
		this.skillsList.add(skills3);
		this.skillsList.add(skills4);
		this.skillsList.add(skills5);

		this.skillList.add("electrician");
		this.skillList.add("plumbing");
		this.skillList.add("cooking");
		this.skillList.add("general volunteering");


		try {
			this.br = new BufferedReader(new FileReader(Populator.class.getClassLoader()
						.getResource("book.txt").getPath()));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		System.out.println("1/12 Adding Volunteers");
		addVolunteers();

		System.out.println("6/12 Adding Jobs");
		addOrganisations();
	}

	protected Job constructJob(int number, GPSLocation gpsLocation, Time time) {
		Job job = new Job();
		job.summary = "Job" + number;
		try {
			while (true) {
				job.description = br.readLine();
				if (!job.description.equals("")) {
					break;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		job.location = new AddressLocation(
				gpsLocation,
				"William Gates Building",
				"15 JJ Thompson Avenue",
				"Cambrige",
				"Cambridgeshire",
				"CB3 0FD",
				"United Kingdom"
		);
		job.expiration = OffsetDateTime.now().plusDays(1);
		job.requiredVolunteers = 1L;
		job.time = time;
		return job;
	}

	protected void addOrganisations() throws DuplicateEntryException, EntryNotFoundException{
		addOrganisation(new GPSLocation(0.002, 0.002), 1);
		addOrganisation(new GPSLocation(0.002, 0.002), 2);
		addOrganisation(new GPSLocation(0.234, 0.564), 3);

		Organisation org1 = this.control.getUserForEmail("orguser1@bar.com").adminOf;

		Job job4 = constructJob(4, new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.MORNING));
		this.control.addJob(job4, org1, this.skillsList.get(0), new ArrayList<>());

		Job job5 = constructJob(5, new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.MORNING));
		this.control.addJob(job5, org1, this.skillsList.get(2), new ArrayList<>());

		Job job6 = constructJob(6, new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON));
		this.control.addJob(job6, org1, this.skillsList.get(2), new ArrayList<>());

		Job job1 = constructJob(1, new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON));
		this.control.addJob(job1, org1, this.skillsList.get(0), new ArrayList<>());



		Organisation org2 = this.control.getUserForEmail("orguser2@bar.com").adminOf;

		Job job2 = constructJob(2, new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON));
		this.control.addJob(job2, org2, this.skillsList.get(0), new ArrayList<>());



		Organisation org3 = this.control.getUserForEmail("orguser3@bar.com").adminOf;

		Job job3 = constructJob(3, new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON));
		this.control.addJob(job3, org3, this.skillsList.get(0), new ArrayList<>());

		System.out.println("7/12 Adding Queens College");
		addQueensCollege();
		System.out.println("8/12 Adding Warwick");
		addWarwickSchool();
		System.out.println("9/12 Adding Banbury");
		addBanbury();
		System.out.println("10/12 Adding New York");
		addNewYork();
		System.out.println("11/12 Adding Beijing");
		addBeijing();
		System.out.println("12/12 Adding Rio de Janeiro");
		addRio();
	}

	protected void addQueensCollege() throws DuplicateEntryException, EntryNotFoundException {
		AddressLocation addressLocation = new AddressLocation(new GPSLocation(52.2013522, 0.1147432), "Queens' College",
				"Silver Street", "Cambridge", "Cambridgeshire", "CB3 9ET", "UK");
		Organisation organisation = new Organisation("Queens College Charity", addressLocation, "qucol@bar.com",
				"www.queens.com", "03456936455");
		this.control.addOrganisation(organisation);

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		User orgUser = new User(passwordDetails.get(0), passwordDetails.get(1), Instant.now(), "Queens user", "queens@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);

		Organisation org4 = this.control.getUserForEmail("queens@bar.com").adminOf;

		//Queens College, Silver Street, CB3 9ET, UK jobs

		Job job7 = constructJob(7, new GPSLocation(52.2013522, 0.1147432),
				this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING));
		job7.summary = "Bulb wiring";
		job7.description = "I'm going to need you to wire up some bulbs for me";
		this.control.addJob(job7, org4, this.skillsList.get(0), new ArrayList<>());

		Job job8 = constructJob(8, new GPSLocation(52.2013522, 0.1147432),
				this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING));
		job8.description = "We need someone to fix the pipes in the kitchen";
		job8.summary = "Pipe fixing";
		this.control.addJob(job8, org4, this.skillsList.get(1), new ArrayList<>());

		Job job9 = constructJob(9, new GPSLocation(52.2013522, 0.1147432),
				this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING));
		job9.description = "There's a distinct need for someone to cook my dinner";
		job9.summary = "Dinner cooking";
		this.control.addJob(job9, org4, this.skillsList.get(2), new ArrayList<>());

		Job job10 = constructJob(10, new GPSLocation(52.2013522, 0.1147432),
				this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING));
		job10.description = "I'm having difficulty standing up and need someone to act as a walking aid";
		job10.summary = "Be my walking aid";
		this.control.addJob(job10, org4, this.skillsList.get(3), new ArrayList<>());

		Job job11 = constructJob(11, new GPSLocation(52.2013522, 0.1147432),
				this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING));
		job11.description = "Water has leaked into the wiring above the porters' lodge, no idea what to do";
		job11.summary = "Porters' lodge leak";
		this.control.addJob(job11, org4, this.skillsList.get(4), new ArrayList<>());

		//Boxworth jobs (about 14 km from queens)

		Job job12 = constructJob(12, new GPSLocation(52.2613812, -0.0252963),
				this.timePeriodService.getTimeEntity(DayOfWeek.THURSDAY, TimePeriod.MORNING));
		job12.description = "Barry's scooter died, we need someone to get it working again";
		job12.summary = "Fix Barry's scooter";
		this.control.addJob(job12, org4, this.skillsList.get(0), this.skillsList.get(2));

		Job job13 = constructJob(13, new GPSLocation(52.2613812, -0.0252963),
				this.timePeriodService.getTimeEntity(DayOfWeek.THURSDAY, TimePeriod.MORNING));
		job13.description = "Our dishwasher is making loud noises that are driving us nuts";
		job13.summary = "Dishwasher racket";
		this.control.addJob(job13, org4, this.skillsList.get(1), this.skillsList.get(3));

		Job job14 = constructJob(14, new GPSLocation(52.2613812, -0.0252963),
				this.timePeriodService.getTimeEntity(DayOfWeek.THURSDAY, TimePeriod.MORNING));
		job14.description = "Our chef recently had a stroke, we need a temporary replacement";
		job14.summary = "Temporary chef";
		this.control.addJob(job14, org4, this.skillsList.get(2), this.skillsList.get(4));
	}

	//job splurge
	protected void addWarwickSchool() throws DuplicateEntryException, EntryNotFoundException {
		AddressLocation addressLocation = new AddressLocation(new GPSLocation(52.2775865, -1.5722693), "Warwick School",
				"Myton Road", "Warwick", "Warwickshire", "CV34 6PP", "UK");
		Organisation organisation = new Organisation("Warwick School", addressLocation, "warwickschool@bar.com",
				"www.warwickschool.com", "03456936455");
		this.control.addOrganisation(organisation);

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		User orgUser = new User(passwordDetails.get(0), passwordDetails.get(1), Instant.now(), "Warwick user", "warwick@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);


		Organisation org5 = this.control.getUserForEmail("warwick@bar.com").adminOf;
		for (int i = 0; i < NUM_JOBS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			Job j = constructJob(i + 100, new GPSLocation(52.2775865 + offset1, -1.5722693 + offset2), getRandomTimeEntity());
			this.control.addJob(j, org5, getRandomSkills(), getRandomSkills());
		}
	}

	//job splurge
	protected void addBanbury() throws DuplicateEntryException, EntryNotFoundException {
		AddressLocation addressLocation = new AddressLocation(new GPSLocation(52.0601807,-1.3402794), "Banbury Cross",
				"Banbury Road", "Banbury", "Oxfordshire", "OX67 8GB", "UK");
		Organisation organisation = new Organisation("Sheesh", addressLocation, "sheesh@bar.com",
				"www.sheeshorg.com", "03456936455");
		this.control.addOrganisation(organisation);

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		User orgUser = new User(passwordDetails.get(0), passwordDetails.get(1), Instant.now(), "Banbury user", "ban@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);


		Organisation org5 = this.control.getUserForEmail("ban@bar.com").adminOf;
		for (int i = 0; i < NUM_JOBS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			Job j = constructJob(i + 200, new GPSLocation(52.0601807 + offset1,-1.3402794 + offset2), getRandomTimeEntity());
			this.control.addJob(j, org5, getRandomSkills(), getRandomSkills());
		}
	}


	//job splurge
	protected void addNewYork() throws DuplicateEntryException, EntryNotFoundException {
		AddressLocation addressLocation = new AddressLocation(new GPSLocation(40.7305991,-73.9865811), "A building",
				"York Street", "New York", "New York", "NY67 8GB", "US");
		Organisation organisation = new Organisation("Bankomatic", addressLocation, "bankomatic@bar.com",
				"www.bankomatic.com", "03456936455");
		this.control.addOrganisation(organisation);

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		User orgUser = new User(passwordDetails.get(0), passwordDetails.get(1), Instant.now(), "NY user", "bank@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);


		Organisation org5 = this.control.getUserForEmail("bank@bar.com").adminOf;
		for (int i = 0; i < NUM_JOBS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			Job j = constructJob(i + 300, new GPSLocation(40.7305991 + offset1,-73.9865811 + offset2), getRandomTimeEntity());
			this.control.addJob(j, org5, getRandomSkills(), getRandomSkills());
		}
	}


	//job splurge
	protected void addBeijing() throws DuplicateEntryException, EntryNotFoundException {
		AddressLocation addressLocation = new AddressLocation(new GPSLocation(39.9059631,116.391248), "Forbidden Palace",
				"Beijing Street", "Beijing", "Beijing County", "BJ67 8GB", "China");
		Organisation organisation = new Organisation("Government", addressLocation, "government@bar.com",
				"www.gov.com", "03456936455");
		this.control.addOrganisation(organisation);

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		User orgUser = new User(passwordDetails.get(0), passwordDetails.get(1), Instant.now(), "gov user", "gov@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);


		Organisation org5 = this.control.getUserForEmail("gov@bar.com").adminOf;
		for (int i = 0; i < NUM_JOBS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			Job j = constructJob(i + 400, new GPSLocation(39.9059631 + offset1,116.391248 + offset2), getRandomTimeEntity());
			this.control.addJob(j, org5, getRandomSkills(), getRandomSkills());
		}
	}

	//job splurge
	protected void addRio() throws DuplicateEntryException, EntryNotFoundException {
		AddressLocation addressLocation = new AddressLocation(new GPSLocation(-22.2752761,-42.4194149), "Rio house",
				"Rio Street", "Rio de Janeiro", "Rio County", "RJ67 8GB", "Brazil");
		Organisation organisation = new Organisation("Rio Gov", addressLocation, "riodejaneiro@bar.com",
				"www.rio.com", "03456936455");
		this.control.addOrganisation(organisation);

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		User orgUser = new User(passwordDetails.get(0), passwordDetails.get(1), Instant.now(), "rio user", "rio@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);


		Organisation org5 = this.control.getUserForEmail("rio@bar.com").adminOf;
		for (int i = 0; i < NUM_JOBS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			Job j = constructJob(i + 500, new GPSLocation(-22.2752761 + offset1,-42.4194149 + offset2), getRandomTimeEntity());
			this.control.addJob(j, org5, getRandomSkills(), getRandomSkills());
		}
	}

	protected Time getRandomTimeEntity() {
		int i = Math.abs(this.random.nextInt()) % 7;
		int j = Math.abs(this.random.nextInt()) % 3;
		return this.timePeriodService.getTimeEntity(days[i], timePeriods[j]);
	}

	protected void addOrganisation(GPSLocation gpsLocation, int number) throws DuplicateEntryException{
		Instant date = Instant.now();
		AddressLocation addressLocation = new AddressLocation(gpsLocation, "line 1", "line2", "town",
				"county", "OX56 7TF", "UK");
		Organisation organisation = new Organisation("organisation" + number, addressLocation, "org" + number + "@bar.com",
				"www.org" + number + ".com", "03456936455");
		this.control.addOrganisation(organisation);
		User orgUser = new User("passwordHash", "passwordSalt", date, "Organisation user", "orguser" + number + "@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);
	}

	protected void addWarwickVolunteers() throws DuplicateEntryException {
		for (int i = 0; i < NUM_VOLUNTEERS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			addRandomVolunteer(new GPSLocation(52.2775865 + offset1, -1.5722693 + offset2), getRandomSkills(), i + 100);
		}
	}

	protected void addNewYorkVolunteers() throws DuplicateEntryException {
		for (int i = 0; i < NUM_VOLUNTEERS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			addRandomVolunteer(new GPSLocation(40.7305991 + offset1, -73.9865811 + offset2), getRandomSkills(), i + 200);
		}
	}

	protected void addRioVolunteers() throws DuplicateEntryException {
		for (int i = 0; i < NUM_VOLUNTEERS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			addRandomVolunteer(new GPSLocation(-22.2752761 + offset1,-42.4194149 + offset2), getRandomSkills(), i + 300);
		}
	}

	protected void addBeijingVolunteers() throws DuplicateEntryException {
		for (int i = 0; i < NUM_VOLUNTEERS; i++) {
			double offset1 = (double) (random.nextInt(20) - 10) / 10000;
			double offset2 = (double) (random.nextInt(20) - 10) / 10000;
			addRandomVolunteer(new GPSLocation(39.9059631 + offset1,116.391248 + offset2), getRandomSkills(), i + 400);
		}
	}

	protected List<String> getRandomSkills() {
		Set<String> skillSet = new HashSet<>();

		int numSkills = (Math.abs(this.random.nextInt()) % 4) + 1;


		for (int i = 0; i < numSkills; i++) {
			int index = Math.abs(this.random.nextInt()) % 4;
			skillSet.add(skillList.get(index));
		}

		List<String> result = new ArrayList<>();

		for (String s : skillSet) {
			result.add(s);
		}

		return result;
	}

	protected void addVolunteers() throws DuplicateEntryException{
		addVolunteer(new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON),
				this.skillsList.get(0),
				1);

		addVolunteer(new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON),
				this.skillsList.get(1),
				2);

		addVolunteer(new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.MORNING),
				this.skillsList.get(2),
				3);

		addVolunteer(new GPSLocation(0.002, 0.002),
				this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.MORNING),
				this.skillsList.get(2),
				4);

		addVolunteer(new GPSLocation(0.234, 0.564),
				this.timePeriodService.getTimeEntity(DayOfWeek.TUESDAY, TimePeriod.MORNING),
				this.skillsList.get(3),
				5);

		addVolunteer(new GPSLocation(0.134, 0.564),
				this.timePeriodService.getTimeEntity(DayOfWeek.TUESDAY, TimePeriod.AFTERNOON),
				this.skillsList.get(1),
				6);


		//volunteer at queens college
		Instant date = Instant.now();
		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		Volunteer volunteer = new Volunteer("me@bar.com", "Me", date, passwordDetails.get(0),
				passwordDetails.get(1), new GPSLocation(52.2013522, 0.1147432), 1L, "01234567890");
		volunteer.times.add(this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING));
		volunteer.times.add(this.timePeriodService.getTimeEntity(DayOfWeek.THURSDAY, TimePeriod.MORNING));
		this.control.addVolunteerWithoutTimes(volunteer, this.skillsList.get(4));

		//volunteer at queens college who should only have access to boxworth jobs, despite having the skills and the time
		passwordDetails = HashPassword.generatePasswordHash("password");
		volunteer = new Volunteer("me2@bar.com", "Me 2", date, passwordDetails.get(0),
				passwordDetails.get(1), new GPSLocation(52.2013522, 0.1147432), 30L, "01234567890");
		volunteer.times.add(this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING));
		volunteer.times.add(this.timePeriodService.getTimeEntity(DayOfWeek.THURSDAY, TimePeriod.MORNING));
		this.control.addVolunteerWithoutTimes(volunteer, this.skillsList.get(4));


		System.out.println("2/12 Adding Warwick Volunteers");
		addWarwickVolunteers();
		System.out.println("3/12 Adding New York Volunteers");
		addNewYorkVolunteers();
		System.out.println("4/12 Adding Rio Volunteers");
		addRioVolunteers();
		System.out.println("5/12 Adding Beijing Volunteers");
		addBeijingVolunteers();
	}

	protected void addRandomVolunteer(GPSLocation gpsLocation, List<String> skills, int number) throws DuplicateEntryException {
		Instant date = Instant.now();
		Volunteer volunteer = new Volunteer("vol" + number + "@bar.com", "Volunteer " + number, date, "password hash 123",
				"salt", gpsLocation, 100L, "01234567890");

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		volunteer.user.passwordHash = passwordDetails.get(0);
		volunteer.user.passwordSalt = passwordDetails.get(1);

		volunteer.times.add(getRandomTimeEntity());
		volunteer.times.add(getRandomTimeEntity());
		volunteer.times.add(getRandomTimeEntity());
		volunteer.times.add(getRandomTimeEntity());
		volunteer.times.add(getRandomTimeEntity());
		volunteer.times.add(getRandomTimeEntity());
		volunteer.times.add(getRandomTimeEntity());
		this.control.addVolunteerWithoutTimes(volunteer, skills);
	}

	protected void addVolunteer(GPSLocation gpsLocation, Time time, List<String> skills, int number) throws DuplicateEntryException {
		Instant date = Instant.now();
		Volunteer volunteer = new Volunteer("vol" + number + "@bar.com", "Volunteer " + number, date, "password hash 123",
				"salt", gpsLocation, 100L, "01234567890");

		List<String> passwordDetails = HashPassword.generatePasswordHash("password");
		volunteer.user.passwordHash = passwordDetails.get(0);
		volunteer.user.passwordSalt = passwordDetails.get(1);

		volunteer.times.add(time);
		this.control.addVolunteerWithoutTimes(volunteer, skills);
	}
}
