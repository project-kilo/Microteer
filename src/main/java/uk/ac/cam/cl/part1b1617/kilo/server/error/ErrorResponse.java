package uk.ac.cam.cl.part1b1617.kilo.server.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"cause", "localizedMessage", "stackTrace", "suppressed"})
public class ErrorResponse extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ErrorResponse(String message) {
		super(message);
	}

	public String getErrorType() {
		return "unknown";
	}

	public int responseCode() {
		return 400;
	}
}
