package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.time.DayOfWeek;

import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Time;

public interface TimePeriodService {
    Time getTimeEntity(DayOfWeek day, TimePeriod period);
}
