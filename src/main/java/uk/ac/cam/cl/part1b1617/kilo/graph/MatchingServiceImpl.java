package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.StreamSupport;

import org.neo4j.ogm.cypher.BooleanOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;

public class MatchingServiceImpl implements MatchingService {
    private static final double JOB_VOLUNTEER_SEARCH_RADIUS_KM = 30.0;

    protected Session session;

    public MatchingServiceImpl(Session session) {
        this.session = session;
    }

    @Override
    public Iterable<Job> getMatchingJobs(Volunteer volunteer) {
        return this.getMatchingJobs(volunteer, (Filters)null);
    }
    @Override
    public Iterable<Job> getMatchingJobs(Volunteer volunteer, Filter filter) {
        return this.getMatchingJobs(volunteer, new Filters(filter));
    }

    @Override
    public Iterable<Job> getMatchingJobs(Volunteer volunteer, Filters filters) {
        Map<String, Object> parameters = new HashMap<>();
        String whereQuery = "";
        if (filters != null) {
            for (Filter filter : filters) {
                if (filter.getBooleanOperator() == BooleanOperator.NONE) {
                    filter.setBooleanOperator(BooleanOperator.AND);
                }
                whereQuery += filter.toCypher("j", false);
                parameters.putAll(filter.parameters());
            }
        }

        parameters.put("id", volunteer.getId());
        parameters.put("layer", Database.SPATIAL_LAYER);

        Iterable<Job> iter = this.session.query(
            Job.class,
            "MATCH (v:Volunteer) WHERE id(v) = {id} " +
                "CALL spatial.withinDistance({layer}, v, v.maxDistancePreferenceKm) " +
                "YIELD node WITH node as j " +
                    "WHERE j:Job " +
                    "AND (v)-[:AVAILABLE]->(:Time)<-[:WHEN]-(j) " +
                    whereQuery + " " +
                "RETURN j",
            parameters
        );
        return () -> StreamSupport.stream(iter.spliterator(), false)
            .map(job -> this.session.load(Job.class, job.uuid))
            .iterator();
    }

    @Override
    public Iterable<Volunteer> getMatchingVolunteers(Job job) {
        return this.getMatchingVolunteers(job, (Filters)null);
    }
    @Override
    public Iterable<Volunteer> getMatchingVolunteers(Job job, Filter filter) {
        return this.getMatchingVolunteers(job, new Filters(filter));
    }

    @Override
    public Iterable<Volunteer> getMatchingVolunteers(Job job, Filters filters) {
        Map<String, Object> parameters = new HashMap<>();
        String whereQuery = "";
        if (filters != null) {
            for (Filter filter : filters) {
                if (filter.getBooleanOperator() == BooleanOperator.NONE) {
                    filter.setBooleanOperator(BooleanOperator.AND);
                }
                whereQuery += filter.toCypher("v", false);
                parameters.putAll(filter.parameters());
            }
        }

        parameters.put("id", job.getId());
        parameters.put("layer", Database.SPATIAL_LAYER);
        parameters.put("radius", JOB_VOLUNTEER_SEARCH_RADIUS_KM);

        Iterable<Volunteer> iter = this.session.query(
            Volunteer.class,
            "MATCH (j:Job) WHERE id(j) = {id} " +
                "CALL spatial.withinDistance({layer}, j, {radius}) " +
                "YIELD node WITH node as v " +
                    "WHERE v:Volunteer " +
                    "AND (v)-[:AVAILABLE]->(:Time)<-[:WHEN]-(j) " +
                    whereQuery + " " +
                "RETURN v",
            parameters
        );
        return () -> StreamSupport.stream(iter.spliterator(), false)
            .map(v -> this.session.load(Volunteer.class, v.getId()))
            .iterator();
    }

}
