package uk.ac.cam.cl.part1b1617.kilo.server.api.job;

import ratpack.func.Action;
import ratpack.handling.Chain;

public class JobEndpoint implements Action<Chain> {

	@Override
	public void execute(Chain chain) throws Exception {
		chain
			.path(new SearchHandler())
			.path(":id", new ManageHandler())
		;
	}

}
