package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class VolunteerJobs extends Entity {

    @Relationship(type = JobAllocationEdge.TYPE, direction = Relationship.OUTGOING)
    public Set<JobAllocationEdge> allocated;
    @Relationship(type = "ACCEPTED", direction = Relationship.OUTGOING)
    public Set<Job> accepted;
    @Relationship(type = "COMPLETED", direction = Relationship.OUTGOING)
    public Set<Job> completed;

    @Relationship(type = "JOBS", direction = Relationship.INCOMING)
    public Volunteer volunteer;

    public VolunteerJobs() {
        allocated = new HashSet<>();
        accepted = new HashSet<>();
        completed = new HashSet<>();
    }

}
