package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;

public class UserResult {
	public String fullName;
	public String email;

	public UserResult(User user) {
		this.fullName = user.fullName;
		this.email = user.email;
	}
}
