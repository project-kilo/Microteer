package uk.ac.cam.cl.part1b1617.kilo;

import java.io.IOException;

import uk.ac.cam.cl.part1b1617.kilo.UIValidation.Geocode;

public class AddressLocation {

    public GPSLocation gps;

    public String address1;
    public String address2;
    public String town;
    public String county;
    public String postcode;
    public String country;

    public AddressLocation(
	GPSLocation gps,
	String address1,
	String address2,
	String town,
	String county,
	String postcode,
	String country
    ) {
	this.gps = gps;
	this.address1 = address1;
	this.address2 = address2;
	this.town = town;
	this.county = county;
	this.postcode = postcode;
	this.country = country;
    }

    public AddressLocation(
	String address1,
	String address2,
	String town,
	String county,
	String postcode,
	String country
    ) {
	this.gps = null;
	this.address1 = address1;
	this.address2 = address2;
	this.town = town;
	this.county = county;
	this.postcode = postcode;
	this.country = country;
    }

    public GPSLocation getGPSLocation() throws IOException {
	if (this.gps == null) {
	    this.gps = Geocode.geocode(
		String.format(
		    "%s, %s, %s, %s",
		    this.address1,
		    this.town,
		    this.postcode,
		    this.country
		)
	    );
	}
	return this.gps;
    }
}
