package uk.ac.cam.cl.part1b1617.kilo;

public enum TimePeriod {
	MORNING,
	AFTERNOON,
	EVENING;
}
