package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import org.neo4j.ogm.typeconversion.UuidStringConverter;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.graph.AddressLocationConverter;
import uk.ac.cam.cl.part1b1617.kilo.graph.OffsetDateTimeConverter;

import java.util.UUID;

@NodeEntity
public class Job extends Entity {
    public String summary;
    public String description;
    @Convert(AddressLocationConverter.class)
    public AddressLocation location;
    @Convert(OffsetDateTimeConverter.class)
    public OffsetDateTime expiration;
    public Long requiredVolunteers;
    @Index(primary = true, unique = true)
    @Convert(UuidStringConverter.class)
    public UUID uuid;

    @Relationship(type = "WHEN", direction = Relationship.OUTGOING)
    public Time time;

    @Relationship(type = "OWNER", direction = Relationship.OUTGOING)
    public Organisation owner;

    @Relationship(type = "REQUIRED", direction = Relationship.OUTGOING)
    public Set<Skill> requiredSkills = new HashSet<>();
    @Relationship(type = "WANTED", direction = Relationship.OUTGOING)
    public Set<Skill> wantedSkills = new HashSet<>();

    @Relationship(type = "ACCEPTED", direction = Relationship.INCOMING)
    public Set<VolunteerJobs> acceptedVolunteers = new HashSet<>();
    @Relationship(type = JobAllocationEdge.TYPE, direction = Relationship.INCOMING)
    public Set<JobAllocationEdge> allocatedVolunteers = new HashSet<>();
    @Relationship(type = "COMPLETED", direction = Relationship.INCOMING)
    public Set<VolunteerJobs> completedVolunteers = new HashSet<>();

    public Job(
        String summary,
        String description,
        AddressLocation location,
        OffsetDateTime expiration,
        Long requiredVolunteers,
        Organisation owner,
        Time time
    ) {
        this.uuid = UUID.randomUUID();
        this.summary = summary;
        this.description = description;
        this.location = location;
        this.expiration = expiration;
        this.requiredVolunteers = requiredVolunteers;
        this.owner = owner;
        this.time = time;
    }

    public Job() {
        super();
        uuid = UUID.randomUUID();
    }

}
