package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobRequest;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobResult;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class JobRootHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(JobRootHandler.class);

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");

		Control control = ControlFactory.makeControl();
		Organisation org;
		try {
			org = control.getOrganisation(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.list(context, org, control))
				.post(() -> this.create(context, org, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void list(Context context, Organisation org, Control control) {
		Set<Job> jobs = control.getOwnedJobs(org);
		List<JobResult> results = jobs.stream()
			.map(j -> new JobResult(j)).collect(Collectors.toList());

		context.render(Jackson.json(results));
		context.getResponse().status(200);
	}

	private void create(Context context, Organisation org, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			User user = LoginUtils.getLoggedInOrgUser(control, context, sessionData);

			if (user.adminOf != org) {
				logger.warn("Forbidden attempt to create job under {} by {}", org.email, user.email);
				throw new ForbiddenResponse("You are not authorized to create jobs for this charity");
			}

			Promise<JobRequest> request = context.parse(
				Jackson.fromJson(JobRequest.class)
			);
			request.then(r -> {
				r.validate();

				OffsetDateTime date = ZonedDateTime.parse(
					r.expiration,
					DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.systemDefault())
				).toOffsetDateTime();
				Job job = new Job(
					r.summary,
					r.description,
					new AddressLocation(
						r.address1,
						r.address2,
						r.city,
						r.county,
						r.postcode,
						r.country
					),
					date,
					Long.parseLong(r.requiredVolunteers),
					org,
					control.getTimeEntity(date)
				);

				control.addJob(job, org, r.requiredSkills, r.wantedSkills);
				context.getResponse().status(201);
				context.header("Location", String.format(
					"/api/v1/charity/%s/job/%s",
					org.email, job.uuid.toString()
				));
				context.render(Jackson.json(new JobResult(job)));
			});
		});

	}
}
