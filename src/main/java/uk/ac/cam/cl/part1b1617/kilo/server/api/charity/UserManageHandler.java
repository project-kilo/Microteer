package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class UserManageHandler implements Handler {
	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");
		String userEmail = context.getPathTokens().get("userEmail");

		Control control = ControlFactory.makeControl();
		Organisation org;
		try {
			org = control.getOrganisation(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}
		User user;
		try {
			user = control.getUserForEmail(userEmail);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(userEmail);
		}

		if (user.adminOf != org) {
			throw new NotFoundResponse(userEmail);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.get(context, user))
				.delete(() -> this.delete(context, user, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void get(Context context, User user) {
		context.getResponse().status(200);
		context.render(Jackson.json(new UserResult(user)));
	}

	private void delete(Context context, User user, Control control) {
		control.removeOrganisationUser(user);
		context.getResponse().status(200);
		context.render(Jackson.json(null));
	}
}
