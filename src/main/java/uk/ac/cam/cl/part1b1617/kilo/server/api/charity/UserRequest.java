package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import static uk.ac.cam.cl.part1b1617.kilo.UIValidation.ValidationWithExceptions.*;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRequest {
	public String fullname;
	public String password;
	public String email;

	public UserRequest(
		@JsonProperty("fullname") String fullname,
		@JsonProperty("password") String password,
		@JsonProperty("email") String email
	) {
		this.fullname = fullname;
		this.password = password;
		this.email = email;
	}

	public void validate() {
		validateFullname(this.fullname);
		validatePassword(this.password);
		validateEmail(this.email);
	}
}
