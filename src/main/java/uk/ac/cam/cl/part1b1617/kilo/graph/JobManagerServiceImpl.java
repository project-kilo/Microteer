package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.neo4j.ogm.session.Session;

import com.google.common.collect.ImmutableMap;


import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;

import java.util.Set;
import java.util.UUID;

public class JobManagerServiceImpl implements JobManagerService {

    protected Session session;

    public JobManagerServiceImpl(Session session) {
        this.session = session;
    }

    //simply saves a job object, this assumes the job's relationships have all been resolved
    @Override
    public void addJob(Job job, Organisation organisation) {
        organisation.jobs.add(job);
        session.save(job);
        session.save(organisation);
        session.query(
            "MATCH (j:Job {uuid: {uuid}}) " +
                "CALL spatial.addNode({layer}, j) " +
                "YIELD node RETURN node",
            ImmutableMap.of(
                "uuid", job.uuid,
                "layer", Database.SPATIAL_LAYER
            )
        );
    }

    @Override
    public Job getJob(UUID id) throws EntryNotFoundException {
        Job job = this.session.load(Job.class, id);
        if (job == null) {
            throw new EntryNotFoundException(id.toString());
        }
        return job;
    }

    //remove a job object from the graph
    @Override
    public void removeJob(Job job){
        session.delete(job);
    }

    //update a job on the graph, assuming the given job has had all it's relationships updated
    @Override
    public void updateJob(Job job) {
        session.save(job);
        session.query(
            "MATCH ()-[r:RTREE_REFERENCE]-(j:Job {uuid: {uuid}}) " +
                "DELETE r",
            ImmutableMap.of(
                "uuid", job.getId()
            )
        );
        session.query(
            "MATCH (j:Job {uuid: {uuid}}) " +
                "CALL spatial.addNode({layer}, j) " +
                "YIELD node RETURN node",
            ImmutableMap.of(
                "uuid", job.uuid,
                "layer", Database.SPATIAL_LAYER
            )
        );
    }

    //switch the allocated relation to an accepted relation
    @Override
    public void acceptJob(Job job, Volunteer volunteer) {
        volunteer.jobs.accepted.add(job);
        job.acceptedVolunteers.add(volunteer.jobs);

        session.save(job);
        session.save(volunteer.jobs);

        JobAllocationEdge jobAllocationEdge = volunteer.getJobAllocationEdgeFor(job);

        session.delete(jobAllocationEdge);
    }

    //remove the allocated relation
    @Override
    public void rejectJob(Job job, Volunteer volunteer) {
        session.save(job);
        JobAllocationEdge jobAllocationEdge = volunteer.getJobAllocationEdgeFor(job);

        session.delete(jobAllocationEdge);
    }

    //switch the accepted relation to an allocated relation
    @Override
    public void unacceptJob(Job job, Volunteer volunteer, double fitness) {
        JobAllocationEdge jobAllocationEdge = new JobAllocationEdge(job, volunteer.jobs, fitness);
        job.allocatedVolunteers.add(jobAllocationEdge);
        volunteer.jobs.allocated.add(jobAllocationEdge);

        job.acceptedVolunteers.remove(volunteer.jobs);
        volunteer.jobs.accepted.remove(job);

        session.save(job);
        session.save(volunteer.jobs);
        session.save(jobAllocationEdge);
    }

    //this adds a JobAllocationEdge object to the graph, and updates job and volunteer.jobs to link to it
    @Override
    public void suggestJob(Job job, Volunteer volunteer, double fitness) {
        JobAllocationEdge jobAllocationEdge = new JobAllocationEdge(job, volunteer.jobs, fitness);
        volunteer.jobs.allocated.add(jobAllocationEdge);
        job.allocatedVolunteers.add(jobAllocationEdge);

        session.save(job);
        session.save(volunteer);
        session.save(jobAllocationEdge);
    }

    @Override
    public void jobCompleted(Volunteer volunteer, Job job, int rating) {
        volunteer.rating = (volunteer.rating * volunteer.numberOfRatings + rating) / (volunteer.numberOfRatings + 1);
        volunteer.numberOfRatings += 1;

        session.save(volunteer);
        volunteer = session.load(Volunteer.class, volunteer.getId(), 1);
        volunteer.jobs = session.load(VolunteerJobs.class, volunteer.jobs.getId(), 1);
        job = session.load(Job.class, job.uuid);

        volunteer.jobs.completed.add(job);
        job.completedVolunteers.add(volunteer.jobs);

        volunteer.jobs.accepted.remove(job);
        job.acceptedVolunteers.remove(volunteer.jobs);

        session.save(volunteer);
        //session.save(job);
    }

    @Override
    public Job loadJob(Job job) {
        return session.load(Job.class, job.uuid);
    }

    @Override
    public void rateJob(Job job, int rating) {
        job.owner.rating = (job.owner.rating * job.owner.numberOfRatings + rating) / (job.owner.numberOfRatings + 1);
        job.owner.numberOfRatings += 1;
        session.save(job);
    }

    @Override
    public void addRequiredSkillToJob(Job job, Skill skill) {
        job.requiredSkills.add(skill);
        session.save(job);
    }

    @Override
    public void addWantedSkillToJob(Job job, Skill skill) {
        job.wantedSkills.add(skill);
        session.save(job);
    }

    @Override
    public Set<Job> getJobsForOrganisation(Organisation organisation) {
        organisation = session.load(Organisation.class, organisation.email, 2);
        return organisation.jobs;
    }
}
