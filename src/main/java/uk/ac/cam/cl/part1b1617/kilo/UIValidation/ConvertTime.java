package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.Set;

import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;
import uk.ac.cam.cl.part1b1617.kilo.graph.TimePeriodService;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Time;
import uk.ac.cam.cl.part1b1617.kilo.server.api.TimeMatrix;
import uk.ac.cam.cl.part1b1617.kilo.server.api.TimeMatrix.TimeMatrixRow;

public class ConvertTime {

	public static Time convertTime(TimePeriodService timePeriodService, String day, String time) {
		DayOfWeek dayObj;
		TimePeriod timePerObj;
		switch(day) {
		case "monday":
			dayObj = DayOfWeek.MONDAY;
		case "tuesday":
			dayObj = DayOfWeek.TUESDAY;
		case "wednesday":
			dayObj = DayOfWeek.WEDNESDAY;
		case "thursday":
			dayObj = DayOfWeek.THURSDAY;
		case "friday":
			dayObj = DayOfWeek.FRIDAY;
		case "saturday":
			dayObj = DayOfWeek.SATURDAY;
		case "sunday":
			dayObj = DayOfWeek.SUNDAY;
		default:
			dayObj = null;
		}
		switch(time) {
		case "morning":
			timePerObj = TimePeriod.MORNING;
		case "afternoon":
			timePerObj = TimePeriod.AFTERNOON;
		case "evening":
			timePerObj = TimePeriod.EVENING;
		default:
			timePerObj = null;
		}
		Time timeObj = timePeriodService.getTimeEntity(dayObj, timePerObj);
		return timeObj;
	}

	public static Set<Time> convertTimes(TimePeriodService timePeriodService, TimeMatrix matrix) {
		Set<Time> availableTimes = new HashSet<Time>();
		availableTimes.addAll(convertRow(timePeriodService, matrix.monday, DayOfWeek.MONDAY));
		availableTimes.addAll(convertRow(timePeriodService, matrix.tuesday, DayOfWeek.TUESDAY));
		availableTimes.addAll(convertRow(timePeriodService, matrix.wednesday, DayOfWeek.WEDNESDAY));
		availableTimes.addAll(convertRow(timePeriodService, matrix.thursday, DayOfWeek.THURSDAY));
		availableTimes.addAll(convertRow(timePeriodService, matrix.friday, DayOfWeek.FRIDAY));
		availableTimes.addAll(convertRow(timePeriodService, matrix.saturday, DayOfWeek.SATURDAY));
		availableTimes.addAll(convertRow(timePeriodService, matrix.sunday, DayOfWeek.SUNDAY));
		return availableTimes;
	}

	public static Set<Time> convertRow(TimePeriodService timePeriodService, TimeMatrixRow row, DayOfWeek day) {
		Set<Time> available = new HashSet<Time>();
		if (row.morning) {
			available.add(timePeriodService.getTimeEntity(day, TimePeriod.MORNING));
		}
		if (row.afternoon) {
			available.add(timePeriodService.getTimeEntity(day, TimePeriod.AFTERNOON));
		}
		if (row.evening) {
			available.add(timePeriodService.getTimeEntity(day, TimePeriod.EVENING));
		}
		return available;
	}

}
