package uk.ac.cam.cl.part1b1617.kilo.server.error;

public class NotFoundResponse extends ErrorResponse {
	private static final long serialVersionUID = 1L;

	public String identifier;

	public NotFoundResponse(String identifier) {
		super(String.format("Resource %s not found", identifier));
		this.identifier = identifier;
	}

	public String getErrorType() {
		return "notFound";
	}

	public int responseCode() {
		return 404;
	}
}
