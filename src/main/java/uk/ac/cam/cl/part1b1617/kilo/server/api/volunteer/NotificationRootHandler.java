package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Notification;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class NotificationRootHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(NotificationRootHandler.class);

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");

		Control control = ControlFactory.makeControl();
		User user;
		try {
			user = control.getUserForEmail(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.list(context, user, control))
				.post(() -> this.create(context, user, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void list(Context context, User user, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			User loggedIn = LoginUtils.getLoggedInUser(control, context, sessionData);

			if (!user.email.equals(loggedIn.email)) {
				logger.warn("Forbidden attempt to view notifications for {} by {}", user.email, user.email);
				throw new ForbiddenResponse("You are not authorized to view this resource");
			}

			Iterable<Notification> notifications = control.getReceivedNotifications(user);
			Stream<Notification> stream = StreamSupport.stream(notifications.spliterator(), false);
			List<NotificationResult> results = stream.map(n -> new NotificationResult(n)).collect(Collectors.toList());

			context.render(Jackson.json(results));
			context.getResponse().status(200);
		});
	}

	private void create(Context context, User user, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			User loggedIn = LoginUtils.getLoggedInUser(control, context, sessionData);

			Promise<NotificationRequest> request = context.parse(
				Jackson.fromJson(NotificationRequest.class)
			);
			request.then(r -> {
				Notification notification = new Notification();
				notification.subject = r.subject;
				notification.body = r.body;
				notification.submitted = Instant.now();

				notification.from = loggedIn;
				notification.to = user;

				control.addNotification(notification);
			});
		});
	}

}
