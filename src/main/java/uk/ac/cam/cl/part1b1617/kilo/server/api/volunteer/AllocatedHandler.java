package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.control.MatchQuality;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobResult;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class AllocatedHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(AllocatedHandler.class);

	protected static class RejectRequest {
		public String reason;

		public RejectRequest(
			@JsonProperty("reason") String reason
		) {
			this.reason = reason;
		}
	}

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");
		String id = context.getPathTokens().get("id");

		Control control = ControlFactory.makeControl();
		User user;
		try {
			user = control.getUserForEmail(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}
		Volunteer volunteer = user.volunteer;
		if (volunteer == null) {
			throw new NotFoundResponse(email);
		}

		Job job;
		try {
			job = control.getJob(UUID.fromString(id));
			
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(id);
		}
		if (!job.allocatedVolunteers.contains(volunteer.getJobAllocationEdgeFor(job))) {		
			throw new NotFoundResponse(id);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.info(context, volunteer, job, control))
				.delete(() -> this.decline(context, volunteer, job, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void info(Context context, Volunteer volunteer, Job job, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			Volunteer loggedIn = LoginUtils.getLoggedInVolunteer(control, context, sessionData);

			if (!volunteer.user.email.equals(loggedIn.user.email)) {
				logger.warn("Forbidden attempt to view jobs for {} by {}", volunteer.user.email, loggedIn.user.email);
				throw new ForbiddenResponse("You are not authorized to view this resource");
			}

			context.render(Jackson.json(new JobResult(job)));
			context.getResponse().status(200);
		});
	}

	private void decline(Context context, Volunteer volunteer, Job job, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			
			Volunteer loggedIn = LoginUtils.getLoggedInVolunteer(control, context, sessionData);

			if (!volunteer.user.email.equals(loggedIn.user.email)) {
				logger.warn("Forbidden attempt to view jobs for {} by {}", volunteer.user.email, loggedIn.user.email);
				throw new ForbiddenResponse("You are not authorized to view this resource");
			}
			

			Promise<RejectRequest> request = context.parse(
				Jackson.fromJson(RejectRequest.class)
			);
			request.then(r -> {
				MatchQuality quality;
				switch (r.reason) {
					case "badMatch":
						quality = MatchQuality.BAD_MATCH;
						break;
					case "goodMatch":
						quality = MatchQuality.GOOD_MATCH;
						break;
					default:
						throw new ErrorResponse("Expected reason = goodMatch|badMatch");
				}
				control.rejectJob(job, volunteer, quality);
				context.render(Jackson.json(null));
				context.getResponse().status(200);
			});
		});
	}
}
