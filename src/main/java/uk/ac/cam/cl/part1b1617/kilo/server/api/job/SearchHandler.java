package uk.ac.cam.cl.part1b1617.kilo.server.api.job;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;

public class SearchHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(SearchHandler.class);

	@Override
	public void handle(Context context) {
		try {
			context.byMethod(s -> s
				.get(() -> this.search(context))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void search(Context context) {
		String search = context.getRequest().getQueryParams().get("search");

		context.get(Session.class).getData().then(sessionData -> {
			Control control = ControlFactory.makeControl();
			Volunteer volunteer = LoginUtils.getLoggedInVolunteer(control, context, sessionData);

			if (Strings.isNullOrEmpty(search)) {
				logger.warn("Search was empty");
				throw new ErrorResponse("Search was empty");
			}

			logger.debug("Searching jobs from {} with search '{}'", volunteer.user.email, search);
			Iterable<Job> jobs = control.searchJobs(volunteer, search);
			Stream<Job> stream = StreamSupport.stream(jobs.spliterator(), false);
			List<JobResult> results = stream.map(j -> new JobResult(j)).collect(Collectors.toList());
			logger.debug("Found {} jobs", results.size());

			context.render(Jackson.json(results));
			context.getResponse().status(200);
		});
	}
}
