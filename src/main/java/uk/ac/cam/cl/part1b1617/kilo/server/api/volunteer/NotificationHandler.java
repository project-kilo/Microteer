package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Notification;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobResult;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class NotificationHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(NotificationHandler.class);

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");
		String id = context.getPathTokens().get("id");

		Control control = ControlFactory.makeControl();
		User user;
		try {
			user = control.getUserForEmail(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}

		context.get(Session.class).getData().then(sessionData -> {
			User loggedIn = LoginUtils.getLoggedInUser(control, context, sessionData);

			if (!user.equals(loggedIn)) {
				logger.warn("Forbidden attempt to access notifications for {} by {}", user.email, user.email);
				throw new ForbiddenResponse("You are not authorized to access this resource");
			}

			Notification notification;
			try {
				notification = control.getNotification(UUID.fromString(id));
			} catch (EntryNotFoundException e) {
				throw new NotFoundResponse(id);
			}
			if (!notification.to.equals(user)) {
				throw new NotFoundResponse(id);
			}

			try {
				context.byMethod(s -> s
					.get(() -> this.info(context, user, notification, control))
					.delete(() -> this.delete(context, user, notification, control))
				);
			} catch (Exception e) {
				context.error(e);
			}
		});
	}

	private void info(Context context, User user, Notification notification, Control control) {
		context.render(Jackson.json(new NotificationResult(notification)));
		context.getResponse().status(200);
	}

	private void delete(Context context, User user, Notification notification, Control control) {
		control.removeNotification(notification);
		context.render(Jackson.json(null));
		context.getResponse().status(200);
	}
}
