package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.ogm.typeconversion.CompositeAttributeConverter;

import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;

public class GPSLocationConverter implements CompositeAttributeConverter<GPSLocation> {

	@Override
	public Map<String, ?> toGraphProperties(GPSLocation location) {
		Map<String, Double> properties = new HashMap<>();
		if (location != null) {
			properties.put("latitude", location.latitude);
			properties.put("longitude", location.longitude);
		}
		return properties;
	}

	@Override
	public GPSLocation toEntityAttribute(Map<String, ?> map) {
		Double latitude = (Double) map.get("latitude");
		Double longitude = (Double) map.get("longitude");
		if (latitude != null && longitude != null) {
			return new GPSLocation(latitude, longitude);
		}
		return null;
	}
}
