package uk.ac.cam.cl.part1b1617.kilo.recommender;

import java.lang.Math;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Skill;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.VolunteerSkillEdge;

public class Recommender {
    
    public static final double MAXFITNESS = 1.0E+6;
    public static final double EPS = 1.0E-6;
    public static final double FRACTION = 10.0;

    //Returns a real number between 0 and MAXFITNESS
    //that indicates how close Volunteer v and Job j are
    //Job and Volunteer need to have profiles in the database
    public static double score(Volunteer v, Job j) {
        //get job and volunteer vectors
        Set<Skill> required = j.requiredSkills;
        Set<Skill> wanted = j.wantedSkills;
        Set<VolunteerSkillEdge> skills = v.skills;

        //calculate job vector length
        double jobVectorLen = Math.sqrt(required.size() + wanted.size());

        //score calculation
        int requiredCnt = 0;
        double matchingScore = 0.0;

        for (VolunteerSkillEdge vSkill : skills) {
            if (vSkill.confidence < EPS) continue;

            if ( required.contains(vSkill.skill) ) {
                requiredCnt += 1;
                matchingScore += vSkill.confidence / (2*jobVectorLen);
            } else if ( wanted.contains(vSkill.skill) ) {
                matchingScore += vSkill.confidence / (2*jobVectorLen);
            }
        }
        
        if (requiredCnt < required.size()) {
            return 0.0;
        } else {
            return matchingScore;
        }
    }

    //Calculation of confidence based on current confidence and action
    //In case of accept increment by one-FRACTION-th of the remaining confidence
    //In case of reject decrement by one-FRACTION-th of the current confidence
    private static Double fit(Double confidence, EAction a) {
        if (a == EAction.ACCEPT) {
            return  Math.min( MAXFITNESS, confidence + (MAXFITNESS - confidence) / FRACTION );
        }
        else {
            return  Math.max( 0.0, confidence - FRACTION - confidence / FRACTION );
        }
    }

    //Update Volunteer profile when a job is accepted/rejected
    //This version assumes that not all skills are stored in Volunteer skillset
    //Therefore new Volunteer Skills are added or deleted
    public static void updateVolunteerOnAction(Volunteer v, Job j, EAction a) {
        //get job and volunteer vectors
        Set<Skill> required = j.requiredSkills;
        Set<Skill> wanted = j.wantedSkills;
        Set<VolunteerSkillEdge> skills = v.skills;

        //Intersection of Volunteer and Job skills
        Set<Skill> present = new HashSet<Skill>();

        //update skills already present
        for (Iterator<VolunteerSkillEdge> it = skills.iterator(); it.hasNext();) {
            VolunteerSkillEdge vSkill = it.next();

            if ( required.contains(vSkill.skill) || wanted.contains(vSkill.skill) ) {
                vSkill.confidence = fit(vSkill.confidence, a);

                if (a == EAction.ACCEPT) {
                    present.add(vSkill.skill);
                } else if (vSkill.confidence < EPS) {
                    it.remove();
                }
            }
        }

        //Add new skills to volunteer vector if job was Accepted
        if (a == EAction.ACCEPT) {
            for (Skill skill : required) {
                if (!present.contains(skill)) {
                    skills.add( new VolunteerSkillEdge(v, skill, fit(0.0, a)) );
                }
            }
            for (Skill skill : wanted) {
                if (!present.contains(skill)) {
                    skills.add( new VolunteerSkillEdge(v, skill, fit(0.0, a)) );
                }
            }
        }

    }

}
