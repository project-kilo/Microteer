package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.neo4j.ogm.session.Session;

public interface Database {
    String DOMAIN_PACKAGE = "uk.ac.cam.cl.part1b1617.kilo.graph.domain";
    String SPATIAL_LAYER = "geom";

    Session openSession();
    void purge();
}
