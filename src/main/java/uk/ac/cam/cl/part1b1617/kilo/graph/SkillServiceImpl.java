package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.neo4j.ogm.session.Session;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Skill;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.VolunteerSkillEdge;

public class SkillServiceImpl implements SkillService{

	protected Session session;

	public SkillServiceImpl(Session session) {
		this.session = session;
	}

	@Override
	public Skill getSkillForSkillName(String skillName) {
		Skill skill = session.load(Skill.class, skillName);

		if (skill == null) {
			skill = new Skill();
			skill.skill = skillName;
			session.save(skill);
		}

		return skill;
	}

	@Override
	public void addVolunteerSkillEdge(VolunteerSkillEdge volunteerSkillEdge) {
		session.save(volunteerSkillEdge);
	}

	@Override
	public void addSkill(String s) {
		Skill skill = new Skill();
		skill.skill = s;
		session.save(skill);
	}
}
