package uk.ac.cam.cl.part1b1617.kilo;

public class GPSLocation {
    public double latitude;
    public double longitude;

    public GPSLocation(double latitude, double longitude) {
	this.latitude = latitude;
	this.longitude = longitude;
    }

    @Override
    public int hashCode() {
	return (int) Math.round(this.latitude+this.longitude);
    }

    @Override
    public boolean equals(Object other) {
	// TODO DO WE HAVE TO WORRY ABOUT DOUBLE PRECISION? SHOULD LAT AND LONG BE ROUNDED AND THEN TESTED?
	if (other instanceof GPSLocation) {
	    GPSLocation test = (GPSLocation) other;
	    if (
		test.hashCode()==this.hashCode() &&
		test.latitude==this.latitude &&
		test.longitude==this.longitude
	    ) {
		return true;
	    }
	}
	return false;
    }
}
