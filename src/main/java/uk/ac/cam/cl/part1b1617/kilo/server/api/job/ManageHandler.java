package uk.ac.cam.cl.part1b1617.kilo.server.api.job;

import java.util.UUID;

import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;

import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class ManageHandler implements Handler {
	@Override
	public void handle(Context context) {
		String id = context.getPathTokens().get("id");

		Control control = ControlFactory.makeControl();
		Job job;
		try {
			job = control.getJob(UUID.fromString(id));
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(id);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.get(context, job))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void get(Context context, Job job) {
		context.getResponse().status(200);
		context.header("Location", String.format(
			"/api/v1/charity/%s/job/%s",
			job.owner.email, job.uuid.toString()
		));
		context.render(Jackson.json(new JobResult(job)));
	}
}
