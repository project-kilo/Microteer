package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.graph.AddressLocationConverter;

@NodeEntity
public class Organisation extends Entity {
    public String name;
    @Convert(AddressLocationConverter.class)
    public AddressLocation location;
    @Index(primary=true, unique=true) public String email;
    public String website;
    public String phone;
    public Double rating = 0.0;
    public int numberOfRatings = 0;
    public String description = "";

    @Relationship(type = "OWNER", direction = Relationship.INCOMING)
    public Set<Job> jobs;

    public Organisation (String name, AddressLocation location, String email, String website, String phone) {
        this.name = name;
        this.location = location;
        this.email = email;
        this.website = website;
        this.phone = phone;
        this.jobs = new HashSet<>();
    }

    public Organisation() {
        super();
        this.jobs = new HashSet<>();
    }
}
