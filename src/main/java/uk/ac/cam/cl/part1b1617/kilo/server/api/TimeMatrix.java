package uk.ac.cam.cl.part1b1617.kilo.server.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TimeMatrix {
	public static class TimeMatrixRow {
		public boolean morning;
		public boolean afternoon;
		public boolean evening;

		public TimeMatrixRow(
			@JsonProperty("morning") boolean morning,
			@JsonProperty("afternoon") boolean afternoon,
			@JsonProperty("evening") boolean evening
		) {
			this.morning = morning;
			this.afternoon = afternoon;
			this.evening = evening;
		}
	}

	public TimeMatrixRow monday;
	public TimeMatrixRow tuesday;
	public TimeMatrixRow wednesday;
	public TimeMatrixRow thursday;
	public TimeMatrixRow friday;
	public TimeMatrixRow saturday;
	public TimeMatrixRow sunday;

	public TimeMatrix(
		@JsonProperty("monday") TimeMatrixRow monday,
		@JsonProperty("tuesday") TimeMatrixRow tuesday,
		@JsonProperty("wednesday") TimeMatrixRow wednesday,
		@JsonProperty("thursday") TimeMatrixRow thursday,
		@JsonProperty("friday") TimeMatrixRow friday,
		@JsonProperty("saturday") TimeMatrixRow saturday,
		@JsonProperty("sunday") TimeMatrixRow sunday
	) {
		if (monday == null) {
			monday = new TimeMatrixRow(false, false, false);
		}
		if (tuesday == null) {
			tuesday = new TimeMatrixRow(false, false, false);
		}
		if (wednesday == null) {
			wednesday = new TimeMatrixRow(false, false, false);
		}
		if (thursday == null) {
			thursday = new TimeMatrixRow(false, false, false);
		}
		if (friday == null) {
			friday = new TimeMatrixRow(false, false, false);
		}
		if (saturday == null) {
			saturday = new TimeMatrixRow(false, false, false);
		}
		if (sunday == null) {
			sunday = new TimeMatrixRow(false, false, false);
		}
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
	}
}
