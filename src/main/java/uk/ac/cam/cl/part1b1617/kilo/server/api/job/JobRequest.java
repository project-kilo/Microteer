package uk.ac.cam.cl.part1b1617.kilo.server.api.job;

import static uk.ac.cam.cl.part1b1617.kilo.UIValidation.ValidationWithExceptions.*;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JobRequest {
	public String summary;
	public String description;

	public String address1;
	public String address2;
	public String city;
	public String county;
	public String postcode;
	public String country;

	public String expiration;
	public String requiredVolunteers;

	public List<String> requiredSkills;
	public List<String> wantedSkills;

	public JobRequest(
		@JsonProperty("summary") String summary,
		@JsonProperty("description") String description,
		@JsonProperty("address1") String address1,
		@JsonProperty("address2") String address2,
		@JsonProperty("city") String city,
		@JsonProperty("county") String county,
		@JsonProperty("postcode") String postcode,
		@JsonProperty("country") String country,
		@JsonProperty("expiration") String expiration,
		@JsonProperty("requiredVolunteers") String requiredVolunteers,
		@JsonProperty("requiredSkills") List<String> requiredSkills,
		@JsonProperty("wantedSkills") List<String> wantedSkills
	) {
		this.summary = summary;
		this.description = description;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.county = county;
		this.postcode = postcode;
		this.country = country;
		this.expiration = expiration;
		this.requiredVolunteers = requiredVolunteers;
		this.requiredSkills = requiredSkills;
		this.wantedSkills = wantedSkills;

		if (this.requiredSkills == null) {
			this.requiredSkills = new ArrayList<>();
		}
		if (this.wantedSkills == null) {
			this.wantedSkills = new ArrayList<>();
		}
	}

	public void validate() {
		validateFullAddress(
			this.address1,
			this.city,
			this.postcode,
			this.country
		);
	}
}
