package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import ratpack.func.Action;
import ratpack.handling.Chain;

public class CharityEndpoint implements Action<Chain> {

	@Override
	public void execute(Chain chain) throws Exception {
		chain
			.path(new RootHandler())
			.path(":email", new ManageHandler())
			.path(":email/job", new JobRootHandler())
			.path(":email/job/:id", new JobManageHandler())
			.path(":email/user", new UserRootHandler())
			.path(":email/user/:user", new UserManageHandler())
		;
	}
}
