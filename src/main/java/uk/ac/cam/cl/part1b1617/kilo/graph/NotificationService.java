package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.util.UUID;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Notification;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;

public interface NotificationService {
    /**
     * simply adds a notification object to the graph
     * @param notification the notification to add
     */
    void addNotification(Notification notification);

    /**
     * removes the specified notification from the graph
     * @param notification the notification to remove
     */
    void removeNotification(Notification notification);

    Iterable<Notification> getReceived(User user);

    Notification get(UUID id) throws EntryNotFoundException;
}
