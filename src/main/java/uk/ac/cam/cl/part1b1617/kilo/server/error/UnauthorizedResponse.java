package uk.ac.cam.cl.part1b1617.kilo.server.error;

public class UnauthorizedResponse extends ErrorResponse {
	private static final long serialVersionUID = 1L;

	public UnauthorizedResponse() {
		super("Unauthorized");
	}

	public String getErrorType() {
		return "unauthorized";
	}

	public int responseCode() {
		return 401;
	}
}
