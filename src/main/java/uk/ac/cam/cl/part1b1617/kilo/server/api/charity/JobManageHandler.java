package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import java.util.HashSet;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.TimeUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobRequest;
import uk.ac.cam.cl.part1b1617.kilo.server.api.job.JobResult;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class JobManageHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(JobManageHandler.class);

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");
		String id = context.getPathTokens().get("id");

		Control control = ControlFactory.makeControl();
		Organisation org;
		try {
			org = control.getOrganisation(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}
		Job job;
		try {
			job = control.getJob(UUID.fromString(id));
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(id);
		} catch (IllegalArgumentException e) {
			throw new ErrorResponse("Invalid UUID format");
		}

		if (job.owner != org) {
			throw new NotFoundResponse(id);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.get(context, job))
				.put(() -> this.update(context, job, control))
				.delete(() -> this.delete(context, job, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void get(Context context, Job job) {
		context.getResponse().status(200);
		context.render(Jackson.json(new JobResult(job)));
	}

	private void delete(Context context, Job job, Control control) {
		control.removeJob(job);
		context.getResponse().status(200);
		context.render(Jackson.json(null));
	}

	private void update(Context context, Job job, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			User user = LoginUtils.getLoggedInOrgUser(control, context, sessionData);

			if (user.adminOf != job.owner) {
				logger.warn("Forbidden attempt to edit job {} by {}", job.uuid.toString(), user.email);
				throw new ForbiddenResponse("You are not authorized to update this job");
			}

			Promise<JobRequest> request = context.parse(
				Jackson.fromJson(JobRequest.class)
			);
			request.then(r -> {
				r.validate();

				if (!job.summary.equals(r.summary)) {
					logger.info("Updated job {} summary to {}", job.uuid.toString(), r.summary);
					job.summary = r.summary;
				}
				if (!job.description.equals(r.description)) {
					logger.info("Updated job {} description to {}", job.uuid.toString(), r.description);
					job.description = r.description;
				}
				job.location = new AddressLocation(
					r.address1,
					r.address2,
					r.city,
					r.county,
					r.postcode,
					r.country
				);
				job.expiration = TimeUtils.parseDateString(r.expiration);
				job.requiredVolunteers = Long.parseLong(r.requiredVolunteers);

				job.requiredSkills = new HashSet<>();
				job.wantedSkills = new HashSet<>();
				control.addSkillsToJob(job, r.requiredSkills, r.wantedSkills);

				control.updateJob(job);
				context.getResponse().status(200);
				context.render(Jackson.json(new JobResult(job)));
			});
		});

	}
}
