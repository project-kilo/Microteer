package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Notification;
import uk.ac.cam.cl.part1b1617.kilo.server.TimeUtils;

public class NotificationResult {
    public String uuid;
    public String from;
    public String job;

    public String time;
    public String subject;
    public String body;

    public NotificationResult(Notification notification) {
        this.uuid = notification.uuid.toString();
        if (notification.from != null) {
            this.from = notification.from.email;
        }
        if (notification.involving != null) {
            this.job = notification.involving.uuid.toString();
        }

        this.time = TimeUtils.formatDate(notification.submitted);
        this.subject = notification.subject;
        this.body = notification.body;
    }

}
