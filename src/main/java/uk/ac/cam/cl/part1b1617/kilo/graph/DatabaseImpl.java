package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.util.Optional;

import org.neo4j.ogm.config.Configuration;
import org.neo4j.ogm.exception.CypherException;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;

import com.google.common.collect.ImmutableMap;

public class DatabaseImpl implements Database {

    private static String getEnv(String env, String def) {
        return Optional.ofNullable(System.getenv(env)).orElse(def);
    }
    private static final Configuration DEFAULT_CONFIGURATION;

    static {
        DEFAULT_CONFIGURATION = new Configuration();
        DEFAULT_CONFIGURATION.driverConfiguration()
            .setDriverClassName("org.neo4j.ogm.drivers.bolt.driver.BoltDriver")
            .setURI(String.format(
                "bolt://%s:%s",
                getEnv("KILO_DB_HOST", "localhost"),
                getEnv("KILO_DB_PORT", "7687")
            ))
            .setCredentials(
                getEnv("KILO_DB_USERNAME", "neo4j"),
                getEnv("KILO_DB_PASSWORD", "neo4j")
            )
            .setConnectionPoolSize(150);
    }

    protected SessionFactory sessionFactory;

    public DatabaseImpl() {
        this(DEFAULT_CONFIGURATION);
    }

    public DatabaseImpl(Configuration config) {
        this.sessionFactory = new SessionFactory(config, Database.DOMAIN_PACKAGE);
        Session initializer = this.openSession();
        this.initialize(initializer);
    }

    @Override
    public Session openSession() {
        return this.sessionFactory.openSession();
    }

    @Override
    public void purge() {
        Session session = this.openSession();
        session.purgeDatabase();
        this.initialize(session);
    }

    private void initialize(Session session) {
        this.initializeSpatial(session);
        TimePeriodServiceImpl.initialize(session);
    }

    private void initializeSpatial(Session session) {
        // create the spatial layer if it does not exist
        try {
            session.query(
                "CALL spatial.layer({name})",
                ImmutableMap.of("name", Database.SPATIAL_LAYER)
            );
        } catch (CypherException e) {
            session.query(
                "CALL spatial.addPointLayer({name})",
                ImmutableMap.of("name", Database.SPATIAL_LAYER)
            );
        }
    }
}
