package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.time.Instant;

import org.neo4j.ogm.typeconversion.AttributeConverter;

public class InstantConverter implements AttributeConverter<Instant, String> {

    @Override
    public String toGraphProperty(Instant value) {
        return value.toString();
    }

    @Override
    public Instant toEntityAttribute(String value) {
        return Instant.parse(value);
    }
}
