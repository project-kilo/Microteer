package uk.ac.cam.cl.part1b1617.kilo.server.api.volunteer;

import java.time.Instant;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.DuplicateEntryException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;

public class SignUpHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(SignUpHandler.class);

	private static class VolunteerExistsResponse extends ErrorResponse {
		private static final long serialVersionUID = 1L;

		public VolunteerExistsResponse(String identifier) {
			super(String.format("Volunteer %s already exists", identifier));
		}

		@Override
		public String getErrorType() {
			return "volunteerExists";
		}
	}

	@Override
	public void handle(Context context) {
		try {
			context.byMethod(s -> s
				.post(() -> this.signup(context))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void signup(Context context) {
		Promise<ProfileRequest> signup = context.parse(
			Jackson.fromJson(ProfileRequest.class)
		);

		signup.then(s -> {
			Control control = ControlFactory.makeControl();

			// Validation
			s.validate();

			Long maxDist;
			try {
				maxDist = Long.parseLong(s.dist);
			} catch (NumberFormatException e) {
				maxDist = 30L;// Default max distance
			}

			Instant date = Instant.now();
			AddressLocation address = new AddressLocation(
				s.address1,
				s.address2,
				s.city,
				s.county,
				s.postcode,
				s.country
			);

			ArrayList<String> passwordWithHash = HashPassword.generatePasswordHash(s.password);
			Volunteer vol = new Volunteer(
				s.email,
				s.fullname,
				date,
				passwordWithHash.get(0),
				passwordWithHash.get(1),
				address.getGPSLocation(),
				maxDist,
				s.phone
			);

			try {
				control.addVolunteer(vol, s.skills, s.times);
			} catch (DuplicateEntryException e) {
				logger.warn("Volunteer {} already exists", s.email);
				throw new VolunteerExistsResponse(s.email);
			}

			context.getResponse().status(201);
			context.header("Location", String.format("/api/v1/volunteer/%s", s.email));
			logger.info("Created volunteer {}", s.email);
			context.render(Jackson.json(null));
		});
	}
}
