package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.neo4j.ogm.typeconversion.CompositeAttributeConverter;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;

public class AddressLocationConverter implements CompositeAttributeConverter<AddressLocation> {

    private static final GPSLocationConverter gpsConverter = new GPSLocationConverter();

    @Override
    public Map<String, ?> toGraphProperties(AddressLocation location) {
        Map<String, Object> result = new HashMap<>();

        if (location != null) {
            Map<String, String> properties = new HashMap<>();
            properties.put("address1", location.address1);
            properties.put("address2", location.address2);
            properties.put("town", location.town);
            properties.put("county", location.county);
            properties.put("postcode", location.postcode);
            properties.put("country", location.country);
            result.putAll(properties);

            try {
                result.putAll(gpsConverter.toGraphProperties(location.getGPSLocation()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public AddressLocation toEntityAttribute(Map<String, ?> map) {
        GPSLocation gpsLocation = gpsConverter.toEntityAttribute(map);

        String address1 = (String) map.get("address1");
        String address2 = (String) map.get("address2");
        String town = (String) map.get("town");
        String county = (String) map.get("county");
        String postcode = (String) map.get("postcode");
        String country = (String) map.get("country");

        if (address1 != null) {
            return new AddressLocation(
                gpsLocation,
                address1,
                address2,
                town,
                county,
                postcode,
                country
            );
        }
        return null;
    }
}
