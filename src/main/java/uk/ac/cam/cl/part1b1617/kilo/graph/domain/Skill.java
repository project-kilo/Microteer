package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import java.util.Objects;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
public class Skill extends Entity {
    @Index(primary=true, unique=true) public String skill;
    public String description;

    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }
        if (!(o instanceof Skill)) {
            return false;
        }

        Skill other = (Skill) o;
        return  Objects.equals(skill, other.skill) &&
                Objects.equals(description, other.description);
    }

    public int hashCode() {
        return Objects.hash(skill, description);
    }
}
