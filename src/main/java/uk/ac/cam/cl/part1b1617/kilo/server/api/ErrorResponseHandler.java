package uk.ac.cam.cl.part1b1617.kilo.server.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import ratpack.error.ClientErrorHandler;
import ratpack.error.ServerErrorHandler;
import ratpack.handling.Context;
import ratpack.jackson.Jackson;

import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.GenericErrorResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ServerError;

public class ErrorResponseHandler implements ServerErrorHandler, ClientErrorHandler {

	private static final Logger logger = LoggerFactory.getLogger(ErrorResponseHandler.class);

	@Override
	public void error(Context context, Throwable throwable) throws Exception {
		if (throwable instanceof ErrorResponse) {
			ErrorResponse error = (ErrorResponse) throwable;
			context.getResponse().status(error.responseCode());
			context.render(Jackson.json(error));
		} else if (throwable instanceof JsonProcessingException) {
			JsonProcessingException error = (JsonProcessingException) throwable;
			this.error(context, new ErrorResponse(error.getMessage()));
		} else {
			logger.error("Unexpected error: {}", throwable.getMessage(), throwable);
			this.error(context, new ServerError("Unexpected error"));
		}
	}

	@Override
	public void error(Context context, int statusCode) throws Exception {
		switch (statusCode) {
			case 404:
				this.error(context, new NotFoundResponse(context.getRequest().getPath()));
				break;
			default:
				this.error(context, new GenericErrorResponse("API error", statusCode));
				break;
		}
	}
}
