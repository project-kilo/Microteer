package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.LoginUtils;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ForbiddenResponse;
import uk.ac.cam.cl.part1b1617.kilo.server.error.NotFoundResponse;

public class ManageHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(ManageHandler.class);

	protected static class CharityResult {
		public String name;
		public String email;
		public String description;
		public AddressLocation location;
		public String website;
		public String phone;
		public Double rating;

		public CharityResult(Organisation org) {
			this.name = org.name;
			this.email = org.email;
			this.description = org.description;
			this.location = org.location;
			this.website = org.website;
			this.phone = org.phone;
			this.rating = org.rating;
		}
	}

	@Override
	public void handle(Context context) {
		String email = context.getPathTokens().get("email");

		Control control = ControlFactory.makeControl();
		Organisation org;
		try {
			org = control.getOrganisation(email);
		} catch (EntryNotFoundException e) {
			throw new NotFoundResponse(email);
		}

		try {
			context.byMethod(s -> s
				.get(() -> this.get(context, org))
				.put(() -> this.update(context, org, control))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void get(Context context, Organisation org) {
		context.getResponse().status(200);
		context.render(Jackson.json(new CharityResult(org)));
	}

	private void update(Context context, Organisation org, Control control) {
		context.get(Session.class).getData().then(sessionData -> {
			User user = LoginUtils.getLoggedInOrgUser(control, context, sessionData);

			if (user.adminOf != org) {
				logger.warn("Forbidden attempt to edit charity {} by {}", org.email, user.email);
				throw new ForbiddenResponse("You are not authorized to update this charity");
			}

			Promise<CharityRequest> request = context.parse(
				Jackson.fromJson(CharityRequest.class)
			);
			request.then(r -> {
				r.validate();

				if (!user.email.equals(r.adminEmail)) {
					logger.info("Updated user {} to {}", user.email, r.adminEmail);
					user.email = r.adminEmail;
				}

				if (!user.fullName.equals(r.adminName)) {
					logger.info("Updated user {} name to {}", user.email, r.adminName);
					user.fullName = r.adminName;
				}

				ArrayList<String> passwordWithHashNew =
					HashPassword.generatePasswordHash(r.adminPassword);
				user.passwordHash = passwordWithHashNew.get(0);
				user.passwordSalt = passwordWithHashNew.get(1);
				logger.info("Updated password for {}", user.email);

				if (!org.email.equals(r.orgemail)) {
					logger.info("Updated charity {} to {}", org.email, r.orgemail);
					org.email = r.orgemail;
				}

				if (!org.name.equals(r.orgname)) {
					logger.info("Updated charity {} name to {}", org.email, r.orgname);
					org.name = r.orgname;
				}
				if (!org.phone.equals(r.phone)) {
					logger.info("Updated charity {} phone to {}", org.email, r.phone);
					org.phone = r.phone;
				}
				if (!org.website.equals(r.website)) {
					logger.info("Updated charity {} website to {}", org.email, r.website);
					org.website = r.website;
				}
				if (!org.description.equals(r.description)) {
					logger.info("Updated charity {} description to {}", org.email, r.description);
					org.description = r.description;
				}
				org.location = new AddressLocation(
					r.address1,
					r.address2,
					r.city,
					r.county,
					r.postcode,
					r.country
				);

				control.updateOrganisation(org);
				context.getResponse().status(200);
				context.render(Jackson.json(new CharityResult(org)));
			});
		});
	}
}
