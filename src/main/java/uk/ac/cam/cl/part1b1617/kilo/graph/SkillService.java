package uk.ac.cam.cl.part1b1617.kilo.graph;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Skill;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.VolunteerSkillEdge;

public interface SkillService {
	Skill getSkillForSkillName(String skillName);

	void addVolunteerSkillEdge(VolunteerSkillEdge volunteerSkillEdge);

	void addSkill(String s);
}
