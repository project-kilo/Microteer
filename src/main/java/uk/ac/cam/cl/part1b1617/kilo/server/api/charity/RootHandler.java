package uk.ac.cam.cl.part1b1617.kilo.server.api.charity;

import java.time.Instant;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.HashPassword;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.ValidationWithExceptions;
import uk.ac.cam.cl.part1b1617.kilo.control.Control;
import uk.ac.cam.cl.part1b1617.kilo.graph.DuplicateEntryException;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.server.ControlFactory;
import uk.ac.cam.cl.part1b1617.kilo.server.error.ErrorResponse;

public class RootHandler implements Handler {
	private static final Logger logger = LoggerFactory.getLogger(RootHandler.class);

	protected static class CharityExistsResponse extends ErrorResponse {
		private static final long serialVersionUID = 1L;

		public String identifier;

		public CharityExistsResponse(String identifier) {
			super(String.format("Charity %s already exists", identifier));
			this.identifier = identifier;
		}

		@Override
		public String getErrorType() {
			return "charityExists";
		}
	}

	protected static class UserExistsResponse extends ErrorResponse {
		private static final long serialVersionUID = 1L;

		public String identifier;

		public UserExistsResponse(String identifier) {
			super(String.format("User %s already exists", identifier));
			this.identifier = identifier;
		}

		@Override
		public String getErrorType() {
			return "userExists";
		}
	}

	@Override
	public void handle(Context context) {
		try {
			context.byMethod(s -> s
				.post(() -> this.signup(context))
			);
		} catch (Exception e) {
			context.error(e);
		}
	}

	private void signup(Context context) {
		Promise<CharityRequest> signup = context.parse(
			Jackson.fromJson(CharityRequest.class)
		);

		signup.then(s -> {
			Control control = ControlFactory.makeControl();
			s.validate();

			AddressLocation address = new AddressLocation(
				s.address1,
				s.address2,
				s.city,
				s.county,
				s.postcode,
				s.country
			);
			ValidationWithExceptions.validateAddress(address);

			ArrayList<String> passwordWithHash = HashPassword.generatePasswordHash(s.adminPassword);

			Organisation org = new Organisation(
				s.orgname,
				address,
				s.orgemail,
				s.website,
				s.phone
			);
			User admin = new User(
				passwordWithHash.get(0),
				passwordWithHash.get(1),
				Instant.now(),
				s.adminName,
				s.adminEmail,
				org
			);

			try {
				control.addOrganisation(org);
			} catch (DuplicateEntryException e) {
				logger.warn("Charity {} already exists", org.email);
				throw new CharityExistsResponse(org.email);
			}

			try {
				control.addOrganisationUser(admin, org);
			} catch (DuplicateEntryException e) {
				logger.warn("User {} already exists", admin.email);
				control.removeOrganisation(org);
				throw new UserExistsResponse(admin.email);
			}

			context.getResponse().status(201);
			context.header("Location", String.format("/api/v1/charity/%s", org.email));
			logger.info("Created charity {}", org.email);
			context.render(Jackson.json(null));
		});
	}
}
