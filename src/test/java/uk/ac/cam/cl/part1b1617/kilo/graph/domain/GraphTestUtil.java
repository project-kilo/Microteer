package uk.ac.cam.cl.part1b1617.kilo.graph.domain;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.neo4j.ogm.session.Session;

public class GraphTestUtil {
    public static void setId(Entity obj, long id) {
        obj.id = id;
    }

    public static void setIdOnSave(Session session) {
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Entity entity = (Entity) invocation.getArgument(0);
                if (entity.getId() == null) {
                    GraphTestUtil.setId(entity, 1);
                }
                return null;
            }
        }).when(session).save(Mockito.any(Entity.class));
    }
}
