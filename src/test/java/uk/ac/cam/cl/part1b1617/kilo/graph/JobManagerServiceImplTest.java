package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.neo4j.ogm.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;

import java.util.ArrayList;
import java.util.HashSet;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

public class JobManagerServiceImplTest {

	private JobManagerService jobManagerService;

	@Mock Session sessionMock;

	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Before
	public void setUp() throws Exception {
		jobManagerService = new JobManagerServiceImpl(sessionMock);
		GraphTestUtil.setIdOnSave(sessionMock);
	}

	@Test
	public void testAddJob() throws Exception {
		Job job = new Job();
		Organisation organisation = new Organisation();
		jobManagerService.addJob(job, organisation);
		verify(sessionMock).save(job);
	}

	@Test
	public void testRemoveJob() throws Exception {
		Job job = new Job();
		job.allocatedVolunteers = new HashSet<>();
		jobManagerService.removeJob(job);
		verify(sessionMock).delete(job);
	}

	@Test
	public void testUpdateJob() throws Exception {
		Job job = new Job();
		jobManagerService.updateJob(job);
		verify(sessionMock).save(job);
	}

	@Test
	public void testAcceptJob() throws Exception {
		Job jobMock = mock(Job.class);
		Volunteer volunteerMock = mock(Volunteer.class);
		JobAllocationEdge jobAllocationEdge = new JobAllocationEdge();
		VolunteerJobs volunteerJobs = new VolunteerJobs();

		when(volunteerMock.getJobAllocationEdgeFor(jobMock)).thenReturn(jobAllocationEdge);
		volunteerMock.jobs = volunteerJobs;
		volunteerJobs.accepted = new HashSet<>();
		jobMock.acceptedVolunteers = new HashSet<>();

		jobManagerService.acceptJob(jobMock, volunteerMock);

		assertTrue(volunteerMock.jobs.accepted.contains(jobMock));
		assertTrue(jobMock.acceptedVolunteers.contains(volunteerMock.jobs));
		verify(volunteerMock).getJobAllocationEdgeFor(jobMock);
		verify(sessionMock).delete(jobAllocationEdge);
	}

	@Test
	public void testRejectJob() throws Exception {
		Job job = new Job();
		Volunteer volunteerMock = mock(Volunteer.class);
		JobAllocationEdge jobAllocationEdge = new JobAllocationEdge();
		when(volunteerMock.getJobAllocationEdgeFor(job)).thenReturn(jobAllocationEdge);

		jobManagerService.rejectJob(job, volunteerMock);

		verify(sessionMock).delete(jobAllocationEdge);
	}

	@Test
	public void testUnacceptJob() throws Exception {
		Job job = new Job();
		Volunteer volunteer = new Volunteer();
		VolunteerJobs volunteerJobs = new VolunteerJobs();
		job.acceptedVolunteers = new HashSet<>();
		job.acceptedVolunteers.add(volunteerJobs);
		volunteer.jobs = volunteerJobs;
		volunteerJobs.accepted = new HashSet<>();
		volunteerJobs.accepted.add(job);
		job.allocatedVolunteers = new HashSet<>();
		volunteer.jobs.allocated = new HashSet<>();

		jobManagerService.unacceptJob(job, volunteer, 1);

		assertTrue(job.acceptedVolunteers.size() == 0);
		assertTrue(volunteer.jobs.accepted.size() == 0);
		assertTrue(job.allocatedVolunteers.size() == 1);
		assertTrue(volunteer.jobs.allocated.size() == 1);
		verify(sessionMock).save(any(JobAllocationEdge.class));
	}

	@Test
	public void testAllocateJob() throws Exception {
		Job job = new Job();
		Volunteer volunteer = new Volunteer();

		job.allocatedVolunteers = new HashSet<>();
		volunteer.jobs = new VolunteerJobs();
		volunteer.jobs.allocated = new HashSet<>();

		jobManagerService.suggestJob(job, volunteer, 1);

		assert(job.allocatedVolunteers.size() == 1);
		assert(volunteer.jobs.allocated.size() == 1);
		verify(sessionMock, times(1)).save(job);
		verify(sessionMock, times(1)).save(volunteer);
		verify(sessionMock, times(1)).save(any(JobAllocationEdge.class));

	}
}
