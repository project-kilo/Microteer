package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Arrays;
import uk.ac.cam.cl.part1b1617.kilo.UIValidation.UserInputValidation;

public class UIValidationTest {
	private static final UserInputValidation u = new UserInputValidation();
	
	@Test
	public void volunteerNameTestValid(){
		assertTrue(UserInputValidation.userName("William"));
		assertTrue(UserInputValidation.userName("Contains Spaces"));
	}
	
	@Test
	public void volunteerNameTestNotValid() {
		assertFalse(UserInputValidation.userName(""));
		assertFalse(UserInputValidation.userName("ThisFirstNameIsTooLongForThisInput"));
		
	}	
	
	@Test
	public void organisationNameTest() {
		assertTrue(UserInputValidation.organisationName("Charitable events org"));
		assertFalse(UserInputValidation.organisationName(""));
		assertFalse(UserInputValidation.organisationName("This organisations name is one character too long for this field"));
	}
	
	@Test
	public void descriptionTests() {
		//Create string of 512 characters
		char[] c = new char[512];
		Arrays.fill(c, 'f');
		String s = new String(c);
		assertFalse(UserInputValidation.description(s));
	}
	
	@Test
	public void organisationLocationTestTrue() {
		assertTrue(UserInputValidation.postCode("CH48 7HB"));
		assertTrue(UserInputValidation.postCode("CH487HB"));
		assertTrue(UserInputValidation.postCode("se34 5gh"));
	}
	
	@Test
	public void organisationLocationTestFalse() {
		assertFalse(UserInputValidation.postCode("thisistoolong"));
	}
	
	@Test
	public void organisationPhoneNumberTest() {
		assertTrue(UserInputValidation.phoneNumber("0151 678 9012"));
		assertFalse(UserInputValidation.phoneNumber("0151 678 90120151 678 90120151 6"));
	}
	@Test
	public void organisationWebsiteTestTrue() {
		assertTrue(UserInputValidation.organisationWebsite("http://www.google.com"));
		assertTrue(UserInputValidation.organisationWebsite("www.google.com"));
	}
	
	@Test
	public void organisationWebsiteTestFalse() {
		assertFalse(UserInputValidation.organisationWebsite("www.i.com"));
		assertFalse(UserInputValidation.organisationWebsite("organisation"));
		assertFalse(UserInputValidation.organisationWebsite(""));
	}
	
	@Test
	public void organisationEmailTest() {
		assertTrue(UserInputValidation.email("email@email.com"));
		assertFalse(UserInputValidation.email(""));
		char[] c = new char[255];
		Arrays.fill(c, 'f');
		String longEmail = new String(c);
		assertFalse(UserInputValidation.email(longEmail));
	}
	
	@Test
	public void userPasswordTestTrue() {
		assertTrue(UserInputValidation.userPassword("password1"));
	}
	
	@Test
	public void userPasswordTestFalse() {
		assertFalse(UserInputValidation.userPassword("passw"));
	}
}