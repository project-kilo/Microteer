package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.neo4j.ogm.session.Session;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Skill;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.VolunteerSkillEdge;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SkillServiceImplTest {

	private SkillService skillService;

	@Mock
	Session sessionMock;


	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Before
	public void setUp() throws Exception {
		skillService = new SkillServiceImpl(sessionMock);
	}

	@Test
	public void testGetSkillForSkillNameSkillExists() throws Exception {
		String skillName = "test skill name";
		Skill skill = new Skill();
		skill.skill = skillName;

		skillService.getSkillForSkillName(skillName);

		verify(sessionMock).load(Skill.class, skillName);
	}

	@Test
	public void testGetSkillForSkillNameSkillDoesNotExist() throws Exception {
		String skillName = "test skill name";
		Skill skill = new Skill();
		skill.skill = skillName;
		when(sessionMock.load(Skill.class, skillName)).thenReturn(null);


		skillService.getSkillForSkillName(skillName);

		verify(sessionMock).load(Skill.class, skillName);
		verify(sessionMock).save(any(Skill.class));
	}

	@Test
	public void testAddVolunteerSkillEdge() throws Exception {
		VolunteerSkillEdge volunteerSkillEdge = new VolunteerSkillEdge();
		skillService.addVolunteerSkillEdge(volunteerSkillEdge);
		verify(sessionMock).save(volunteerSkillEdge);
	}
}
