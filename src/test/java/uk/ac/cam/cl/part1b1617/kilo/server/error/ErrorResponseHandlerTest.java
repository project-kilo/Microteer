package uk.ac.cam.cl.part1b1617.kilo.server.error;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import ratpack.handling.Context;
import ratpack.http.Response;
import ratpack.jackson.JsonRender;
import uk.ac.cam.cl.part1b1617.kilo.server.api.ErrorResponseHandler;

public class ErrorResponseHandlerTest {
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	@Mock protected Context context;
	@Mock protected Response response;

	protected ErrorResponseHandler handler;

	private static class TestErrorResponse extends ErrorResponse {
		private static final long serialVersionUID = 1L;
		public TestErrorResponse() {
			super("some error");
		}
		@Override
		public int responseCode() {
			return 123;
		}
	}

	@Before
	public void setUp() {
		this.handler = new ErrorResponseHandler();
		when(this.context.getResponse()).thenReturn(this.response);
	}

	@Test
	public void testRenderErrorResponse() throws Exception {
		ErrorResponse errorResponse = spy(new ErrorResponse("some error"));
		this.handler.error(this.context, errorResponse);
		verify(errorResponse).responseCode();
		verify(this.response).status(400);

		verify(this.context).render(argThat(json -> ((JsonRender)json).getObject() == errorResponse));
	}

	@Test
	public void testRenderErrorResponseSubclass() throws Exception {
		TestErrorResponse errorResponse = spy(new TestErrorResponse());
		this.handler.error(this.context, errorResponse);
		verify(errorResponse).responseCode();
		verify(this.response).status(123);

		verify(this.context).render(argThat(json -> ((JsonRender)json).getObject() == errorResponse));
	}

	@Test
	public void testRenderThrowable() throws Exception {
		Exception exception = spy(new Exception());
		this.handler.error(this.context, exception);
		verify(this.response).status(500);
		verify(this.context).render(argThat(json -> ((JsonRender)json).getObject() instanceof ServerError));
	}
}
