package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

import static org.junit.Assert.*;

import java.io.IOException;

import org.json.JSONException;
import org.junit.Test;

import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;

public class GeocodeTest {
	
	/*
	 * Coordinates - partition the test inputs into:
	 * Equal; not equal
	 * Same hashcode and equal; same hash code and not equal
	 * 
	 * Geocode - partition the test inputs into:
	 * Coordinates found; coordinates not found
	 * Search: full address, city, country, postal code
	 * Case Sensitivity
	 */
	
	@Test
	public void coordinatesEqualTest() {
		GPSLocation coord1 = new GPSLocation(45.0,45.0);
		GPSLocation coord2 = new GPSLocation(45.0,45.0);
		assertTrue(coord1.equals(coord2));
	}
	
	@Test
	public void coordinatesEqualPrecisionTest() {
		GPSLocation coord1 = new GPSLocation(45.0,45.0);
		GPSLocation coord2 = new GPSLocation(45.00,45.00);
		assertTrue(coord1.equals(coord2));
	}
	
	@Test
	public void coordinatesNotEqualTest() {
		GPSLocation coord1 = new GPSLocation(45.0,45.0);
		GPSLocation coord2 = new GPSLocation(46.0,45.0);
		assertFalse(coord1.equals(coord2));
	}
	
	@Test
	public void coordinatesNotEqualSameHashcodeTest() {
		GPSLocation coord1 = new GPSLocation(45.0,45.0);
		GPSLocation coord2 = new GPSLocation(60.0,30.0);
		assertFalse(coord1.equals(coord2));
	}

	@Test(expected = JSONException.class)
	public void coordinatesNotFoundTest() throws IOException {
		String query = "momomo";
		Geocode.geocode(query);
	}

	@Test
	public void cityTest() throws IOException, JSONException {
		String query = "Baileys Harbor";
		GPSLocation geocoded = Geocode.geocode(query);
		assertEquals(geocoded,new GPSLocation(45.064993,-87.1242739));
	}

	@Test
	public void addressTest() throws IOException, JSONException {
		String query = "700 Cottage Place, Teaneck";
		GPSLocation geocoded = Geocode.geocode(query);
		assertEquals(geocoded,new GPSLocation(40.9149568003259,-74.0258532302821));
	}
	
	@Test
	public void countryTest() throws IOException, JSONException {
		String query = "France";
		GPSLocation geocoded = Geocode.geocode(query);
		assertEquals(geocoded,new GPSLocation(46.603354,1.8883335));
	}
	
	@Test
	public void caseSensitivityTest() throws IOException, JSONException {
		String query1 = "CB2 1TP";
		GPSLocation geocodedCaps = Geocode.geocode(query1);
		String query2 = "cb2 1tp";
		GPSLocation geocodedLower = Geocode.geocode(query2);
		assertEquals(geocodedCaps,geocodedLower);
	}
	
}
