package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.neo4j.ogm.session.Session;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.*;

public class UserManagerTest {

	private UserManager userManager;

	@Mock
	Session mockSession;

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Before
	public void setUp() throws Exception {
		userManager = new UserManager(mockSession);
		GraphTestUtil.setIdOnSave(mockSession);
	}

	@Test
	public void updateVolunteerTest() {
		Volunteer volunteer = new Volunteer();
		userManager.updateVolunteer(volunteer);
		verify(mockSession).save(volunteer);
	}

	@Test
	public void updateOrganisationTest() {
		Organisation organisation = new Organisation();
		userManager.updateOrganisation(organisation);
		verify(mockSession).save(organisation);
	}

	@Test
	public void updateOrganisationUserTest() {
		User user = new User();
		userManager.updateOrganisationUser(user);
		verify(mockSession).save(user);
	}

	@Test
	public void removeVolunteerTest() {
		Volunteer volunteer = new Volunteer();
		userManager.removeVolunteer(volunteer);
		verify(mockSession).delete(volunteer);
	}

	@Test
	public void removeOrganisationTest() {
		Organisation org = spy(new Organisation());
		doReturn(42L).when(org).getId();
		Job job1 = new Job();
		Job job2 = new Job();

		org.email = "foo42@bar.com";
		org.jobs.add(job1);
		org.jobs.add(job2);

		doReturn(org).when(mockSession).load(Organisation.class, "foo42@bar.com", 1);
		userManager.removeOrganisation(org);
		verify(mockSession).delete(job1);
		verify(mockSession).delete(job2);
		verify(mockSession).delete(org);
	}

	@Test
	public void removeOrganisationUserTest() {
		User user = new User();
		userManager.removeOrganisationUser(user);
		verify(mockSession).delete(user);
	}

	@Test
	public void addVolunteerTest() throws DuplicateEntryException {
		Volunteer volunteer = new Volunteer();
		userManager.addVolunteer(volunteer);
		verify(mockSession).save(volunteer);
	}

	@Test(expected = DuplicateEntryException.class)
	public void addVolunteerDuplicateTest() throws DuplicateEntryException {
		Volunteer volunteer = new Volunteer();
		User user = new User();
		user.email = "leah@gmail.com";
		volunteer.user = user;
		userManager.addVolunteer(volunteer);
		verify(mockSession).save(volunteer);
		when(mockSession.load(User.class,user.email)).thenReturn(user);
		userManager.addVolunteer(volunteer);
	}

	@Test
	public void addOrganisationTest() throws DuplicateEntryException {
		Organisation organisation = new Organisation();
		userManager.addOrganisation(organisation);
		verify(mockSession).save(organisation);
	}

	@Test(expected = DuplicateEntryException.class)
	public void addOrganisationDuplicateTest() throws DuplicateEntryException {
		Organisation organisation = new Organisation();
		organisation.email = "leah@gmail.com";
		userManager.addOrganisation(organisation);
		verify(mockSession).save(organisation);
		when(mockSession.load(Organisation.class,organisation.email)).thenReturn(organisation);
		userManager.addOrganisation(organisation);
	}

	@Test
	public void addOrganisationUserTest() throws DuplicateEntryException {
		User user = new User();
		userManager.addOrganisationUser(user);
		verify(mockSession).save(user);
	}

	@Test(expected = DuplicateEntryException.class)
	public void addOrganisationUserDuplicateTest() throws DuplicateEntryException {
		User user = new User();
		user.email = "leah@gmail.com";
		userManager.addOrganisationUser(user);
		verify(mockSession).save(user);
		when(mockSession.load(User.class,user.email)).thenReturn(user);
		userManager.addOrganisationUser(user);
	}
	
	@Test(expected = EntryNotFoundException.class)
	public void getUserNotFoundTest() throws EntryNotFoundException {
		User user = new User();
		String email = "leah@gmail.com";
		user.email = email;
		userManager.getUserForEmail(email);
	}
	
	@Test
	public void getUserTestWhenVolunteer() throws EntryNotFoundException, DuplicateEntryException {
		Volunteer volunteer = mock(Volunteer.class);
		User user = new User();
		String email = "leah@gmail.com";
		user.email = email;
		user.volunteer = volunteer;
		VolunteerJobs volunteerJobs = mock(VolunteerJobs.class);
		volunteer.jobs = volunteerJobs;

		when(mockSession.load(User.class,email)).thenReturn(user);
		when(mockSession.load(Volunteer.class, 42L, 1)).thenReturn(volunteer);
		when(mockSession.load(VolunteerJobs.class, 43L, 1)).thenReturn(volunteerJobs);
		when(volunteer.getId()).thenReturn((long) 42);
		when(volunteerJobs.getId()).thenReturn((long) 43);

		User userReturned = userManager.getUserForEmail(email);

		assertEquals(user,userReturned);
		assertEquals(userReturned.volunteer.jobs,volunteerJobs);
		assertEquals(userReturned.volunteer, volunteer);
	}

}
