package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import static org.junit.Assert.*;

import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;

public class GPSLocationConverterTest {

    private static final double EPSILON = 0.001;
    private static final GPSLocationConverter CONVERTER = new GPSLocationConverter();

    @Test
    public void convertToGraphProperties() {
        GPSLocation location = new GPSLocation(
            0.5,
            3.6
        );

        Map<String, ?> result = CONVERTER.toGraphProperties(location);

        assertEquals(0.5, (Double) result.get("latitude"), EPSILON);
        assertEquals(3.6, (Double) result.get("longitude"), EPSILON);
    }

    @Test
    public void nullToGraphProperties() {
        Map<String, ?> result = CONVERTER.toGraphProperties(null);

        assertFalse(result.containsKey("latitude"));
        assertFalse(result.containsKey("longitude"));
    }

    @Test
    public void convertToGPSLocation() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("latitude", (Double) 0.5);
        map.put("longitude", (Double) 3.6);

        GPSLocation result = CONVERTER.toEntityAttribute(map);

        assertNotNull(result);
        assertEquals(0.5, result.latitude, EPSILON);
        assertEquals(3.6, result.longitude, EPSILON);
    }

    @Test
    public void invalidToGPSLocation() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("foobar", (Double) 0.5);

        GPSLocation result = CONVERTER.toEntityAttribute(map);

        assertNull(result);
    }
}
