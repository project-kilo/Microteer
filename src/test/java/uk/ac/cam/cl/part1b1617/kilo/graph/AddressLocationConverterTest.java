package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;

public class AddressLocationConverterTest {

    private static final double EPSILON = 0.001;
    private static final AddressLocationConverter CONVERTER = new AddressLocationConverter();

    @Test
    public void convertToGraphProperties() {
        AddressLocation location = new AddressLocation(
            new GPSLocation(0.5, 3.6),
            "foo",
            "bar",
            "baz",
            "qux",
            "deadbeef",
            "123"
        );

        Map<String, ?> result = CONVERTER.toGraphProperties(location);

        assertEquals(0.5, (Double) result.get("latitude"), EPSILON);
        assertEquals(3.6, (Double) result.get("longitude"), EPSILON);
        assertEquals("foo", (String) result.get("address1"));
        assertEquals("bar", (String) result.get("address2"));
        assertEquals("baz", (String) result.get("town"));
        assertEquals("qux", (String) result.get("county"));
        assertEquals("deadbeef", (String) result.get("postcode"));
        assertEquals("123", (String) result.get("country"));
    }

    @Test
    public void nullToGraphProperties() {
        Map<String, ?> result = CONVERTER.toGraphProperties(null);

        assertFalse(result.containsKey("latitude"));
        assertFalse(result.containsKey("longitude"));
        assertFalse(result.containsKey("address1"));
        assertFalse(result.containsKey("address2"));
        assertFalse(result.containsKey("town"));
        assertFalse(result.containsKey("county"));
        assertFalse(result.containsKey("postcode"));
        assertFalse(result.containsKey("country"));
    }

    @Test
    public void convertToAddressLocation() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("latitude", (Double) 0.5);
        map.put("longitude", (Double) 3.6);
        map.put("address1", (String) "foo");
        map.put("address2", (String) "bar");
        map.put("town", (String) "baz");
        map.put("county", (String) "qux");
        map.put("postcode", (String) "deadbeef");
        map.put("country", (String) "123");

        AddressLocation result = CONVERTER.toEntityAttribute(map);

        assertNotNull(result);
        assertNotNull(result.gps);
        assertEquals("foo", result.address1);
        assertEquals("bar", result.address2);
        assertEquals("baz", result.town);
        assertEquals("qux", result.county);
        assertEquals("deadbeef", result.postcode);
        assertEquals("123", result.country);
    }

    @Test
    public void convertMinimalToAddressLocation() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("latitude", (Double) 0.5);
        map.put("longitude", (Double) 3.6);
        map.put("address1", (String) "foo");

        AddressLocation result = CONVERTER.toEntityAttribute(map);

        assertNotNull(result);
        assertNotNull(result.gps);
    }

    @Test
    public void invalidToAddressLocation() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("foobar", (Double) 0.5);

        AddressLocation result = CONVERTER.toEntityAttribute(map);

        assertNull(result);
    }

    @Test
    public void convertToAddressLocationWithoutGPS() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("address1", (String) "foo");

        AddressLocation result = CONVERTER.toEntityAttribute(map);

        assertNotNull(result);
        assertNull(result.gps);
        assertEquals("foo", result.address1);
    }

   /* @Test
    public void convertToGraphPropertiesWithoutGPS() {
        AddressLocation location = new AddressLocation(
            "foo",
            "bar",
            "baz",
            "qux",
            "deadbeef",
            "123"
        );

        Map<String, ?> result = CONVERTER.toGraphProperties(location);

        assertFalse(result.containsKey("latitude"));
        assertFalse(result.containsKey("longitude"));
        assertEquals("foo", (String) result.get("address1"));
        assertEquals("bar", (String) result.get("address2"));
        assertEquals("baz", (String) result.get("town"));
        assertEquals("qux", (String) result.get("county"));
        assertEquals("deadbeef", (String) result.get("postcode"));
        assertEquals("123", (String) result.get("country"));
    }*/
}
