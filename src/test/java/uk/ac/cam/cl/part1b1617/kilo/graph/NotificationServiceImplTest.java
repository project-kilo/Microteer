package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Ignore;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.neo4j.ogm.session.Session;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Notification;

import static org.mockito.Mockito.*;

public class NotificationServiceImplTest {

	private NotificationService notificationService;

	@Mock
	Session sessionMock;

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Before
	public void setUp() throws Exception {
		notificationService = new NotificationServiceImpl(sessionMock);
	}

	@Ignore @Test
	public void testAddNotification() throws Exception {
		Notification notification = new Notification();
		notificationService.addNotification(notification);
		verify(sessionMock).save(notification);
	}

	@Ignore @Test
	public void testRemoveNotification() throws Exception {
		Notification notification = new Notification();
		notificationService.removeNotification(notification);
		verify(sessionMock).delete(notification);
	}
}