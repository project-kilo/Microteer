package uk.ac.cam.cl.part1b1617.kilo.recommender;

import java.util.HashSet;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Skill;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.VolunteerSkillEdge;



public class RecommenderTest {

    private static final int SKILLCOUNT = 10;
    private static final double EPS = 1.0e-6;

    //All available skills
    private static Skill[] skills = new Skill[SKILLCOUNT];

    //This method returns a Job with specific skills
    private Job createJob(Skill[] required, Skill[] wanted) {
        Job j = new Job();

        j.requiredSkills = new HashSet<Skill>();
        for (Skill skill : required) {
            j.requiredSkills.add(skill);
        }

        j.wantedSkills = new HashSet<Skill>();
        for (Skill skill : wanted) {
            j.wantedSkills.add(skill);
        }

        return j;
    }

    //This method return a SkillEdge
    private VolunteerSkillEdge edge(Skill skill, double confidence) {
        VolunteerSkillEdge edge = new VolunteerSkillEdge();

        edge.skill = skill;
        edge.confidence = confidence;

        return edge;
    }

    //This method returns a Volunteer with specific skills
    private Volunteer createVolunteer(VolunteerSkillEdge[] skills) {
        Volunteer v = new Volunteer();

        v.skills = new HashSet<VolunteerSkillEdge>();
        for (VolunteerSkillEdge skill : skills) {
            v.skills.add(skill);
        }

        return v;
    }


    @BeforeClass
    public static void defineDummySkills() {
        for (int i = 0; i < SKILLCOUNT; i++) {
            skills[i] = new Skill();
            skills[i].description = "S"+i;
        }
    }

    //score should be non-zero because volunteer
    //has all required skills
    @Test
    public void testRecommenderScoreNotZero1() {
        Job j = createJob(  new Skill[] {skills[0], skills[2]},
                            new Skill[] {skills[3], skills[4]}
                          );

        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0),
                                                                    edge(skills[1], 0.0),
                                                                    edge(skills[2], 2.0),
                                                                    edge(skills[3], 3.0),
                                                                    edge(skills[4], 0.0)
                                                                });
        assertTrue( Recommender.score(v,j) > 0 + EPS );
    }

    //empty required skill set should yield non-zero score
    //provided wanted and volunteer sets are not disjoint
    @Test
    public void testRecommenderScoreNotZero2() {
        Job j = createJob(  new Skill[] {},
                            new Skill[] {skills[0], skills[2], skills[1], skills[3], skills[4]}
                          );

        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0),
                                                                    edge(skills[1], 0.0),
                                                                    edge(skills[2], 2.0),
                                                                    edge(skills[3], 3.0),
                                                                    edge(skills[4], 0.0)
                                                                });
        assertTrue( Recommender.score(v,j) > 0 + EPS );
    }

    //volunteer doesnt have all required skills by a Job
    //score should be zero
    @Test
    public void testRecommenderScoreZero() {
        Job j = createJob(  new Skill[] {skills[0], skills[2], skills[1], skills[3], skills[4]},
                            new Skill[] {}
                          );

        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0),
                                                                    edge(skills[1], 0.0),
                                                                    edge(skills[2], 2.0),
                                                                    edge(skills[3], 3.0),
                                                                    edge(skills[4], 0.0)
                                                                });
        double expected = 0.0;
        assertEquals( expected, Recommender.score(v,j), EPS );
    }

    @Test
    public void testRecommenderUpdateScoreIncrease() {
        Job j = createJob(  new Skill[] {skills[0], skills[2]},
                            new Skill[] {skills[3], skills[4]}
                          );

        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0),
                                                                    edge(skills[1], 0.0),
                                                                    edge(skills[2], 2.0),
                                                                    edge(skills[3], 3.0),
                                                                    edge(skills[4], 0.0)
                                                                });

        double prev = 0.0;
        for (int i = 0; i < 10; i++) {
            Recommender.updateVolunteerOnAction(v, j, EAction.ACCEPT);
            double cur = Recommender.score(v, j);
            assertTrue( (cur - prev) > 0 );
            prev = cur;
        }

    }

    @Test
    public void testRecommenderUpdateScoreDecrease() {
        Job j = createJob(  new Skill[] {skills[0], skills[2]},
                            new Skill[] {skills[3], skills[4]}
                          );

        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0E+6),
                                                                    edge(skills[1], 0.0),
                                                                    edge(skills[2], 5.0E+5),
                                                                    edge(skills[3], 1.0E+6),
                                                                    edge(skills[4], 0.0)
                                                                });

        double prev = 1.0E+8;
        for (int i = 0; i < 10; i++) {
            Recommender.updateVolunteerOnAction(v, j, EAction.REJECT);
            double cur = Recommender.score(v, j);
            assertTrue( (prev - cur) > 0 );
            prev = cur;
        }

    }

    @Test
    public void testScoreMoreSuitableVolunteer() {
        Job j = createJob(  new Skill[] {skills[0], skills[1], skills[2]},
                            new Skill[] {skills[3], skills[4], skills[5]}
                          );

        Volunteer vBetter = createVolunteer( new VolunteerSkillEdge[] { edge(skills[0], 1.0),
                                                                        edge(skills[1], 1.0),
                                                                        edge(skills[2], 1.0),
                                                                        edge(skills[3], 3.0),
                                                                        edge(skills[4], 3.0),
                                                                        edge(skills[5], 3.0)
                                                                        });

        Volunteer vWorse = createVolunteer( new VolunteerSkillEdge[] {  edge(skills[0], 2.0),
                                                                        edge(skills[1], 1.0),
                                                                        edge(skills[2], 1.0),
                                                                        edge(skills[3], 1.0),
                                                                        edge(skills[4], 2.0),
                                                                        edge(skills[5], 2.0)
                                                                        });

        assertTrue(  Recommender.score(vBetter, j) > Recommender.score(vWorse, j) + EPS );
    }

    @Test
    public void testScoreMoreSuitableJob() {
        Job jBetter = createJob(  new Skill[] {skills[0], skills[1], skills[2]},
                            new Skill[] {skills[3], skills[4], skills[5]}
                          );

        Job jWorse = createJob(  new Skill[] {skills[0], skills[1], skills[2]},
                            new Skill[] {skills[6], skills[7], skills[8]}
                          );

        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0),
                                                                    edge(skills[1], 1.0),
                                                                    edge(skills[2], 1.0),
                                                                    edge(skills[3], 3.0),
                                                                    edge(skills[4], 3.0),
                                                                    edge(skills[5], 3.0)
                                                                });

        assertTrue(  Recommender.score(v, jBetter) > Recommender.score(v, jWorse) + EPS );
    }

    //Volunteer's wanted skills match
    //but required dont so score is zero
    @Test
    public void testRecommenderScoreRequiredSkillsNoMatch() {
        Job j = createJob(  new Skill[] {skills[0], skills[1], skills[2], skills[3], skills[4]},
                            new Skill[] {skills[5], skills[6], skills[7], skills[8], skills[9]}
                          );

        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0),
                                                                    edge(skills[1], 1.0),
                                                                    edge(skills[2], 2.0),
                                                                    edge(skills[5], 3.0),
                                                                    edge(skills[6], 2.0),
                                                                    edge(skills[7], 3.0),
                                                                    edge(skills[8], 1.0),
                                                                    edge(skills[9], 1.0)
                                                                });
        double expected = 0.0;
        assertEquals( expected, Recommender.score(v, j), EPS );
    }

    //Initially Volunteer v1 is more suitable than v2 for Job j1
    //However, after v2 accepts j1 it becomes more suitable for j1
    //Then v1 accepts j1 and becomes more suitable for j1 again
    @Test
    public void testScoreUpdateMoreSuitableVolunteer() {
        Job j = createJob(  new Skill[] {skills[0], skills[1], skills[2]},
                            new Skill[] {skills[3], skills[4], skills[5]}
                          );

        Volunteer v1 = createVolunteer( new VolunteerSkillEdge[] {      edge(skills[0], 1.0),
                                                                        edge(skills[1], 1.0),
                                                                        edge(skills[2], 1.0),
                                                                        edge(skills[3], 3.0),
                                                                        edge(skills[4], 3.0),
                                                                        edge(skills[5], 3.0)
                                                                        });

        Volunteer v2 = createVolunteer( new VolunteerSkillEdge[] {  edge(skills[0], 2.0),
                                                                    edge(skills[1], 1.0),
                                                                    edge(skills[2], 1.0),
                                                                    edge(skills[3], 1.0),
                                                                    edge(skills[4], 2.0),
                                                                    edge(skills[5], 2.0)
                                                                 });

        //v1 better than v2 for j1
        assertTrue(  Recommender.score(v1, j) > Recommender.score(v2, j) + EPS );
        Recommender.updateVolunteerOnAction(v2, j, EAction.ACCEPT);
        //after v2 accepts j1, v2 better than v1 for j1
        assertFalse(  Recommender.score(v1, j) > Recommender.score(v2, j) + EPS );
        Recommender.updateVolunteerOnAction(v1, j, EAction.ACCEPT);
        //after v1 accepts j1, v1 is again bettter than v2 for j1
        assertTrue(  Recommender.score(v1, j) > Recommender.score(v2, j) + EPS );
    }

    //Volunteer rejects job and his confidence for all skills becomes zero
    //So all skills are deleted
    @Test
    public void testUpdateDeleteZeroSkills() {
        Job j = createJob(  new Skill[] {skills[0], skills[1], skills[2]},
                            new Skill[] {skills[3], skills[4], skills[5]}
                          );


        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {   edge(skills[0], 1.0),
                                                                    edge(skills[1], 1.0),
                                                                    edge(skills[2], 1.0),
                                                                    edge(skills[3], 3.0),
                                                                    edge(skills[4], 3.0),
                                                                    edge(skills[5], 3.0)
                                                                });

        Recommender.updateVolunteerOnAction(v, j, EAction.REJECT);
        assertEquals( 0, v.skills.size() );
    }

    //Volunteer with no skills accepts a job and acquires its skills
    @Test
    public void testUpdateAddNewSkills() {
        Job j = createJob(  new Skill[] {},
                            new Skill[] {skills[0], skills[1], skills[2]}
                          );


        Volunteer v = createVolunteer( new VolunteerSkillEdge[] {});

        Recommender.updateVolunteerOnAction(v, j, EAction.ACCEPT);
        assertEquals( j.wantedSkills.size(), v.skills.size() );
    }
}
