package uk.ac.cam.cl.part1b1617.kilo.UIValidation;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

import org.junit.Test;

public class HashPasswordTest {
	/*
	 * generatePasswordHash
	 * 
	 * checkPassword
	 * 
	 */
	
	@Test
	public void checkPasswordEqualTest() throws NoSuchAlgorithmException, InvalidKeySpecException {
		String password = "password";
		ArrayList<String> passwordDetails = HashPassword.generatePasswordHash(password);
		String passwordHash = passwordDetails.get(0);
		String salt = passwordDetails.get(1);
		assertTrue(HashPassword.checkPassword(password,passwordHash,salt));
	}
}

