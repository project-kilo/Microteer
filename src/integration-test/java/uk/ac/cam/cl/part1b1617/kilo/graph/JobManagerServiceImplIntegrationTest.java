package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;

import static org.junit.Assert.*;

import java.util.Iterator;

public class JobManagerServiceImplIntegrationTest extends GraphIntegrationTestBase {
	protected JobManagerService service;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.service = new JobManagerServiceImpl(this.session);
	}

	@Test
	public void testAddUpdateRemove() {
		GPSLocation gpsLocation = new GPSLocation(0.0002, 0.0002);
		AddressLocation addressLocation = new AddressLocation(gpsLocation, "line 1", "line2", "town",
				"county", "OX56 7TF", "UK");
		Organisation organisation = new Organisation("some organisation", addressLocation, "org@bar.com",
				"www.org.com", "03456936455");

		Job job = this.constructJob("A Job");
		this.service.addJob(job, organisation);

		Iterator<Job> jobs = this.session.loadAll(Job.class).iterator();
		assertTrue(jobs.hasNext());
		Job dbJob = jobs.next();
		assertEquals("A Job", dbJob.summary);
		assertFalse(jobs.hasNext());

		this.session.clear();
		job.summary = "Another Job";
		this.service.updateJob(job);

		jobs = this.session.loadAll(Job.class).iterator();
		assertTrue(jobs.hasNext());
		dbJob = jobs.next();
		assertEquals("Another Job", dbJob.summary);
		assertFalse(jobs.hasNext());

		this.session.clear();
		this.service.removeJob(job);
		jobs = this.session.loadAll(Job.class).iterator();
		assertFalse(jobs.hasNext());
	}
}
