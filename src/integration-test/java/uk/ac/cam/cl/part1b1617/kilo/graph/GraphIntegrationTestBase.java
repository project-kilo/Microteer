package uk.ac.cam.cl.part1b1617.kilo.graph;

import java.time.Instant;
import java.time.OffsetDateTime;

import org.junit.Before;
import org.neo4j.ogm.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;
import uk.ac.cam.cl.part1b1617.kilo.IntegrationTestBase;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;

abstract public class GraphIntegrationTestBase extends IntegrationTestBase {
	protected Session session;

	@Before
	public void setUp() throws Exception {
		database.purge();
		this.session = database.openSession();
	}

	protected AddressLocation dummyAddressLocation(
		GPSLocation gps,
		String address1
	) {
		return new AddressLocation(
			gps,
			address1,
			"15 JJ Thompson Avenue",
			"Cambrige",
			"Cambridgeshire",
			"CB3 0FD",
			"United Kingdom"
		);
	}

	protected AddressLocation dummyAddressLocation() {
		return this.dummyAddressLocation(this.dummyGPSLocation(), "WGB");
	}

	protected GPSLocation dummyGPSLocation() {
		return new GPSLocation(52.202925,0.090530);
	}

	protected Job constructJob(String summary) {
		Job job = new Job();
		job.summary = summary;
		job.description = "Some work";
		job.location = this.dummyAddressLocation();
		job.expiration = OffsetDateTime.now().plusDays(1);
		job.requiredVolunteers = 1L;
		return job;
	}

	protected Volunteer constructVolunteer(String name, String email) {
		Volunteer volunteer = new Volunteer(
			email,
			name,
			Instant.now().plusSeconds(-3600),
			"h4sh", "salt",
			this.dummyGPSLocation(),
			50,
			"0123456789"
		);
		return volunteer;
	}

	protected Organisation constructOrganisation(String email) {
		Organisation organisation = new Organisation(
			"some organisation",
			this.dummyAddressLocation(),
			email,
			"www.example.com",
			"03456936455"
		);
		return organisation;
	}

}
