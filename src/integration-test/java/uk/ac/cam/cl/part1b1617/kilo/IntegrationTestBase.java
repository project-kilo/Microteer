package uk.ac.cam.cl.part1b1617.kilo;

import uk.ac.cam.cl.part1b1617.kilo.graph.Database;
import uk.ac.cam.cl.part1b1617.kilo.graph.DatabaseImpl;

abstract public class IntegrationTestBase {
	public static Database database;

	static {
		database = new DatabaseImpl();
		database.purge();
	}

}
