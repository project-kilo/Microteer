package uk.ac.cam.cl.part1b1617.kilo.graph;

import org.junit.Before;
import org.junit.Test;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;

import java.time.DayOfWeek;
import java.util.Map;

import com.google.common.collect.Maps;

import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;
import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Time;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;

import static org.junit.Assert.*;

public class MatchingServiceImplIntegrationTest extends GraphIntegrationTestBase {
	protected MatchingService service;
	protected UserManagerService userManager;
	protected JobManagerService jobManager;
	protected TimePeriodService timePeriodService;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.service = new MatchingServiceImpl(this.session);
		this.userManager = new UserManager(this.session);
		this.jobManager = new JobManagerServiceImpl(this.session);
		this.timePeriodService = new TimePeriodServiceImpl(this.session);
	}

	/**
	 * Create a Volunteer and several Jobs, some of which are within the
	 * maxDistancePreferenceKm of the Volunteer, some of which aren't, and
	 * retrieve all nearby Jobs.
	 *
	 * Time periods are ignored.
	 */
	@Test
	public void testGetNearbyJobs() throws DuplicateEntryException {
		Time time = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING);

		Volunteer volunteer = this.constructVolunteer("Foo bar", "foo@bar.com");
		volunteer.location = new GPSLocation(52.202925,0.090530);
		volunteer.maxDistancePreferenceKm = 50;
		volunteer.times.add(time);

		Job job1 = this.constructJob("Job 1");
		job1.location = this.dummyAddressLocation(new GPSLocation(52.2029252,0.0905302), "a");
		job1.time = time;
		Job job2 = this.constructJob("Job 2");
		job2.location = this.dummyAddressLocation(new GPSLocation(52.2029252,0.0905302), "b");
		job2.time = time;
		Job job3 = this.constructJob("Job 3");
		job3.location = this.dummyAddressLocation(new GPSLocation(38.8994611,-77.0846061), "c");
		job3.time = time;

		Organisation organisation = this.constructOrganisation("foo@example.com");

		this.userManager.addVolunteer(volunteer);
		this.jobManager.addJob(job1, organisation);
		this.jobManager.addJob(job2, organisation);
		this.jobManager.addJob(job3, organisation);

		Iterable<Job> jobs = this.service.getMatchingJobs(volunteer);
		Map<String, Job> jobMap = Maps.uniqueIndex(jobs, job -> job.summary);

		assertEquals(2, jobMap.size());
		assertTrue(jobMap.containsKey("Job 1"));
		assertTrue(jobMap.containsKey("Job 2"));
		assertFalse(jobMap.containsKey("Job 3"));
	}

	/**
	 * Create a Job and several Volunteers, some of which are within the
	 * job search radius, some of which aren't, and retrieve all nearby
	 * Volunteers.
	 *
	 * Time periods are ignored.
	 */
	@Test
	public void testGetNearbyVolunteers() throws DuplicateEntryException {
		Time time = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING);

		Job job = this.constructJob("Job");
		job.location = this.dummyAddressLocation(new GPSLocation(52.202925,0.090530), "a");
		job.time = time;

		Volunteer volunteer1 = this.constructVolunteer("Volunteer 1", "foo1@bar.com");
		volunteer1.location = new GPSLocation(52.1855461,0.1323421);
		volunteer1.times.add(time);
		Volunteer volunteer2 = this.constructVolunteer("Volunteer 2", "foo2@bar.com");
		volunteer2.location = new GPSLocation(52.1855461,0.1323421);
		volunteer2.times.add(time);
		Volunteer volunteer3 = this.constructVolunteer("Volunteer 3", "foo3@bar.com");
		volunteer3.location = new GPSLocation(51.5489011,0.6869061);
		volunteer3.times.add(time);

		Organisation organisation = this.constructOrganisation("foo@example.com");

		this.jobManager.addJob(job, organisation);
		this.userManager.addVolunteer(volunteer1);
		this.userManager.addVolunteer(volunteer2);
		this.userManager.addVolunteer(volunteer3);

		Iterable<Volunteer> volunteers = this.service.getMatchingVolunteers(job);
		Map<String, Volunteer> volunteerMap = Maps.uniqueIndex(volunteers, v -> v.user.fullName);

		assertEquals(2, volunteerMap.size());
		assertTrue(volunteerMap.containsKey("Volunteer 1"));
		assertTrue(volunteerMap.containsKey("Volunteer 2"));
		assertFalse(volunteerMap.containsKey("Volunteer 3"));
	}

	/**
	 * Create a Volunteer and several Jobs, some of which share a common
	 * Time with the Volunteer, others which don't, and retrieve all matching
	 * Jobs.
	 *
	 * Spatial location is ignored.
	 */
	@Test
	public void testGetTimedJobs() throws DuplicateEntryException {
		Time mondayMorning = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING);
		Time mondayAfternoon = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.AFTERNOON);
		Time tuesdayMorning = this.timePeriodService.getTimeEntity(DayOfWeek.TUESDAY, TimePeriod.MORNING);
		Time thursdayEvening = this.timePeriodService.getTimeEntity(DayOfWeek.THURSDAY, TimePeriod.EVENING);

		Volunteer volunteer = this.constructVolunteer("Foo bar", "foo@bar.com");
		volunteer.times.add(mondayMorning);
		volunteer.times.add(tuesdayMorning);

		Job job1 = this.constructJob("Job 1");
		job1.time = mondayMorning;
		Job job2 = this.constructJob("Job 2");
		job2.time = mondayAfternoon;
		Job job3 = this.constructJob("Job 3");
		job3.time = tuesdayMorning;
		Job job4 = this.constructJob("Job 4");
		job4.time = thursdayEvening;

		Organisation organisation = this.constructOrganisation("foo@example.com");

		this.userManager.addVolunteer(volunteer);
		this.jobManager.addJob(job1, organisation);
		this.jobManager.addJob(job2, organisation);
		this.jobManager.addJob(job3, organisation);
		this.jobManager.addJob(job4, organisation);

		Iterable<Job> jobs = this.service.getMatchingJobs(volunteer);
		Map<String, Job> jobMap = Maps.uniqueIndex(jobs, job -> job.summary);

		assertEquals(2, jobMap.size());
		assertTrue(jobMap.containsKey("Job 1"));
		assertTrue(jobMap.containsKey("Job 3"));
	}

	/**
	 * Create a Job and several Volunteers, some of which share a common Time
	 * with the Job, others which don't, and retrieve all matching Volunteers.
	 *
	 * Spatial location is ignored.
	 */
	@Test
	public void testGetTimedVolunteers() throws DuplicateEntryException {
		Time mondayMorning = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING);
		Time mondayAfternoon = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.AFTERNOON);
		Time tuesdayMorning = this.timePeriodService.getTimeEntity(DayOfWeek.TUESDAY, TimePeriod.MORNING);
		Time thursdayEvening = this.timePeriodService.getTimeEntity(DayOfWeek.THURSDAY, TimePeriod.EVENING);

		Job job = this.constructJob("Job");
		job.time = mondayMorning;

		Volunteer volunteer1 = this.constructVolunteer("Volunteer 1", "foo1@bar.com");
		volunteer1.times.add(mondayMorning);
		volunteer1.times.add(tuesdayMorning);
		Volunteer volunteer2 = this.constructVolunteer("Volunteer 2", "foo2@bar.com");
		volunteer2.times.add(mondayAfternoon);
		volunteer2.times.add(thursdayEvening);
		Volunteer volunteer3 = this.constructVolunteer("Volunteer 3", "foo3@bar.com");
		volunteer3.times.add(mondayMorning);
		volunteer3.times.add(mondayAfternoon);
		volunteer3.times.add(tuesdayMorning);
		volunteer3.times.add(thursdayEvening);

		Organisation organisation = this.constructOrganisation("foo@example.com");

		this.jobManager.addJob(job, organisation);
		this.userManager.addVolunteer(volunteer1);
		this.userManager.addVolunteer(volunteer2);
		this.userManager.addVolunteer(volunteer3);

		Iterable<Volunteer> volunteers = this.service.getMatchingVolunteers(job);
		Map<String, Volunteer> volunteerMap = Maps.uniqueIndex(volunteers, v -> v.user.fullName);

		assertEquals(2, volunteerMap.size());
		assertTrue(volunteerMap.containsKey("Volunteer 1"));
		assertTrue(volunteerMap.containsKey("Volunteer 3"));
	}
	
	/**
	 * Create a Volunteer and several Jobs, some of which are within the
	 * maxDistancePreferenceKm and time preferences of the Volunteer, 
	 * some of which aren't, and retrieve all nearby Jobs.
	 * 
	 * Tests: neither time nor distance match, time doesn't match,
	 * distance doesn't match
	 */
	@Test
	public void testSpatialTimedJobs() throws DuplicateEntryException {
		Time mondayMorning = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING);
		Time mondayAfternoon = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.AFTERNOON);
		Time tuesdayMorning = this.timePeriodService.getTimeEntity(DayOfWeek.TUESDAY, TimePeriod.MORNING);
		

		Volunteer volunteer = this.constructVolunteer("Foo bar", "foo@bar.com");
		volunteer.location = new GPSLocation(52.202925,0.090530);
		volunteer.maxDistancePreferenceKm = 50;
		volunteer.times.add(mondayMorning);

		Job job1 = this.constructJob("Job 1");
		job1.location = this.dummyAddressLocation(new GPSLocation(52.2029252,0.0905302), "a");
		job1.time = mondayMorning;
		Job job2 = this.constructJob("Job 2");
		job2.location = this.dummyAddressLocation(new GPSLocation(52.2029252,0.0905302), "b");
		job2.time = mondayAfternoon;
		Job job3 = this.constructJob("Job 3");
		job3.location = this.dummyAddressLocation(new GPSLocation(38.8994611,-77.0846061), "c");
		job3.time = tuesdayMorning;
		Job job4 = this.constructJob("Job 4");
		job4.location = this.dummyAddressLocation(new GPSLocation(38.8994611,-77.0846061), "c");
		job4.time = tuesdayMorning;

		Organisation organisation = this.constructOrganisation("foo@example.com");

		this.userManager.addVolunteer(volunteer);
		this.jobManager.addJob(job1, organisation);
		this.jobManager.addJob(job2, organisation);
		this.jobManager.addJob(job3, organisation);

		Iterable<Job> jobs = this.service.getMatchingJobs(volunteer);
		Map<String, Job> jobMap = Maps.uniqueIndex(jobs, job -> job.summary);

		assertEquals(1, jobMap.size());
		assertTrue(jobMap.containsKey("Job 1"));
		assertFalse(jobMap.containsKey("Job 2"));
		assertFalse(jobMap.containsKey("Job 3"));
		assertFalse(jobMap.containsKey("Job 4"));
	}

	/**
	 * Create a Job and several Volunteers, some of which are within the
	 * job search radius, some of which aren't, and retrieve all nearby
	 * Volunteers.
	 *
	 * Tests: neither time nor distance match, time doesn't match,
	 * distance doesn't match
	 */
	@Test
	public void testSpatialTimedVolunteers() throws DuplicateEntryException {
		Time mondayMorning = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING);
		Time mondayAfternoon = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.AFTERNOON);

		Job job = this.constructJob("Job");
		job.location = this.dummyAddressLocation(new GPSLocation(52.202925,0.090530), "a");
		job.time = mondayMorning;

		Volunteer volunteer1 = this.constructVolunteer("Volunteer 1", "foo1@bar.com");
		volunteer1.location = new GPSLocation(52.1855461,0.1323421);
		volunteer1.times.add(mondayMorning);
		Volunteer volunteer2 = this.constructVolunteer("Volunteer 2", "foo2@bar.com");
		volunteer2.location = new GPSLocation(52.1855461,0.1323421);
		volunteer2.times.add(mondayAfternoon);
		Volunteer volunteer3 = this.constructVolunteer("Volunteer 3", "foo3@bar.com");
		volunteer3.location = new GPSLocation(51.5489011,0.6869061);
		volunteer3.times.add(mondayMorning);
		Volunteer volunteer4 = this.constructVolunteer("Volunteer 4", "foo4@bar.com");
		volunteer4.location = new GPSLocation(51.5489011,0.6869061);
		volunteer4.times.add(mondayAfternoon);

		Organisation organisation = this.constructOrganisation("foo@example.com");

		this.jobManager.addJob(job,organisation);
		this.userManager.addVolunteer(volunteer1);
		this.userManager.addVolunteer(volunteer2);
		this.userManager.addVolunteer(volunteer3);

		Iterable<Volunteer> volunteers = this.service.getMatchingVolunteers(job);
		Map<String, Volunteer> volunteerMap = Maps.uniqueIndex(volunteers, v -> v.user.fullName);

		assertEquals(1, volunteerMap.size());
		assertTrue(volunteerMap.containsKey("Volunteer 1"));
		assertFalse(volunteerMap.containsKey("Volunteer 2"));
		assertFalse(volunteerMap.containsKey("Volunteer 3"));
		assertFalse(volunteerMap.containsKey("Volunteer 4"));
	}

	/**
	 * Create some jobs with varying descriptions, and perform searches on them.
	 *
	 * Ignores spatial and timed parameters.
	 */
	@Test
	public void testJobDescriptionSearch() throws DuplicateEntryException {
		Time time = this.timePeriodService.getTimeEntity(DayOfWeek.MONDAY, TimePeriod.MORNING);

		Volunteer volunteer = this.constructVolunteer("Foo bar", "foo@bar.com");
		volunteer.times.add(time);

		Job job1 = this.constructJob("Job 1");
		job1.description = "Summary of the Amazon S3 Service Disruption";
		job1.time = time;
		Job job2 = this.constructJob("Job 2");
		job2.description = "Interview with AMD CEO Lisa Su";
		job2.time = time;
		Job job3 = this.constructJob("Job 3");
		job3.description = "AMD Zen and Ryzen 7 Review: A Deep Dive";
		job3.time = time;

		Organisation organisation = this.constructOrganisation("foo@example.com");

		this.userManager.addVolunteer(volunteer);
		this.jobManager.addJob(job1, organisation);
		this.jobManager.addJob(job2, organisation);
		this.jobManager.addJob(job3, organisation);

		Iterable<Job> jobs;
		Map<String, Job> jobMap;

		jobs = this.service.getMatchingJobs(
			volunteer, new Filter("description", ComparisonOperator.LIKE, "*amazon*")
		);
		jobMap = Maps.uniqueIndex(jobs, job -> job.summary);
		assertEquals(1, jobMap.size());
		assertTrue(jobMap.containsKey("Job 1"));

		jobs = this.service.getMatchingJobs(
			volunteer, new Filter("description", ComparisonOperator.LIKE, "*ryzen 7*")
		);
		jobMap = Maps.uniqueIndex(jobs, job -> job.summary);
		assertEquals(1, jobMap.size());
		assertTrue(jobMap.containsKey("Job 3"));

		jobs = this.service.getMatchingJobs(
			volunteer, new Filter("description", ComparisonOperator.LIKE, "*ryzen 8*")
		);
		jobMap = Maps.uniqueIndex(jobs, job -> job.summary);
		assertEquals(0, jobMap.size());

		jobs = this.service.getMatchingJobs(
			volunteer, new Filter("description", ComparisonOperator.LIKE, "*Amd*")
		);
		jobMap = Maps.uniqueIndex(jobs, job -> job.summary);
		assertEquals(2, jobMap.size());
		assertTrue(jobMap.containsKey("Job 2"));
		assertTrue(jobMap.containsKey("Job 3"));
	}
}
