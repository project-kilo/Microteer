package uk.ac.cam.cl.part1b1617.kilo.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.neo4j.ogm.session.Session;

import uk.ac.cam.cl.part1b1617.kilo.AddressLocation;
import uk.ac.cam.cl.part1b1617.kilo.GPSLocation;
import uk.ac.cam.cl.part1b1617.kilo.IntegrationTestBase;
import uk.ac.cam.cl.part1b1617.kilo.TimePeriod;
import uk.ac.cam.cl.part1b1617.kilo.graph.DuplicateEntryException;
import uk.ac.cam.cl.part1b1617.kilo.graph.EntryNotFoundException;
import uk.ac.cam.cl.part1b1617.kilo.graph.JobManagerService;
import uk.ac.cam.cl.part1b1617.kilo.graph.JobManagerServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.MatchingService;
import uk.ac.cam.cl.part1b1617.kilo.graph.MatchingServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.NotificationService;
import uk.ac.cam.cl.part1b1617.kilo.graph.NotificationServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.SkillService;
import uk.ac.cam.cl.part1b1617.kilo.graph.SkillServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.TimePeriodService;
import uk.ac.cam.cl.part1b1617.kilo.graph.TimePeriodServiceImpl;
import uk.ac.cam.cl.part1b1617.kilo.graph.UserManager;
import uk.ac.cam.cl.part1b1617.kilo.graph.UserManagerService;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Job;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.JobAllocationEdge;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Organisation;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Time;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.User;
import uk.ac.cam.cl.part1b1617.kilo.graph.domain.Volunteer;

public class ControlImplIntegrationTest extends IntegrationTestBase {
	protected JobManagerService jobManagerService;
	protected UserManagerService userManagerService;
	protected SkillService skillService;
	protected MatchingService matchingService;
	protected TimePeriodService timePeriodService;
	protected NotificationService notificationService;
	protected Control control;

	protected Session session;

	@Before
	public void setUp() throws Exception {
		database.purge();
		this.session = database.openSession();

		Session thisSession = database.openSession();

		this.jobManagerService = new JobManagerServiceImpl(thisSession);
		this.userManagerService = new UserManager(thisSession);
		this.skillService = new SkillServiceImpl(thisSession);
		this.matchingService = new MatchingServiceImpl(thisSession);
		this.timePeriodService = new TimePeriodServiceImpl(thisSession);
		this.notificationService = new NotificationServiceImpl(thisSession);

		this.control = new ControlImpl(jobManagerService, matchingService, 
				userManagerService, skillService, timePeriodService, notificationService);
	}

	protected Job constructJob() {
		Job job = new Job();
		job.summary = "A Job";
		job.description = "Some work";
		job.location = new AddressLocation(
			new GPSLocation(0.0, 0.0),
			"William Gates Building",
			"15 JJ Thompson Avenue",
			"Cambrige",
			"Cambridgeshire",
			"CB3 0FD",
			"United Kingdom"
		);
		job.expiration = OffsetDateTime.now().plusDays(1);
		job.requiredVolunteers = 1L;
		return job;
	}

	@Test
	public void testAddUpdateRemove() throws DuplicateEntryException {
		Job job = this.constructJob();
		GPSLocation gpsLocation = new GPSLocation(0.0002, 0.0002);
		AddressLocation addressLocation = new AddressLocation(gpsLocation, "line 1", "line2", "town",
				"county", "OX56 7TF", "UK");
		Organisation organisation = new Organisation("some organisation", addressLocation, "org@bar.com",
				"www.org.com", "03456936455");
		this.control.addOrganisation(organisation);
		this.control.addJob(job, organisation, new ArrayList<>(), new ArrayList<>());

		Iterator<Job> jobs = this.session.loadAll(Job.class).iterator();
		assertTrue(jobs.hasNext());
		Job dbJob = jobs.next();
		assertEquals("A Job", dbJob.summary);
		assertFalse(jobs.hasNext());

		this.session.clear();
		job.summary = "Another Job";
		this.control.updateJob(job);

		jobs = this.session.loadAll(Job.class).iterator();
		assertTrue(jobs.hasNext());
		dbJob = jobs.next();
		assertEquals("Another Job", dbJob.summary);
		assertFalse(jobs.hasNext());

		this.session.clear();
		this.control.removeJob(job);
		jobs = this.session.loadAll(Job.class).iterator();
		assertFalse(jobs.hasNext());
	}

	@Test
	public void testGetMatches() throws DuplicateEntryException, EntryNotFoundException{
		// setup the graph
		Instant date = Instant.now();
		GPSLocation gpsLocation = new GPSLocation(0.0002, 0.0002);
		List<String> skills = new ArrayList<>();
		skills.add("sitting");
		Time time = this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON);

		//add volunteer
		Volunteer volunteer = new Volunteer("foo@bar.com", "Boris Yasukox", date, "password hash 123",
				"salt", gpsLocation, 100, "01234567890");
		volunteer.times.add(time);
		this.control.addVolunteerWithoutTimes(volunteer, skills);

		//add organisation
		AddressLocation addressLocation = new AddressLocation(gpsLocation, "line 1", "line2", "town",
				"county", "OX56 7TF", "UK");
		Organisation organisation = new Organisation("some organisation", addressLocation, "org@bar.com",
				"www.org.com", "03456936455");
		this.control.addOrganisation(organisation);
		User orgUser = new User("passwordHash", "passwordSalt", date, "Organisation user", "orguser@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);

		//add job
		Job job = constructJob();
		job.time = time;
		this.control.addJob(job, organisation, skills, new ArrayList<>());

		//test
		Iterable<Volunteer> nearbyVolunteers = matchingService.getMatchingVolunteers(job);
		assertTrue(nearbyVolunteers.iterator().hasNext());
		this.session.clear();

		// now simulate someone logging in, getting their matches and accepting the match
		User myUser = this.control.getUserForEmail("foo@bar.com");
		Volunteer myVolunteer = myUser.volunteer;
		Set<Job> myMatches = this.control.getMatches(myVolunteer);
		Job myJob = null;
		for (Job j : myMatches) {
			myJob = j;
			assertTrue(myJob.summary.equals("A Job"));
		}
		this.control.acceptJob(myJob, myVolunteer);

		//test
		Iterator<Volunteer> testVolunteers = this.session.loadAll(Volunteer.class, 2).iterator();
		assertTrue(testVolunteers.next().jobs.accepted.size() == 1);
		Iterator<JobAllocationEdge> jobAllocationEdges = this.session.loadAll(JobAllocationEdge.class).iterator();
		assertFalse(jobAllocationEdges.hasNext());
	}

	public void testUnacceptJob() throws EntryNotFoundException {
		User myUser = this.control.getUserForEmail("foo@bar.com");
		Volunteer myVolunteer = myUser.volunteer;
		Set<Job> myAccepted = this.control.getAcceptedJobs(myVolunteer);
		for (Job j : myAccepted) {
			this.control.unacceptJob(j, myVolunteer);
		}

		Iterator<JobAllocationEdge> jobAllocationEdges = this.session.loadAll(JobAllocationEdge.class).iterator();
		assertTrue(jobAllocationEdges.hasNext());
	}

	//sign up volunteer
	//sign up organisation
	//log in to organisation and add a job
	//log in as a volunteer and accept the job
	//log in to the org user, get the jobs owned by the org and make sure the job has an accepted volunteer
	@Test
	public void testUseCase1() throws DuplicateEntryException, EntryNotFoundException{
		Instant date = Instant.now();
		GPSLocation gpsLocation = new GPSLocation(0.0002, 0.0002);
		Time time = this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON);
		List<String> skills = new ArrayList<>();
		skills.add("sitting");

		//sign up volunteer
		Volunteer volunteer = new Volunteer("foo@bar.com", "Boris Yasukox", date, "password hash 123",
				"salt", gpsLocation, 100, "01234567890");
		volunteer.times.add(time);
		this.control.addVolunteerWithoutTimes(volunteer, skills);
		this.session.clear();

		//sign up organisation
		AddressLocation addressLocation = new AddressLocation(gpsLocation, "line 1", "line2", "town",
				"county", "OX56 7TF", "UK");
		Organisation organisation = new Organisation("some organisation", addressLocation, "org@bar.com",
				"www.org.com", "03456936455");
		this.control.addOrganisation(organisation);
		User orgUser = new User("passwordHash", "passwordSalt", date, "Organisation user", "orguser@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);
		this.session.clear();

		//log in and add job
		orgUser = this.control.getUserForEmail("orguser@bar.com");
		Job job = constructJob();
		UUID testUUID = job.uuid;
		job.time = time;
		this.control.addJob(job, orgUser.adminOf, skills, new ArrayList<>());
		this.session.clear();

		// now simulate a volunteer logging in, getting their matches and accepting the match
		User myUser = this.control.getUserForEmail("foo@bar.com");
		Volunteer myVolunteer = myUser.volunteer;
		Set<Job> myMatches = this.control.getMatches(myVolunteer);
		Job myJob = null;
		for (Job j : myMatches) {
			myJob = j;
			assertTrue(myJob.summary.equals("A Job"));
		}
		this.control.acceptJob(myJob, myVolunteer);

		//test
		Iterator<Volunteer> testVolunteers = this.session.loadAll(Volunteer.class, 2).iterator();
		assertTrue(testVolunteers.next().jobs.accepted.size() == 1);
		Iterator<JobAllocationEdge> jobAllocationEdges = this.session.loadAll(JobAllocationEdge.class).iterator();
		assertFalse(jobAllocationEdges.hasNext());

		//log in to the org user, get the jobs owned by the org, make sure the job has an accepted volunteer
		this.session.clear();
		orgUser = this.control.getUserForEmail("orguser@bar.com");
		Set<Job> myOwnedJobs = this.control.getOwnedJobs(orgUser.adminOf);
		myJob = myOwnedJobs.iterator().next();
		assertTrue(myJob.uuid.equals(testUUID));
		Set<Volunteer> acceptedVolunteers = this.control.getAcceptedVolunteers(myJob);
		assertTrue(acceptedVolunteers.iterator().hasNext());
	}

	//sign up volunteer
	//sign up organisation
	//log in to organisation and add a job
	//log in as a volunteer and reject the job
	@Test
	public void testUseCase2() throws DuplicateEntryException, EntryNotFoundException{
		Instant date = Instant.now();
		GPSLocation gpsLocation = new GPSLocation(0.0002, 0.0002);
		Time time = this.timePeriodService.getTimeEntity(DayOfWeek.FRIDAY, TimePeriod.AFTERNOON);
		List<String> skills = new ArrayList<>();
		skills.add("sitting");

		//sign up volunteer
		Volunteer volunteer = new Volunteer("foo@bar.com", "Boris Yasukox", date, "password hash 123",
				"salt", gpsLocation, 100, "01234567890");
		volunteer.times.add(time);
		this.control.addVolunteerWithoutTimes(volunteer, skills);
		this.session.clear();

		//sign up organisation
		AddressLocation addressLocation = new AddressLocation(gpsLocation, "line 1", "line2", "town",
				"county", "OX56 7TF", "UK");
		Organisation organisation = new Organisation("some organisation", addressLocation, "org@bar.com",
				"www.org.com", "03456936455");
		this.control.addOrganisation(organisation);
		User orgUser = new User("passwordHash", "passwordSalt", date, "Organisation user", "orguser@bar.com");
		this.control.addOrganisationUser(orgUser, organisation);
		this.session.clear();

		//log in and add job
		orgUser = this.control.getUserForEmail("orguser@bar.com");
		Job job = constructJob();
		job.time = time;
		this.control.addJob(job, orgUser.adminOf, skills, new ArrayList<>());
		this.session.clear();

		// now simulate a volunteer logging in, getting their matches and rejecting the match
		User myUser = this.control.getUserForEmail("foo@bar.com");
		Volunteer myVolunteer = myUser.volunteer;
		Set<Job> myMatches = this.control.getMatches(myVolunteer);
		Job myJob = null;
		for (Job j : myMatches) {
			myJob = j;
			assertTrue(myJob.summary.equals("A Job"));
		}
		this.control.rejectJob(myJob, myVolunteer, MatchQuality.BAD_MATCH);

		//test
		Iterator<Volunteer> testVolunteers = this.session.loadAll(Volunteer.class, 2).iterator();
		assertTrue(testVolunteers.next().jobs.allocated.size() == 0);
		Iterator<JobAllocationEdge> jobAllocationEdges = this.session.loadAll(JobAllocationEdge.class).iterator();
		assertFalse(jobAllocationEdges.hasNext());

		Set<Volunteer> completedVolunteers = this.control.getCompletedVolunteers(myJob);
		assertFalse(completedVolunteers.iterator().hasNext());

	}
}
