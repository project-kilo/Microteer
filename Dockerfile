FROM openjdk:8-jre-alpine
MAINTAINER Robin McCorkell "robin@mccorkell.me.uk"

ADD build/distributions/Microteer.tar /opt/

EXPOSE 5050

ENTRYPOINT ["/opt/Microteer/bin/Microteer"]
